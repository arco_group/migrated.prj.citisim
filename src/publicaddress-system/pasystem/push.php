<!DOCTYPE html> 
<html> 
      
<head> 
    <title> 
        Fire Triggers
    </title> 
</head> 
  
<body style="text-align:center;"> 

      
    <?php
        session_start();
        if(array_key_exists('button1', $_POST)) { 
            button1(); 
        }

        if(array_key_exists('button2', $_POST)) { 
            button2(); 
        }

        if(array_key_exists('button3', $_POST)) { 
            button3(); 
        }

        if(array_key_exists('button4', $_POST)) { 
            button4(); 
        }


    ?>
    
    <form method="post"> 
        <input type="submit" name="button1"class="button" value="Pulse 1" /> 
    </form> 
    <form method="post"> 
        <input type="submit" name="button2"class="button" value="Pulse 2" /> 
    </form> 
    <form method="post"> 
        <input type="submit" name="button3"class="button" value="Smoke detector" /> 
    </form> 
    <form method="post"> 
        <input type="submit" name="button4"class="button" value="Reboot" /> 
    </form> 
    <?php

        function button1() { 
            $emergency = calculateRoute(true, 16, 0.71, 30);
            $central = calculateRoute(false, 50, 0.71, 30);

            if ($emergency == -1 or $central == -1){
                if ($emergency != -1){
                    //Salida 1
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else if ($central != -1){
                    //Salida 2
                    $_SESSION['screen1'] = "./assets/case3.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }
            }
            else{
                if ( $emergency <= $central){
                    //Las dos pero más cerca emergencia
                    $_SESSION['screen1'] = "./assets/case1.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else{
                    //Las dos pero más cerca central
                    $_SESSION['screen1'] = "./assets/case4.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }

            }

        } 

        function button2() { 
            $emergency = calculateRoute(false, 16, 0.71, 30);
            $central = calculateRoute(true, 50, 0.71, 30);

            if ($emergency == -1 or $central == -1){
                if ($emergency != -1){
                    //Salida 1
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else if ($central != -1){
                    //Salida 2
                    $_SESSION['screen1'] = "./assets/case3.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }
            }
            else{
                if ( $emergency <= $central){
                    //Las dos pero más cerca emergencia
                    $_SESSION['screen1'] = "./assets/case1.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else{
                    //Las dos pero más cerca central
                    $_SESSION['screen1'] = "./assets/case4.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }

            }

        } 

        function button3() { 
            $emergency = calculateRoute(false, 16, 0.71, 30);
            $central = calculateRoute(false, 50, 0.71, 30);

            if ($emergency == -1 or $central == -1){
                if ($emergency != -1){
                    //Salida 1
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else if ($central != -1){
                    //Salida 2
                    $_SESSION['screen1'] = "./assets/case3.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }
            }
            else{
                if ( $emergency <= $central){
                    //Las dos pero más cerca emergencia
                    $_SESSION['screen1'] = "./assets/case1.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=1&r2=0'; 
                    $contents = file_get_contents($url);
                }
                else{
                    //Las dos pero más cerca central
                    $_SESSION['screen1'] = "./assets/case4.png";
                    $_SESSION['screen1'] = "./assets/case2.png";
                    $url = 'http://192.168.55.102/status.xml?r1=0&r2=1'; 
                    $contents = file_get_contents($url);
                }

            }

        } 

        function button4() { 
            $_SESSION['screen1'] = "./assets/normal.png";
        } 

        function calculateRoute($blocked, $distance, $velocity, $percent){
            if ($blocked){
                return -1;
            }
            else{

                $ttrav = $distance / $velocity;
                $tevac = $percent + $ttrav;
                return $tevac;
            }
        }
    ?> 
  
    
</head> 
  
</html> 