#!/usr/bin/python3

import os
import sys
import Ice
import logging
from argparse import ArgumentParser
from flask import Flask, request, Response
from libcitisim import Broker, SmartObject, PropertyService

logging.getLogger().setLevel(logging.INFO)


class FlaskApp(Flask):
    def __init__(self, observer):
        self.observer = observer
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_url_path=static)

        @self.route('/')
        def index():
            args = request.args.to_dict()
            value = args.get("value", "False").lower() != "false"
            logging.info("- observer notify: {}".format(value))
            self.observer.notify(value, "", {})
            return Response("OK")


# FIXME: it only supports one single destination and of type DigitalSink
#        make it generic!

class Forwarder(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)

        key = "Destination.ID"
        oid = self.broker.props.getProperty(key)
        if not oid:
            logging.error("Please, provide the '{}' property".format(key))
            return 1

        prx = self.get_proxy(oid).ice_encodingVersion(Ice.Encoding_1_0)
        prx = SmartObject.DigitalSinkPrx.uncheckedCast(prx)
        logging.info("Forwarding to: " + str(prx))

        app = FlaskApp(prx)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def get_proxy(self, oid):
        if not self.args.use_topic:
            prx = self.broker.get_property(oid + " | proxy")
            return self.ic.stringToProxy(prx)
        else:
            try:
                tpcname = self.broker.get_property(oid + " | private-channel")
                topic = self.broker.get_topic(tpcname)
                return topic.getPublisher()
            except PropertyService.PropertyException:
                logging.warning(
                    " no private-channel for {}!".format(oid))

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--port", type=int, default=5002,
            help="port to bind web interface")
        parser.add_argument("--use-topic", action="store_true",
            help="use destination private topic instead its proxy")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    Forwarder(1).main(sys.argv)
