
Overview
=========

Provision to install and configure a Citisim node on a Raspberry Pi. This is for CitiSpeak node.

**Note:** You must be able to access it from ssh using public key. The SSH host configuration name must be 'citispeak'.


