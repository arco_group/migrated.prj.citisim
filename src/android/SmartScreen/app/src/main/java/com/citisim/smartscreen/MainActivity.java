package com.citisim.smartscreen;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class MainActivity extends AppCompatActivity {

    private SmartScreenApp _app;
    private Handler _handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
        setContentView(R.layout.activity_main);

        _handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                ImageView img = findViewById(R.id.mainImage);

                if (msg.what == 1) {
                    String rname = (String) msg.obj;

                    // get resource name (i.e. no extension)
                    String bname = rname;
                    int index = rname.lastIndexOf('.');
                    if (index != -1)
                        bname = rname.substring(0, index);

                    // get actual resource ID
                    int rid = getResources().getIdentifier(bname, "drawable", getPackageName());
                    if (rid == 0) {
                        System.out.println("Resource not found! (name: " + rname + ")") ;
                        return;
                    }

                    if (rname.endsWith(".gif")) {
                        System.out.println("Its a gif!! (" + rid + ", " + rname + ")");
                        Glide.with(MainActivity.this)
                                .asGif()
                                .load(rid)
                                .transition(withCrossFade())
                                .into(img);
                    }
                    else {
                        img.setImageResource(rid);
                    }
                }
                else if (msg.what == 0) {
                    img.setImageResource(R.drawable.black);
                }
            }
        };

        _app = (SmartScreenApp)getApplication();
        _app.setHandler(_handler);
    }
}
