package com.citisim.smartscreen;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import java.util.concurrent.ExecutionException;

import Ice.Communicator;
import Ice.Current;
import Ice.ObjectAdapter;
import Ice.ObjectPrx;
import SmartObject.WelcomeServicePrx;
import SmartObject.WelcomeServicePrxHelper;
import SmartObject._SmartScreenDisp;

// FIXME: remember to use Ice 3.6

public class SmartScreenApp extends Application {

    private Communicator _ic;
    private ObjectAdapter _adapter;
    private Handler _handler;

    public void setHandler(Handler handler) {
        _handler = handler;
    }

    private class SmartScreenI extends _SmartScreenDisp {

        @Override
        public void setObserver(String observer, Current __current) {
            System.out.println("setObserver() not implemented!!!");
        }

        @Override
        public void render(String messageID, Current __current) {
            System.out.println("Render called with: " + messageID);
            _handler.sendMessage(
                    Message.obtain(_handler, 1, messageID));
        }

        @Override
        public void clear(Current __current) {
            _handler.sendMessage(
                    Message.obtain(_handler, 0));
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        asyncIceSetup();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        if (_ic != null) {
            _ic.shutdown();
            _ic.destroy();
            _ic = null;
        }

        if (_adapter != null) {
            _adapter.deactivate();
            _adapter.destroy();
            _adapter = null;
        }
    }

    private void asyncIceSetup() {

        class MyAsyncTask extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... params) {
                createCommunicator();
                createObjectAdapter();
                registerServant();
                return null;
            }
        }

        try {
            new MyAsyncTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void createCommunicator() {
        final Ice.InitializationData idata = new Ice.InitializationData();
        idata.properties = Ice.Util.createProperties();
        idata.properties.setProperty("Ice.IPv6", "0");

        String path = Environment.getExternalStorageDirectory() + "/smart-screen.config";
        idata.properties.load(path);

        _ic = Ice.Util.initialize(idata);
    }

    private void createObjectAdapter() {
        _adapter = _ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 3333");
        _adapter.activate();
    }

    private void registerServant() {
        String oid = _ic.getProperties().getProperty("SmartScreen.ID");
        ObjectPrx prx = _adapter.add(new SmartScreenI(), _ic.stringToIdentity(oid));
        System.out.println("Service ready, prx: '" + prx + "'");

        // get proxy to hello service
        ObjectPrx welcomePrx = _ic.propertyToProxy("WelcomeServer.Proxy");
        WelcomeServicePrx welcome = WelcomeServicePrxHelper.checkedCast(welcomePrx);

        // say hello with this proxy
        welcome.hello(prx.toString(), "SmartScreen");
    }
}
