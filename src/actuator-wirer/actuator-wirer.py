#!/usr/bin/python3

import sys
import Ice
import logging
import IceStorm
from argparse import ArgumentParser
from time import sleep
from libcitisim import Broker, PropertyService

logging.basicConfig(level=logging.INFO)


class ActuatorWirerService(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return 1

        self.ic = self.communicator()
        self.settings = {}
        self.broker = Broker(ic=self.ic)

        try:
            logging.info("sleeping time is: {}".format(self.args.sleep))
            while True:
                self.check_wirings()
                sleep(self.args.sleep)
        except KeyboardInterrupt:
            pass

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--sleep", default=60, type=int,
            help="time between checks")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True

    def check_wirings(self):
        self.update_settings()
        for devid, tpcname in self.settings.items():
            devprx = self.get_proxy(devid)
            if devprx is None:
                logging.warning("actuator '{}' is not available!".format(devid))
                continue

            topic = self.broker.get_topic(tpcname)
            topic.unsubscribe(devprx)
            print(topic.getPublisher())
            topic.subscribeAndGetPublisher({}, devprx)
            logging.info("'{}' linked to '{}'".format(devid, tpcname))

    def get_proxy(self, devid):
        try:
            prx = self.broker.get_property(devid + " | proxy")
            return self.ic.stringToProxy(prx)
        except PropertyService.PropertyException:
            return None

    def update_settings(self):
        try:
            key = "actuator-links"
            self.settings = self.broker.get_property(key)
        except PropertyService.PropertyException:
            logging.warning(
                " no settings defined (no '{}' property in PS)!".format(key))


if __name__ == "__main__":
    ActuatorWirerService(1).main(sys.argv)


