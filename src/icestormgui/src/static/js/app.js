// -*- mode: web; coding: utf-8 -*-

_ = console.info.bind(console);
app = angular.module("IceStormGUI", ['ngRoute', 'ngCookies']);

app.config([
    '$locationProvider', '$routeProvider',
    function($locationProvider, $routeProvider) {
	$locationProvider.hashPrefix('!');

	$routeProvider
          .when('/config', {
	      templateUrl: 'static/partials/config.html',
	  })
	  .when('/topicList', {
	      templateUrl: 'static/partials/topiclist.html',
	  })
	  .otherwise('/topicList');
    }
])

app.directive('hasConfirmDialog', function($parse) {
    return function(scope, element, attrs) {
	function on_confirm() {
	    var exp = $parse(attrs.onConfirm);
	    scope.$apply(exp);
	};

	$(element).confirmation({
	    rootSelector: '[has-confirm-dialog]',
	    title: "Are you sure?",
	    placement: 'top',
	    singleton: true,
	    popout: true,
	    btnOkLabel: 'Yes',
	    btnCancelLabel: 'No&nbsp;',
	    onConfirm: on_confirm,
	});
    };
});

app.controller('topicListCtrl', function($scope, $cookies, $location) {
    _get_settings($scope, $cookies);

    $scope.go_to_settings = function() {
	$('#showConfigModal').modal('hide').on('hidden.bs.modal', function() {
	    $location.path('/config');
	    $scope.$apply();
	});
    };

    $scope.go_create_topic = async function() {
	try {
	    await $scope.manager.create($scope.new_topic_name);
	    $scope.new_topic_name = "";
	    $scope.do_reload();
	} catch (ex) {
	    _show_error($scope, ex);
	}
    };

    $scope.do_remove_topic = async function(topic) {
	try {
	    await topic.proxy.destroy();
	    await $scope.do_reload();
	} catch (ex) {
	    _show_error($scope, ex);
	}
    };

    $scope.do_clean_topics = function(topic) {
	$scope.topics.forEach(async function(topic, name) {
	    if (topic.subscribers.length != 0)
		return;
	    await topic.proxy.destroy();
	});
	$scope.do_reload();
    };

    $scope.do_subscribe_proxy = async function() {
	try {
	    var proxy = ic.stringToProxy($scope.proxy_to_subscribe);
	    await $scope.selected.proxy.subscribeAndGetPublisher([], proxy);
	    await $scope.do_reload();
	} catch (ex) {
	    _show_error($scope, ex);
	}
    };

    $scope.do_unsubscribe_from_topic = async function(subscriber) {
	await $scope.selected.proxy.unsubscribe(subscriber.proxy);
	await $scope.do_reload();
    };

    $scope.do_remove_subscribers = async function(topic) {
	topic.subscribers.forEach(async function(subscriber) {
	    await $scope.selected.proxy.unsubscribe(subscriber.proxy);
	});
	await $scope.do_reload();
    };

    $scope.do_reload = async function() {
	var current_page = $scope.pager ? $scope.pager.current : undefined;
	var current_selected = $scope.selected ? $scope.selected.name : undefined;

	$scope.topics = [];
	$scope.selected = null;
	$scope.pager = {
	    current: 1,
	    pages: 0,
	    page_size: 10,
	    next: function() {
		if ($scope.pager.current < $scope.pager.pages)
		    $scope.pager.current++;
	    },
	    prev: function() {
		if ($scope.pager.current > 1)
		    $scope.pager.current--;
	    }
	};
	await _retrieve_topics($scope, $cookies);
	if (current_page !== undefined) {
	    $scope.pager.current = Math.min($scope.pager.pages, current_page);
	    $scope.$apply();
	}

	if (current_selected) {
	    $scope.selected = $scope.topics_by_name[current_selected];
	}
	setTimeout(function() { $scope.$apply(); }, 10);
    };

    $scope.do_select_topic = function(topic) {
	$scope.selected = topic;
    };

    if (!$scope.icestorm_proxy) {
	$('#showConfigModal').modal('show');
	return;
    }

    // perform initial load of topics
    $scope.do_reload();
});

app.controller('configCtrl', function($scope, $cookies) {
    _get_settings($scope, $cookies);

    $scope.save_settings = function() {
	var opts = {expires: new Date((new Date()).getFullYear() + 10, 1)};
	var is_proxy = $('#IceStormProxy').val();
	$cookies.put('IceStorm.TopicManager.Proxy', is_proxy, opts);

	var locator = $('#IceDefaultLocator').val();
	$cookies.put('Ice.Default.Locator', locator, opts);

	var is_admin = $('#IceStormAdmin').val();
	$cookies.put('IceStorm.Admin.Proxy', is_admin, opts);
    };

    $scope.revert_settings = function() {
	var is_proxy = $cookies.get('IceStorm.TopicManager.Proxy');
	$('#IceStormProxy').val(is_proxy);

	var locator = $cookies.get('Ice.Default.Locator');
	$('#IceDefaultLocator').val(locator);

	var is_admin = $cookies.get('IceStorm.Admin.Proxy');
	$('#IceStormAdmin').val(is_admin);
    };
});

function _get_settings($scope, $cookies) {
	let locator_def = "IceGrid/Locator -t:ws -h pike.esi.uclm.es -p 5071";
	let isprx_def = "IceStorm/TopicManager";
	let isadm_def = "IceStorm/admin -t:ws -h pike.esi.uclm.es -p 9195";

   $scope.icestorm_proxy = $cookies.get('IceStorm.TopicManager.Proxy');
   $scope.default_locator = $cookies.get('Ice.Default.Locator');
   $scope.icestorm_admin = $cookies.get('IceStorm.Admin.Proxy');

	if (!$scope.icestorm_proxy)
	 	$scope.icestorm_proxy = isprx_def;
	if (!$scope.default_locator)
	 	$scope.default_locator = locator_def;
	if (!$scope.icestorm_admin)
	 	$scope.icestorm_admin = isadm_def;
}

async function _retrieve_topics($scope, $cookies) {
    ic = _get_communicator($scope);

    try {
	var manager = ic.stringToProxy($scope.icestorm_proxy);
	manager = await IceStorm.TopicManagerPrx.checkedCast(manager);
	var topics = await manager.retrieveAll();

	$scope.manager = manager;
	$scope.topic_names = [];
	$scope.topics_by_name = {};

	var counter = 1;
	var iter = topics.entries();
	for (var e=iter.next(); !e.done; e=iter.next()) {
	    var name = e.value[0];
	    var proxy = e.value[1];

	    var info = {name: name, number: counter++, proxy: proxy};
	    $scope.topics.push(info);
	    $scope.topics_by_name[name] = info;
	    await _retrieve_topic_info($scope, proxy, info);
	}

	await _retrieve_metrics($scope, $cookies);
	$scope.pager.pages = Math.ceil((counter-1)/$scope.pager.page_size);
	setTimeout(function() { $scope.$apply(); }, 10);
    }
    catch(ex) {
	_show_error($scope, ex);
    }
}

ic = null;
function _get_communicator($scope) {
    if (ic != null) {
	return ic;
    }

    var idata = new Ice.InitializationData();
    idata.properties = Ice.createProperties();
    idata.properties.setProperty("Ice.Default.Locator", $scope.default_locator);
    ic = Ice.initialize(idata);
    return ic;
}

async function _retrieve_topic_info($scope, proxy, info) {
    info.links = [];
    info.subscribers = [];
    info.active_subscribers = [];

    try {
	var links = await proxy.getLinkInfoSeq();
	for (i in links)
	    info.links.push(links[i].name);

	var subs = await proxy.getSubscribers();
	subs.forEach(function(oid) {
	    oid = ic.identityToString(oid);
	    info.active_subscribers.push(oid);
	});

	info.publisher = await proxy.getPublisher();
	setTimeout(function() { $scope.$apply(); }, 10);
    }
    catch(ex) {
	_show_error($scope, ex);
    }
}

async function _retrieve_metrics($scope, $cookies) {
    _get_settings($scope, $cookies);

    try {
	try {
	    var metrics = ic.stringToProxy($scope.icestorm_admin);
	    metrics = metrics.ice_facet("Metrics");
	    metrics = await IceMX.MetricsAdminPrx.checkedCast(metrics);
	} catch (ex) {
	    _(ex);
	    metrics = null;
	}

	if (!metrics) {
	    $scope.no_metrics = true;
	    $scope.$apply();
	    return;
	}

	var view = await metrics.getMetricsView("IceStormView");

	view = view[0];
	var mx_subscribers = view.get('Subscriber');
	var mx_topics = view.get('Topic');

	// retrieve metrics of topics
	for (var i in mx_topics) {
	    var mx = mx_topics[i];

	    // if topic does not exists (old MX object), ignore record
	    var topic = $scope.topics_by_name[mx.id];
	    if (topic === undefined)
		continue;

	    topic.mx = mx;
	}

	// retrieve metrics of subscribers
	for (var i in mx_subscribers) {
	    var mx = mx_subscribers[i];
	    var fields = mx.id.split("|");
	    var proxy = fields[0];
	    var state = fields[1];
	    var topic_name = fields[2];

	    // if topic does not exists (old MX object), ignore record
	    var topic = $scope.topics_by_name[topic_name];
	    if (topic === undefined)
		continue;

	    proxy = ic.stringToProxy(proxy);
	    var oid = ic.identityToString(proxy.ice_getIdentity());
	    if (topic.active_subscribers.indexOf(oid) == -1)
		continue;

	    $scope.topics_by_name[topic_name].subscribers.push({
		proxy: proxy,
		state: state,
		mx: mx,
	    });
	}

	setTimeout(function() { $scope.$apply(); }, 10);
    }
    catch(ex) {
	_show_error($scope, ex);
    }
}

function _show_error($scope, ex) {
    $scope.error = ex;
    $('#error-dlg').show();
    _("ERROR:", ex);

    // ensure the model is updated
    setTimeout(function() { $scope.$apply(); }, 10);
}

function copy_to_clipboard(id) {
    var content = $('#'+id).text();
    var item = $('<input value="'+content+'">');
    $('#'+id).append(item);
    item.select();
    document.execCommand("Copy");
    item.remove();
}
