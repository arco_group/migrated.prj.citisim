Overview
========

This folder contains the IceStorm GUI, a web application to manage an
IceStorm service, to list topics or subscribers, create and remove
elements, etc.


Citisim Settings
================

* Ice default locator: `IceGrid/Locator -t:ws -h pike.esi.uclm.es -p 5071`
* IceStorm Proxy: `IceStorm/TopicManager`
* IceStorm Admin Proxy: `IceStorm/admin -t:ws -h pike.esi.uclm.es -p 9195`


Setting up IceStorm for Metrics
===============================

You need to add the following properties on IceStorm settings (not IceBox):

    Ice.Admin.Endpoints = tcp -p SOME-PORT
    Ice.Admin.InstanceName = IceStorm
    IceMX.Metrics.IceStormView.Map.Topic.GroupBy = id
    IceMX.Metrics.IceStormView.Map.Subscriber.GroupBy = id|state|topic
