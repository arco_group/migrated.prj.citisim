#!/usr/bin/python3

import sys
import logging
import ast
import time
import signal
import Ice

from urllib.request import urlopen
from xml.dom import minidom
from datetime import datetime
from libcitisim import Broker

logging.basicConfig(level=logging.INFO)

UPDATE_TIME = 5 * 60
MEASURE_TIME = 30

class Requester:
    def __init__(self, vars):
        self.vars = vars
        self.domains = {}
        self.values = {}
        self.requested_values = {}

    def set_urls(self, urls):
        for key in urls.keys():
            host = urls[key].split('?')[0]
            device_id = urls[key].split('id=')[-1]
            self.domains[key] = host + '?var=' + device_id + '.'

    def get_average_measures(self, interval):
        time_reference = time.time()
        avereage_measures = None
        iterations = 0

        while(time.time() - time_reference < interval):
            if iterations == 0:
                average_measures = self.get_values()
                iterations += 1
            else:
                self.sum_dicts(average_measures, self.get_values())
                iterations += 1
            time.sleep(MEASURE_TIME)

        self.mean(average_measures, iterations)
        return average_measures    

    def sum_dicts(self, dict1, dict2):
        for key in dict1.keys():
            for var in dict1[key].keys():
                dict1[key][var] += dict2[key][var]

    def mean(self, dictionary, iterations):
        for key in dictionary.keys():
            for var in dictionary[key].keys():
                dictionary[key][var] /= iterations

    def get_values(self):
        values = {}
        for key, path in self.domains.items():
            values[key] = {}
            for var in self.vars:
                var_url = path + var
                values[key][var] = self.get_var_value(var_url)
        return values
    
    def get_var_value(self, var_url):
        dom = minidom.parse(urlopen(var_url))
        requested_value = float(dom.getElementsByTagName(
            'value')[0].firstChild.data)
        return requested_value


class Wibeee(Ice.Application):
    def __init__(self, requester, publisher):
        self.requester = requester
        self.publisher = publisher

    def run(self, args):     
        signal.signal(signal.SIGINT, service_shutdown)   
        self.broker = Broker(ic=self.communicator())
        
        urls = self.get_properties(prefix="Wibeee.url.")
        ids = self.get_properties(prefix="Source.ids.")

        self.requester.set_urls(urls)
        self.publisher.set_broker(self.broker)
        self.publisher.set_ids(ids)
        
        while(True):
            values = self.requester.get_average_measures(UPDATE_TIME)
            self.publisher.publish(values)

    def get_properties(self, prefix):
        properties = self.broker.props.getPropertiesForPrefix(prefix)
        self.remove_prefix_in_keys(properties, prefix) 
        return properties

    def remove_prefix_in_keys(self, dictionary, prefix):
        aux_dict = dictionary.copy()
        for key in aux_dict.keys():
            dictionary[key.split(prefix)[1]] = aux_dict[key]
            del dictionary[key]


class Publisher:
    def __init__(self, vars, meta):
        self.vars = vars
        self.meta = meta

    def set_ids(self, ids):
        self.ids = {key: ast.literal_eval(values) 
                        if values else {} for key, values in ids.items()}
        self.wib_publishers = {key: {} for key in self.ids.keys()}
        self.create_publishers()

    def set_broker(self, broker):
        self.broker = broker

    def create_publishers(self):
        assert self.wib_publishers, "WARNING: no publishers defined!"
        for key in self.ids.keys():
            self.wib_publishers[key][self.vars[0]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[0]], transducer_type="PowerSensor", meta=self.meta)
            self.wib_publishers[key][self.vars[1]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[1]], transducer_type="PowerSensor", meta=self.meta)
            self.wib_publishers[key][self.vars[2]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[2]], transducer_type="PowerSensor", meta=self.meta)
            self.wib_publishers[key][self.vars[3]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[3]], transducer_type="CurrentSensor", meta=self.meta)
            self.wib_publishers[key][self.vars[4]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[4]], transducer_type="EnergySensor", meta=self.meta)
            self.wib_publishers[key][self.vars[5]] = self.broker.get_publisher(
                source=self.ids[key][self.vars[5]], transducer_type="VoltageSensor", meta=self.meta)

    def publish(self, values):
        for key in values.keys():
            for var in values[key].keys():
                self.wib_publishers[key][var].publish(values[key][var])
                logging.info("{} [{} {}] {}".format(
                    datetime.now(), key, var, values[key][var]))
            logging.info("")


def service_shutdown(signum, frame):
    exit(0)

if __name__ == "__main__":
    vars = ["pact", "papt", "fpott", "irmst", "eact", "vrmst"]
    meta = {"latitude": "38.997947",
            "longitude": "-3.919902",
            "altitude": "639.10",
            "place": "ARCO Lab ITSI"}

    Wibeee(Requester(vars), 
            Publisher(vars, meta)).main(sys.argv)
