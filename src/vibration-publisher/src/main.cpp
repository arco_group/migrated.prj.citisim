// -*- mode: c++; coding: utf-8 -*-

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Ticker.h>

#include <ESP8266mDNS.h>   // force IDE to include this
#include <EEPROM.h>        // force IDE to include this

#include <IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>

#include "utils.h"
#include "IceStorm-min.h"
#include "iot-node.h"
#include "iot.h"

#define MAX_PROXY_SIZE      64
#define DBINDEX_IS_PROXY    DBINDEX_LAST

#define SAMPLES_PER_SECOND  50

Ticker async;
Ice_Communicator ic;
Ice_ObjectAdapter adapter;

// servants
IoT_IoTNode node;

// proxies
Ice_ObjectPrx manager;
Ice_ObjectPrx topic;
Ice_ObjectPrx publisher;

void
IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) {
    Serial.println("Node: restarting...");
    async.once(1, []() { ESP.restart(); });
}

void
IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) {
    async.once(1, []() { factory_reset(); });
}

void
IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self, Ice_String ssid, Ice_String key) {
    const char zero = 0;
    IceC_Storage_put(DBINDEX_SSID, ssid.value, ssid.size);
    IceC_Storage_put(DBINDEX_SSID + ssid.size, &zero, 1);
    IceC_Storage_put(DBINDEX_WPAKEY, key.value, key.size);
    IceC_Storage_put(DBINDEX_WPAKEY + key.size, &zero, 1);

    char ssid_[ssid.size + 1];
    strncpy(ssid_, ssid.value, ssid.size);
    ssid_[ssid.size] = 0;

    char key_[key.size + 1];
    strncpy(key_, key.value, key.size);
    key_[key.size] = 0;

    Serial.printf("WiFi: store settings, ssid: '%s', key: '%s'\n", ssid_, key_);
}

void
IoT_ISPublisherAdminI_setTopicManager(IoT_ISPublisherAdminPtr self, Ice_String is) {
    const char zero = 0;
    IceC_Storage_put(DBINDEX_IS_PROXY, is.value, is.size);
    IceC_Storage_put(DBINDEX_IS_PROXY + is.size, &zero, 1);

    char prx[is.size + 1];
    strncpy(prx, is.value, is.size);
    prx[is.size] = 0;

    Serial.printf("IS: set TopicManager to '%s'\n", prx);
}

bool
setup_publisher() {
    char strprx[MAX_PROXY_SIZE] = {0};
    IceC_Storage_get(DBINDEX_IS_PROXY, strprx, MAX_PROXY_SIZE);

    if (strprx[0] == 0) {
        Serial.println("Error: IS Manager not set!\n");
        return false;
    }

    Ice_Communicator_stringToProxy(&ic, strprx, &manager);

    new_Ice_String(topic_name, "Vibration");
    IceStorm_TopicManager_retrieve(&manager, topic_name, &topic);
    if (Ice_ObjectPrx_raisedException(&manager, "::IceStorm::NoSuchTopic")) {
        IceStorm_TopicManager_create(&manager, topic_name, &topic);
    }

    IceStorm_Topic_getPublisher(&topic, &publisher);

    if (Ice_ObjectPrx_raisedAnyException(&manager) ||
        Ice_ObjectPrx_raisedAnyException(&topic))
        return false;
    return true;
}

class Sensor {
public:
    Sensor() : _counter(0), _last(0) {
    }

    void capture() {
        unsigned long now = millis();
        if ((now - _last) < (1000 / SAMPLES_PER_SECOND))
            return;

        _data[_counter++] = map(analogRead(A0), 0, 1023, 0, 255);
        _last = now;
    }

    void send() {
        if (_counter < SAMPLES_PER_SECOND)
            return;

        _counter = 0;

        SmartObject_ByteSeq data;
        data.items = _data;
        data.size = SAMPLES_PER_SECOND;

        String id = "vib-" + String(ESP.getChipId());
        new_Ice_String(source, id.c_str());

        SmartObject_Metadata meta;
        SmartObject_Metadata_init(meta, NULL, 0);

        SmartObject_DataSink_notify(&publisher, data, source, meta);
        Serial.printf("- %d bytes sent\n", data.size);
    }

private:
    byte _data[SAMPLES_PER_SECOND];
    unsigned long _last;
    uint16_t _counter;
};

Sensor sensor;

void
setup() {
    Serial.begin(115200);
    Serial.println("\n--------\nBooting...");
    pinMode(BUILTIN_LED, OUTPUT);
    digitalWrite(BUILTIN_LED, HIGH);

    // setup WiFi, Ice, Endpoints...
    IceC_Storage_begin();
    setup_wireless();
    setup_ota();
    Ice_initialize(&ic);
    TCPEndpoint_init(&ic);

    // get publisher if online, reset if fail
    if (is_online) {
        if (not setup_publisher()) {
            prints("ERROR: can't get IS publisher\n");
            reset_node();
        }
    }

    // setup adapter and register servant
    Ice_Communicator_createObjectAdapterWithEndpoints
        (&ic, "Adapter", "tcp -p 4455", &adapter);
    Ice_ObjectAdapter_activate(&adapter);
    IoT_IoTNode_init(&node);
    Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "VibrationNode");

    Serial.println("Boot done!");
}

void
loop() {
    if (is_online) {
        sensor.capture();
        sensor.send();
    }

    handle_ota();
    check_buttons();
    Ice_Communicator_loopIteration(&ic);
}
