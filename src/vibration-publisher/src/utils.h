// -*- mode: c++; coding: utf-8 -*-

#ifndef _NODE_UTILS_H_
#define _NODE_UTILS_H_

#include <Arduino.h>

#define PIN_BUTTON          0
#define MAX_SSID_SIZE       32
#define MAX_WPAKEY_SIZE     64
#define DBINDEX_SSID        0
#define DBINDEX_WPAKEY      DBINDEX_SSID + MAX_SSID_SIZE
#define DBINDEX_LAST        DBINDEX_WPAKEY + MAX_WPAKEY_SIZE

// set to true if it is connected and can reach IceStorm
extern bool is_online;

void led_blink(byte count, byte timeout);
void reset_node();
void factory_reset();
String get_local_ip();
void setup_wireless();
void setup_ota();
void handle_ota();
void check_buttons();

#endif /* end of include guard: _NODE_UTILS_H_ */
