// -*- mode: c++; coding: utf-8 -*-

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <IceC.h>

#include "utils.h"

void
led_blink(byte count, byte timeout) {
    for (byte i=0; i<count; i++) {
        digitalWrite(BUILTIN_LED, HIGH);
        delay(timeout);
        digitalWrite(BUILTIN_LED, LOW);
        delay(timeout);
    }
}

void
reset_node() {
    Serial.println("Resetting node...");
    delay(2000);
    ESP.restart();
}

void
factory_reset() {
    Serial.println("Erasing EEPROM...");
    IceC_Storage_clear();
    Serial.println("EEPROM cleared! Restarting node...");

    led_blink(5, 50);
    ESP.restart();
}

String
get_local_ip() {
    if (WiFi.getMode() == WIFI_AP)
        return WiFi.softAPIP().toString();
    else
        return WiFi.localIP().toString();
}

void
setup_ota() {
    bool led_state = LOW;
    ArduinoOTA.onStart([]() {
        Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\nEnd");
        digitalWrite(BUILTIN_LED, HIGH);
    });
    ArduinoOTA.onProgress([&led_state](unsigned int progress, unsigned int total) {
        byte percentage = (progress / (total / 100));
        led_state = percentage % 2;
        digitalWrite(BUILTIN_LED, led_state);
        Serial.printf("Progress: %u%%\r", percentage);
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
    ArduinoOTA.begin();
}

void
handle_ota() {
    ArduinoOTA.handle();
}

void
check_buttons() {
    if (digitalRead(PIN_BUTTON) == 1)
	   return;

    bool erase = false;
    bool led_state = false;
    long start = millis();
    while (digitalRead(PIN_BUTTON) == 0) {
        delay(100);

        if (millis() - start > 3000) {
            erase = true;
            digitalWrite(BUILTIN_LED, led_state);
            led_state = !led_state;
        }
    }

    if (erase)
        factory_reset();
}

// set to true if it is connected and can other objects
bool is_online;

void
_check_wifi_connected() {
    int c = 0;
    while (WiFi.status() != WL_CONNECTED) {
    	c++;
    	Serial.print(".");
    	check_buttons();

    	if (c > 60) {
    	    Serial.println("\nWiFi: connection failed! Rebooting...");
            reset_node();
    	}
    	delay(500);
    }

    is_online = true;
}

void
setup_wireless() {
    // get stored wireless settings
    char ssid[MAX_SSID_SIZE] = {0};
    IceC_Storage_get(DBINDEX_SSID, ssid, MAX_SSID_SIZE);

    // if available, try to connect
    if (ssid[0] != 0) {
	    char key[MAX_WPAKEY_SIZE];
	    IceC_Storage_get(DBINDEX_WPAKEY, key, MAX_WPAKEY_SIZE);

	    Serial.printf("WiFi: connecting to '%s'", ssid);
        WiFi.disconnect();
        WiFi.mode(WIFI_STA);
        WiFi.enableAP(false);
        WiFi.begin(ssid, key);

        _check_wifi_connected();
        Serial.println();
    }

    // if not available, config as AP
    else {
        String ssid = "node-" + String(ESP.getChipId());
        Serial.printf("WiFi: setting up AP as '%s'\n", ssid.c_str());

        WiFi.mode(WIFI_AP);
        WiFi.enableSTA(false);
        WiFi.softAP(ssid.c_str());
    }

    Serial.printf("WiFi: ready, IP address: ");
    Serial.println(get_local_ip());
}
