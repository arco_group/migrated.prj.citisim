// -*- mode: cpp; coding: utf-8 -*-

#include <Arduino.h>
#include <Ticker.h>

#include <ArduinoOTA.h>    // force IDE to include this
#include <ESP8266WiFi.h>   // force IDE to include this
#include <ESP8266mDNS.h>   // force IDE to include this
#include <EEPROM.h>        // force IDE to include this

#include <IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

#include "iot-node.h"

#define RELAY_PIN 12
#define STATUS_LED 13

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;
Ice_ObjectPrx publisher;

// servants
IoT_IoTNode node;

void
IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) {
    async_restart_node();
}

void
IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) {
    async_factory_reset();
}

void
IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self,
                         Ice_String ssid,
                         Ice_String key) {
    store_wifi_settings(ssid, key);
}

void SmartObject_ObservableI_setObserver(SmartObject_ObservablePtr self, Ice_String Observer){
    char observer_prx[Observer.size];
    strncpy(observer_prx, Observer.value, Observer.size);
    Ice_Communicator_stringToProxy(&ic, observer_prx, &publisher);
}

void
SmartObject_DigitalSinkI_notify(SmartObject_DigitalSinkPtr self,
                              Ice_Bool value,
                              Ice_String source,
                              SmartObject_Metadata data) {
    value ? Serial.print("Turning on ") : Serial.print("Turning off ");                           
    Serial.println("the lamp");
    value ? digitalWrite(RELAY_PIN, HIGH) : digitalWrite(RELAY_PIN, LOW); 
}


void setup() {
    Serial.begin(115200);
    Serial.flush();
    pinMode(STATUS_LED, OUTPUT);
    pinMode(RELAY_PIN, OUTPUT);
    delay(2000);
    Serial.println("\n------\nBooting...\n");

    // setup WiFi, Ice, Endpoints...
    IceC_Storage_begin();

    setup_wireless();

    Ice_initialize(&ic);
    TCPEndpoint_init(&ic);

    // setup adapter and register servants
    Ice_Communicator_createObjectAdapterWithEndpoints
        (&ic, "Adapter", "tcp -p 4455", &adapter);
    Ice_ObjectAdapter_activate(&adapter);
    
    IoT_IoTNode_init(&node);


    // FIXME: add interface to set these identities
    Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");
    Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, STR(TRANSDUCER_ADDR));

    Ice_Communicator_setDefaultLocator(&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");

    if (is_online) {
        Ice_ObjectPrx welcome;
        Ice_Communicator_stringToProxy(&ic, "WelcomeServer -o @ WelcomeServiceAdapter", &welcome);
        String proxy = STR(TRANSDUCER_ADDR) " -o -e 1.0:tcp -h " + get_local_ip() + " -p 4455";
        Serial.printf("Proxy: %s\n", proxy.c_str());
        new_Ice_String(address, proxy.c_str());
        new_Ice_String(type, STR(TRANSDUCER_TYPE));
        SmartObject_WelcomeService_hello(&welcome, address, type);
    }

    Serial.println("Boot done!\n");
}

void loop() {
  check_buttons();
  Ice_Communicator_loopIteration(&ic);
}