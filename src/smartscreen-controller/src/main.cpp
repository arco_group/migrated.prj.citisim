// -*- mode: cpp; coding: utf-8 -*-

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <string>
#include <map>

#include <IceC/IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

// NOTE: remember to run 'make' to generate this file
#include "panel-controller.h"

#define EXIT_LEFT 0
#define EXIT_RIGHT 1
#define EXIT_DOWN 2
#define NOT_PASS 3
#define CITISIM 4

// A map to save relation between images names and images ids 
std::map<std::string, int> images;

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;
Ice_ObjectPrx publisher;

// servants
Panel_PanelController node;

void init_images() {
  images["left"] = EXIT_LEFT;
  images["right"] = EXIT_RIGHT;
  images["down"] = EXIT_DOWN;
  images["not"] = NOT_PASS;
  images["citi"] = CITISIM;
}

void setup() {
  Serial.begin(115200);
  Serial.flush();

  digitalWrite(LED_BUILTIN, LOW);
  
  delay(1000);
  Serial.println("\n------Booting---------\n");

  // setup WiFi, Ice, Endpoints...
  IceC_Storage_begin();
  setup_wireless();
  setup_ota();
  init_images();

  Ice_initialize(&ic);
  TCPEndpoint_init(&ic);

  // set Panel locator
  Ice_Communicator_setDefaultLocator
    (&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");

  // setup adapter and register servants
  Ice_Communicator_createObjectAdapterWithEndpoints
    (&ic, "Adapter", "tcp -p 4455", &adapter);
  Ice_ObjectAdapter_activate(&adapter);
  Panel_PanelController_init(&node);

  // register twice to be able to connect it even if ADDR is unknown
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, STR(TRANSDUCER_ADDR));

  // register on Welcome Service
  if (is_online) {
    Ice_ObjectPrx welcome;
    Ice_Communicator_stringToProxy(&ic, "WelcomeServer -o @ WelcomeServiceAdapter", &welcome);
    String proxy = STR(TRANSDUCER_ADDR) " -o -e 1.0:tcp -h " + get_local_ip() + " -p 4455";
    new_Ice_String(address, proxy.c_str());
    new_Ice_String(type, STR(TRANSDUCER_TYPE));
    SmartObject_WelcomeService_hello(&welcome, address, type);
  }

  // everything is fine, say it to the world!
  Serial.println("\n------Boot done!------\n");
  led_blink(5, 50);
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  Ice_Communicator_loopIteration(&ic);
  handle_ota();
  check_buttons();
}

// redirect callbacks (needed here to overwrite __weak__ defs)
void IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) { async_restart_node(); }
void IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) { async_factory_reset(); }
void IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self, Ice_String ssid, Ice_String key) {
    store_wifi_settings(ssid, key);
}

void SmartObject_ObservableI_setObserver(SmartObject_ObservablePtr self, Ice_String Observer){
  char observer_prx[Observer.size];
  strncpy(observer_prx, Observer.value, Observer.size);
  Ice_Communicator_stringToProxy(&ic, observer_prx, &publisher);
}

void SmartServices_MessageRenderI_render(SmartServices_MessageRenderPtr self, Ice_String message) {
  
  std::string img_name(message.value, message.size);
  
  /* The commnad that must be sent by the serial port in order to change the image is:
        0x55 0xaa 0x02 <image_id>
      
     There are five images with their respective id
      EXIT_LEFT:  0
      EXIT_RIGHT: 1
      EXIT_DOWN:  2
      NOT_PASS:   3
      CITISIM:    4
  */

  Serial.write(0x55);
  Serial.write(0xaa);
  Serial.write(0x02);

  Serial.write(images[img_name]);
}