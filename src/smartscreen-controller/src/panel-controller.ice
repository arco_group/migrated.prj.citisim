// // -*- mode: c++; coding: utf-8 -*-

#include <iot/node.ice>
#include <citisim/iot.ice>

module SmartServices {
    interface MessageRender {
        void render(string messageID);
        void clear();
    };
};

module Panel {
    interface PanelController extends
        IoT::NodeAdmin,
        IoT::WiFiAdmin,
        SmartObject::Observable,
        SmartServices::MessageRender {
    };
};

