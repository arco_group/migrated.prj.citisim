/* -*- mode: js; coding: utf-8 -*- */

_ = console.info.bind(console);
var nodeapp = angular.module('NodeApp', [], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

// https://stackoverflow.com/a/29594677/870503
nodeapp.directive('focusOnShow', function($timeout) {
    return {
        restrict: 'A',
        link: function($scope, $element, $attr) {
            if ($attr.ngShow){
                $scope.$watch($attr.ngShow, function(newValue){
                    if(newValue){
                        $timeout(function(){
                            $element[0].focus();
                        }, 0);
                    }
                })
            }
            if ($attr.ngHide){
                $scope.$watch($attr.ngHide, function(newValue){
                    if(!newValue){
                        $timeout(function(){
                            $element[0].focus();
                        }, 0);
                    }
                })
            }
        }
    };
})

nodeapp.controller("NodeCtrl", function($scope, $filter) {

    function show_error(msg) {
	// FIXME: use an error message system
	_(msg);
    };

    // FIXME: get from settings
    $scope.ps_proxy = "PropertyServer -t:tcp -h pike.esi.uclm.es -p 4334";
    $scope.new_node = {};

    $scope.on_new_node = function() {
	$.post("/node/add/", $scope.new_node)
	    .fail(function(resp) {
		show_error("Error: could not process request, "
			   + resp.responseJSON.message);
	    })
	    .done(function() {
		$scope.new_node = {};
		$scope.on_system_scan();
	    });
    };

    $scope.on_remove_node = function() {
	if (!confirm("Are you sure?"))
	    return;

	$.post("/node/" + $scope.selected.id + "/remove/")
	    .fail(function(resp) {
		show_error("Error: could not remove node. "
			   + resp.responseJSON.message);
	    })
	    .done(function() {
		$scope.on_system_scan();
	    });
    };

    $scope.on_edit_node = function() {
	$scope.edit_endpoints = false;
	$scope.edit_name = false;

	var args = {
	    endpoints: $scope.selected.endpoints,
	    name: $scope.selected.name,
	};

	$.post("/node/" + $scope.selected.id + "/update/", args)
	    .fail(function(resp) {
		show_error("Error: could not update node. "
			   + resp.responseJSON.message);
	    });
    };

    $scope.on_system_scan = function() {
	$scope.selected = null;

	$.getJSON("/node/scan/", function(nodes) {
	    $scope.nodes = {};
	    for (var i in nodes) {
		var n = nodes[i];
		$scope.nodes[n.id] = n;

		// add object's reverse lookup flags
		n.object_ids = "";
		for (var id in n.objects) {
		    n.object_ids += id + ", ";
		    var ifaces = n.objects[id];
		    for (var j in ifaces) {
			var iface = ifaces[j].replace(/\./g, "_");
			n["id_for_" + iface] = id;
		    }
		}

		// remove last ','
		n.object_ids = n.object_ids.slice(0, -2);
	    }

	    // DEBUG only (select first)
	    $scope.selected = $scope.nodes[Object.keys($scope.nodes)[0]];

	    $scope.$apply();

	}).fail(function(resp) {
	    show_error("Error: could not process request, "
		       + resp.responseJSON.message);
	});
    };

    // DEBUG only
    $scope.on_system_scan();

    $scope.set_selected = function(node) {
    	$scope.selected = node;
    };

    $scope.on_EventSink_notify = function() {
	var args = {
	    node_proxy: $scope.selected.id_for_SmartObject_EventSink +
		" -o:" + $scope.selected.endpoints,
	};

	$.get("/node/" + $scope.selected.id + "/eventsink/notify/", args)
	    .fail(function(resp) {
		show_error("Error: could not send request. "
			   + resp.responseJSON.message);
	    });
    };

    $scope.digital_sink_state = false;
    $scope.on_DigitalSink_notify = function() {
	var args = {
	    value: $scope.digital_sink_state,
	    node_proxy: $scope.selected.id_for_SmartObject_DigitalSink +
		" -o:" + $scope.selected.endpoints,
	};

	$.get("/node/" + $scope.selected.id + "/digitalsink/notify/", args)
	    .fail(function(resp) {
		show_error("Error: could not send request. "
			   + resp.responseJSON.message);
	    });
    };

    $scope.on_WiFiAdmin_setupWiFi = function() {
	var args = {
	    node_proxy: $scope.selected.id_for_IoT_WiFiAdmin +
		" -o:" + $scope.selected.endpoints,
	    ssid: $scope.selected.WiFiAdmin.ssid,
	    key: $scope.selected.WiFiAdmin.key,
	};

	$.post("/node/" + $scope.selected.id + "/wifiadmin/setup/", args)
	    .fail(function(resp) {
		show_error("Error: could not change WiFi settings. "
			   + resp.responseJSON.message);
	    });
    };

    $scope.on_NodeAdmin_restart = function() {
	var args = {
	    node_proxy: $scope.selected.id_for_IoT_NodeAdmin +
		" -o:" + $scope.selected.endpoints,
	};

	$.get("/node/" + $scope.selected.id + "/nodeadmin/restart/", args)
	    .fail(function(resp) {
		show_error("Error: could not send request. "
			   + resp.responseJSON.message);
	    });
    };

    $scope.on_NodeAdmin_factoryReset = function() {
	var args = {
	    node_proxy: $scope.selected.id_for_IoT_NodeAdmin +
		" -o:" + $scope.selected.endpoints,
	};

	$.post("/node/" + $scope.selected.id + "/nodeadmin/factoryreset/", args)
	    .fail(function(resp) {
		show_error("Error: could not send request. "
			   + resp.responseJSON.message);
	    })
	    .done(function() {
		$scope.on_system_scan();
	    });
    };

    $scope.on_ISPublisher_setTopicManager = function() {
	var args = {
	    node_proxy: $scope.selected.id_for_IoT_NodeAdmin +
		" -o:" + $scope.selected.endpoints,
	    topic_manager: $scope.selected.ISPublisherAdmin.TopicManager,
	};

	$.post("/node/" + $scope.selected.id + "/ispublisher/settopicmanager/", args)
	    .fail(function(resp) {
		show_error("Error: could not set Topic Manager proxy. "
			   + resp.responseJSON.message);
	    });

    };

});
