#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import os
import argparse
import Ice
import logging
import subprocess
import json
from datetime import datetime
from threading import Thread, Event
from flask import Flask, render_template, request
from flask.json import jsonify

Ice.loadSlice("/usr/share/slice/PropertyService/PropertyService.ice")
import PropertyService  # noqa

Ice.loadSlice("/usr/share/slice/iot/node.ice")
import IoT  # noqa

pwd = os.path.abspath(os.path.dirname(__file__))
so_slice = os.path.join(pwd, "../../slice/iot.ice")
if not os.path.exists(so_slice):
    so_slice = "/usr/share/slice/citisim/iot.ice"
Ice.loadSlice(so_slice)
import SmartObject  # noqa

logging.basicConfig(level=logging.INFO)
pwd = os.path.abspath(os.path.dirname(__file__))


class BadRequest(Exception):
    def __init__(self, msg):
        self.msg = msg


class WebServer(Ice.Application):
    def run(self, args):
        try:
            self.args = self.parse_args(args)
        except SystemExit:
            return 1

        self.ic = self.communicator()
        self.shutdownOnInterrupt()
        self.should_stop = Event()

        self.load_settings()
        self.create_flask_app()
        self.setup_handlers()
        self.run_browser()
        self.run_webserver()
        self.event_loop()

    def parse_args(self, args):
        parser = argparse.ArgumentParser()
        parser.add_argument("--host", default="0.0.0.0",
                            help="host address where web server will listen")
        parser.add_argument("--port", type=int, default=7154,
                            help="port number where web server will listen")
        parser.add_argument("--disable-flask-log", action="store_true",
                            help="disable logging of web server")
        parser.add_argument("--debug", action="store_true",
                            help="enable Flask debugging")
        parser.add_argument("--browser", action="store_true",
                            help="run also a chromium web browser")

        return parser.parse_known_args(args)[0]

    def create_flask_app(self):
        assets = os.path.join(pwd, "webui")
        if not os.path.exists(assets):
            assets = "/usr/share/iot-node-controller/webui"
        templates = os.path.join(assets, "templates")
        static = os.path.join(assets, "static")
        self.app = Flask(__name__, static_folder=static, template_folder=templates)

        if self.args.disable_flask_log:
            werkzeug = logging.getLogger('werkzeug')
            werkzeug.setLevel(logging.WARNING)

    def load_settings(self):
        self.settings = {}

        home = os.environ.get('HOME', '')
        config = os.path.join(home, '.config/node-controller/settings.conf')
        if not os.path.isfile(config):
            return

        with open(config) as src:
            exec(src.read(), {}, self.settings)

    def run_webserver(self):
        if self.args.disable_flask_log:
            url = "http://{}:{}/".format(self.args.host, self.args.port)
            logging.info(" WebUI ready, visit '{}'".format(url))

        logging.info(" waiting events...")
        args = dict(
            threaded = True,
            debug = self.args.debug,
            host = self.args.host,
            port = self.args.port
        )

        if self.args.debug:
            self.app.run(**args)
        else:
            web_server = Thread(target=self.app.run, kwargs=args, daemon=True)
            web_server.start()

    def run_browser(self):
        if not self.args.browser:
            return

        home = os.getenv("HOME")
        url = "http://127.0.0.1:7154/"
        user_data_dir = "{}/.config/node-controller/browser/".format(home)
        browser = "chromium-browser"
        if not os.path.exists("/usr/bin/" + browser):
            browser = "google-chrome"

        cmd = ("{} "
               "--disable-translate "
               "--disable-popup-blocking "
               "--user-data-dir={} "
               "--disable-session-crashed-bubble "
               "--no-first-run "
               "--window-size=1080,900 "
               "--app={}".format(browser, user_data_dir, url))

        def func():
            subprocess.call(cmd, shell=True)
            self.should_stop.set()

        Thread(target=func, daemon=True).start()

    def event_loop(self):
        self.should_stop.wait()
        self.ic.shutdown()
        self.ic.destroy()

    @property
    def ps_proxy(self):
        try:
            return self._ps_proxy
        except AttributeError:
            pass

        self._ps_proxy = None
        ps = self.settings.get('ps_proxy')
        if ps is None:
            return

        ps = self.ic.stringToProxy(ps)
        self._ps_proxy = PropertyService.PropertyServerPrx.uncheckedCast(ps)
        return self._ps_proxy

    def set_properties(self, path, props):
        if not self.ps_proxy:
            raise BadRequest("There is no Property Service available")

        self.ps_proxy.set(path, json.dumps(props))

    def remove_properties(self, path):
        if not self.ps_proxy:
            raise BadRequest("There is no Property Service available")

        try:
            self.ps_proxy.remove(path)
        except PropertyService.PropertyException as e:
            if e.reason != PropertyService.ExceptionReason.PropertyNotFound:
                raise e

    def catch_bad_request(self, fn):
        def deco(*args, **kwargs):
            try:
                return fn(*args, **kwargs)
            except BadRequest as e:
                response = jsonify({'message': e.msg})
                response.status_code = 400
                return response
        deco.__name__ = fn.__name__
        return deco

    def register_node_on_ps(self, sn, endpoint, objects, name=None):
        if name is None:
            name = "Node-" + str(sn)

        node = dict(
            id = str(sn),
            iotnode = True,
            name = name,
            status = 'active',
            endpoints = endpoint,
            date = int(datetime.now().timestamp() * 1000),
            objects = objects,
        )

        node = json.dumps(node)
        self.ps_proxy.set(str(sn), node)
        return node

    def setup_handlers(self):
        @self.app.route("/")
        def index():
            return render_template("index.html")

        @self.app.route("/node/add/", methods=['POST'])
        @self.catch_bad_request
        def add_node():
            if not self.ps_proxy:
                raise BadRequest("There is no Property Service available")

            name = request.form.get('name')
            snumber = request.form.get('sn')
            endpoint = request.form.get('endpoint')
            try:
                objects = {}
                for line in request.form['objects'].split('\n'):
                    identity, ifaces = line.split(":")
                    objects[identity] = list(map(str.strip, ifaces.split(',')))
            except Exception as e:
                logging.error(e)
                raise BadRequest("Invalid objects string format")

            node = self.register_node_on_ps(snumber, endpoint, objects, name)
            return node

        @self.app.route("/node/<node_id>/remove/", methods=['POST'])
        @self.catch_bad_request
        def remove_node(node_id):
            self.remove_properties(node_id)
            return "OK"

        @self.app.route("/node/<node_id>/update/", methods=['POST'])
        @self.catch_bad_request
        def update_node(node_id):
            if not self.ps_proxy:
                raise BadRequest("There is no Property Service available")

            for key, value in request.form.items():
                self.set_properties(node_id + " | " + key, value)
            return "OK"

        @self.app.route("/node/scan/")
        @self.catch_bad_request
        def ps_scan():
            if not self.ps_proxy:
                raise BadRequest("There is no Property Service available")

            nodes = {}
            props = json.loads(self.ps_proxy.get(""))
            for k, v in props.items():
                if v.get('iotnode'):
                    nodes[k] = v
            return json.dumps(nodes)

        def get_node_proxy():
            proxy = request.args.get('node_proxy')
            if proxy is None:
                proxy = request.form.get('node_proxy')
            if proxy is None:
                raise BadRequest("Missing argument: 'node_proxy'")

            proxy = self.ic.stringToProxy(proxy)
            proxy = proxy.ice_encodingVersion(Ice.Encoding_1_0)
            proxy = proxy.ice_timeout(10000)
            return proxy

        @self.app.route("/node/<node_id>/eventsink/notify/")
        @self.catch_bad_request
        def eventsink_notify(node_id):
            proxy = get_node_proxy()
            proxy = SmartObject.EventSinkPrx.uncheckedCast(proxy)

            try:
                proxy.notify("NodeCtrl", {})
                return "OK"
            except Exception as e:
                raise BadRequest("Cant invoke method: " + str(e))

        @self.app.route("/node/<node_id>/digitalsink/notify/")
        @self.catch_bad_request
        def digitalsink_notify(node_id):
            proxy = get_node_proxy()
            proxy = SmartObject.DigitalSinkPrx.uncheckedCast(proxy)
            value = request.args.get("value") != "false"

            try:
                proxy.notify(value, "NodeCtrl", {})
                return "OK"
            except Exception as e:
                raise BadRequest("Cant invoke method: " + str(e))

        @self.app.route("/node/<node_id>/wifiadmin/setup/", methods=['POST'])
        @self.catch_bad_request
        def wifiadmin_setup(node_id):
            settings = {
                'ssid': request.form.get("ssid"),
                'key': request.form.get("key"),
            }

            proxy = get_node_proxy()
            proxy = IoT.WiFiAdminPrx.uncheckedCast(proxy)
            proxy.setupWiFi(settings['ssid'], settings['key'])

            self.set_properties(node_id + " | WiFiAdmin", settings)
            return "OK"

        @self.app.route("/node/<node_id>/nodeadmin/restart/")
        @self.catch_bad_request
        def nodeadmin_restart(node_id):
            proxy = get_node_proxy()
            proxy = IoT.NodeAdminPrx.uncheckedCast(proxy)
            proxy.restart()
            logging.info("Restarting node...")
            return "OK"

        @self.app.route("/node/<node_id>/nodeadmin/factoryreset/", methods=['POST'])
        @self.catch_bad_request
        def nodeadmin_factoryReset(node_id):
            proxy = get_node_proxy()
            proxy = IoT.NodeAdminPrx.uncheckedCast(proxy)
            proxy.factoryReset()

            self.remove_properties(node_id + " | WiFiAdmin")
            self.remove_properties(node_id + " | ISPublisherAdmin")
            return "OK"

        @self.app.route("/node/<node_id>/ispublisher/settopicmanager/", methods=['POST'])
        @self.catch_bad_request
        def ispublisheradmin_setTopicManager(node_id):
            settings = {
                'TopicManager': request.form.get("topic_manager"),
            }

            proxy = get_node_proxy()
            proxy = IoT.ISPublisherAdminPrx.uncheckedCast(proxy)
            proxy.setTopicManager(settings['TopicManager'])

            self.set_properties(node_id + " | ISPublisherAdmin", settings)
            return "OK"


if __name__ == "__main__":
    WebServer().main(sys.argv)
