Documentation
=============

You can see more docs about this software in the official Citisim documentation site:

* http://pike.esi.uclm.es:8012/
* [Citisim Manager Tool: getting started](http://pike.esi.uclm.es:8012/recipe/citisim_manager_tool_getting_started/)

Settings
========

Create a file called ~/.config/node-controller/settings.conf, and put the following:

    ps_proxy = "PropertyServer -t:tcp -h pike.esi.uclm.es -p 4334"

Change to suit your needs.



