#!/bin/bash
# -*- mode: sh; coding: utf-8 -*-

echo -n "IceC... "
if [ ! -d IceC ]; then
    wget -q https://bitbucket.org/arco_group/icec/downloads/arduino-icec-latest.zip
    unzip -qq arduino-icec-latest.zip -d IceC
    rm arduino-icec-latest.zip
    echo "[ok] "
else
    echo "[already present]"
fi

echo -n "IceC-IoT-Node... "
if [ ! -d IceC-IoT-Node ]; then
    wget -q https://bitbucket.org/arco_group/iot.node/downloads/icec-iot-node-latest.zip
    unzip -qq icec-iot-node-latest.zip -d IceC-IoT-Node
    rm icec-iot-node-latest.zip
    echo "[ok] "
else
    echo "[already present]"
fi

echo -n "HLW8012... "
if [ ! -d hlw8012 ]; then
    git clone https://github.com/xoseperez/hlw8012 > /dev/null 2>&1
    echo "[ok] "
else
    echo "[already present]"
fi
