// -*- mode: c++; coding: utf-8 -*-

#include <iot/node.ice>

module IoT {
    interface IoTNode extends NodeAdmin, ISPublisherAdmin, WiFiAdmin {
    };
};
