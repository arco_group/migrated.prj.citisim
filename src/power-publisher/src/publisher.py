#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceStorm

from libcitisim import SmartObject


class DataSinkI(SmartObject.DataSink):
    def notify(self, data, source, meta, current):
        print("{} bytes arrived from {}".format(len(data), source))


class Publisher(Ice.Application):
    def run(self, args):
        topic = self.get_topic("Power")
        pub = topic.getPublisher()
        pub = SmartObject.DataSinkPrx.uncheckedCast(pub)

        data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        source = "422560"
        meta = {SmartObject.MetadataField.Delta: "3"}
        pub.notify(data, source, meta)

    def get_topic(self, name):
        ic = self.communicator()
        mgr = ic.propertyToProxy("TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.checkedCast(mgr)

        try:
            return mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            return mgr.create(name)


if __name__ == "__main__":
    Publisher().main(sys.argv)
