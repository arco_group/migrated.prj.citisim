// -*- mode: cpp; coding: utf-8 -*-

#include <Arduino.h>
#include <Ticker.h>
#include <HLW8012.h>

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h> 
#include <EEPROM.h>  

#include <IceC/IceC.h>
#include <IceC/Locator.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

#include "IceStorm-min.h"
#include "iot-node.h"
#include "iot.h"

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;

// servants
IoT_IoTNode node;

// proxies
Ice_ObjectPrx manager;
Ice_ObjectPrx topics[5];
Ice_ObjectPrx publisher[5];

//SENSOR PINS
HLW8012 sensor;
#define SEL_PIN                         5
#define CF1_PIN                         13
#define CF_PIN                          14

#define CURRENT_MODE                    HIGH
#define CURRENT_RESISTOR                0.001
#define VOLTAGE_RESISTOR_UPSTREAM       (5 * 470000)
#define VOLTAGE_RESISTOR_DOWNSTREAM     1000

#define UPDATE_TIME                     (5 * 60000) // 1 publication every 5 minutes
#define NUM_OF_READS                    5   // 5 reads every notify
#define NUM_OF_SENSORS                  5   // 5 sensors by device

//SONOFF POW PINS
#define RELAY_PIN          12
#define PIN_BUTTON          0

float **sensors_readings = (float **) malloc(NUM_OF_SENSORS*sizeof(float));
float *sensors_means = (float *) malloc(NUM_OF_SENSORS*sizeof(float));

const char* topic_names [NUM_OF_SENSORS] = {"ActivePower", "Voltage", "Current", "ApparentPower", "PowerFactor"};
const char* sensors_id [NUM_OF_SENSORS] = {"0A06175100000038", "0A06175100000039", "0A06175100000040", "0A06175100000041", "0A06175100000042"};

// When using interrupts we have to call the library entry point
// whenever an interrupt is triggered
void ICACHE_RAM_ATTR
hlw8012_cf1_interrupt() {
    sensor.cf1_interrupt();
}

void ICACHE_RAM_ATTR
hlw8012_cf_interrupt() {
    sensor.cf_interrupt();
}

void
setup_sensor() {
    sensor.begin(CF_PIN, CF1_PIN, SEL_PIN, CURRENT_MODE, true);
    sensor.setResistors(CURRENT_RESISTOR,
                        VOLTAGE_RESISTOR_UPSTREAM,
                        VOLTAGE_RESISTOR_DOWNSTREAM);

    attachInterrupt(CF1_PIN, hlw8012_cf1_interrupt, CHANGE);
    attachInterrupt(CF_PIN, hlw8012_cf_interrupt, CHANGE);
}

void
meanValues() {
    for(int i=0; i<NUM_OF_READS; i++){
        sensors_means[i] = 0.0;
    }
   
    for (int i=0; i<NUM_OF_READS; i++){
        for (int j=0; j<NUM_OF_SENSORS; j++){
            sensors_means[j] += sensors_readings[i][j];
        }       
    }
    
    for (int i=0; i<NUM_OF_SENSORS; i++){
        sensors_means[i] = sensors_means[i]/(float)NUM_OF_READS;
    }
}

void
send_readings() {
    meanValues();

    // metadata information
    SmartObject_Metadata_Pair items[7];
    SmartObject_Metadata meta;
    SmartObject_Metadata_init(meta, items, 7);

    String str_delta = String(UPDATE_TIME / 1000);
    new_Ice_String(delta, str_delta.c_str());
    Ice_Dict_set(meta, 0, MetadataField_Delta, delta);

    String str_size = String(sizeof(Ice_Int));
    new_Ice_String(size, str_size.c_str());
    Ice_Dict_set(meta, 1, MetadataField_Size, size);

    String str_expiration = "15";
    new_Ice_String(expiration, str_expiration.c_str());
    Ice_Dict_set(meta, 2, MetadataField_Expiration, expiration);

    String str_latitude = "38.997947";
    new_Ice_String(latitude, str_latitude.c_str());
    Ice_Dict_set(meta, 3, MetadataField_Latitude, latitude);

    String str_longitude = "-3.919902";
    new_Ice_String(longitude, str_longitude.c_str());
    Ice_Dict_set(meta, 4, MetadataField_Longitude, longitude);

    String str_altitude = "639.10";
    new_Ice_String(altitude, str_altitude.c_str());
    Ice_Dict_set(meta, 5, MetadataField_Altitude, altitude);

    String str_place = "ARCO Lab ITSI";
    new_Ice_String(place, str_place.c_str());
    Ice_Dict_set(meta, 6, MetadataField_Place, place);
    
    for (int i=0; i<NUM_OF_SENSORS; i++){
        // identifier of this event source
        new_Ice_String(source, sensors_id[i]);

        SmartObject_AnalogSink_notify(&publisher[i], sensors_means[i], source, meta);
        Serial.printf("Sensor: %s - Value: %f\n", sensors_id[i], sensors_means[i]);
    }
}

void
read_sensor() {
    static unsigned long last = millis();
    static unsigned short counter = 0;

    // This UPDATE_TIME should be at least twice the minimum time for
    // the current or voltage signals to stabilize. Experimentally
    // that's about 1 second.
    if ((millis() - last) > UPDATE_TIME) {
        last = millis();

        for (int i=0; i<NUM_OF_READS; i++){
            sensors_readings[i][0] = sensor.getActivePower();
            sensors_readings[i][1] = sensor.getVoltage();
            sensors_readings[i][2] = sensor.getCurrent();
            sensors_readings[i][3] = sensor.getApparentPower();
            sensors_readings[i][4] = (100 * sensor.getPowerFactor());            
            counter++;
        }

        if (counter >= NUM_OF_READS) {
            send_readings();
            counter = 0;
        }
    }
}

void
IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) {
    async_restart_node();
}

void
IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) {
    async_factory_reset();
}

void
IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self,
                         Ice_String ssid,
                         Ice_String key) {
    store_wifi_settings(ssid, key);
}

void
IoT_ISPublisherAdminI_setTopicManager(IoT_ISPublisherAdminPtr self,
                                      Ice_String proxy) {
    store_topic_manager(proxy);
}

bool
setup_publishers() {
    if (not create_topic_manager(&ic, &manager))
        return false;
    
    for (int i=0; i<NUM_OF_SENSORS; i++){
        if (not get_topic(&manager, topic_names[i], &topics[i]))
            return false;

        if (not get_publisher(&topics[i], &publisher[i]))
            return false;
    }

    return true;
}

void setup() {
    Serial.begin(115200);
    Serial.flush();

    pinMode(STATUS_LED, OUTPUT);
    pinMode(RELAY_PIN, OUTPUT);
    digitalWrite(RELAY_PIN, HIGH);

    for (int i=0; i<NUM_OF_SENSORS; i++){
        sensors_readings[i] = (float *) malloc(NUM_OF_READS*sizeof(float));
    }
    
    delay(1000);
    Serial.println("\n------Booting------\n");

    // setup WiFi, Ice, Endpoints...
    IceC_Storage_begin();

    setup_wireless();
    setup_ota();
    setup_sensor();

    Ice_initialize(&ic);
    TCPEndpoint_init(&ic);

    // setup adapter and register servants
    Ice_Communicator_createObjectAdapterWithEndpoints
        (&ic, "Adapter", "tcp -p 4455", &adapter);
    Ice_ObjectAdapter_activate(&adapter);
    IoT_IoTNode_init(&node);

    // FIXME: add interface to set these identities
    Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");

    // Set Citisim locator
    Ice_Communicator_setDefaultLocator
        (&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");
   
    Serial.println("\n------Boot done!------\n");
}

void loop() {
    Ice_Communicator_loopIteration(&ic);
    handle_ota();
    check_buttons();

    //get publisher if online, reset if fail
    if (is_online) {
        if (not setup_publishers()) {
            prints("ERROR: can't get IS publisher\n");
            reset_node();
        }
        else
        {
            read_sensor();
        }
    }
}