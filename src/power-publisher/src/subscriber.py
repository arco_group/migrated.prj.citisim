#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import struct
from array import array
import Ice
import IceStorm

from libcitisim import SmartObject


class DataSinkI(SmartObject.DataSink):
    def notify(self, data, source, meta, current):
        size = int(meta.get(SmartObject.MetadataField.Size, 4))
        count = int(len(data) / size)
        print("{} bytes ({} items) arrived from {}".format(len(data), count, source))

        print("- values: ", end='')
        for i in range(0, len(data), 4):
            bs = array('B', data[i:i + 4])
            value = struct.unpack('>I', bs)[0]
            print(value, end=' ')
        print()


class Subscriber(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapterWithEndpoints("Adapter", "tcp")
        adapter.activate()
        subscriber = adapter.addWithUUID(DataSinkI())

        topic = self.get_topic("Power")
        topic.subscribeAndGetPublisher({}, subscriber)

        print("Ready, waiting events...")
        self.shutdownOnInterrupt()
        ic.waitForShutdown()

    def get_topic(self, name):
        ic = self.communicator()
        mgr = ic.propertyToProxy("TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.checkedCast(mgr)

        try:
            return mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            return mgr.create(name)


if __name__ == "__main__":
    Subscriber().main(sys.argv)
