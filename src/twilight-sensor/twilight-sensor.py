#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import logging
import asyncio
import signal
import ephem
import math
import datetime

from libcitisim import Broker

logger = logging.getLogger("twilight-sensor")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


# FIXME: twilight happens twice in a day, but this sensor emits:
# - 1 if goes from day to night
# - 0 if goes from night to day


def run_interval(delta):
    def decorator(fn):
        async def _async_call(*args, **kwargs):
            while True:
                try:
                    fn(*args, **kwargs)
                    await asyncio.sleep(delta)
                except asyncio.CancelledError:
                    asyncio.get_event_loop().stop()
                    break
        return _async_call
    return decorator


class TwilightSensor(Ice.Application):
    def run(self, args):
        self.publish_everytime = False
        if len(args) > 1 and args[1] == "--publish-everytime":
            self.publish_everytime = True

        broker = Broker("", ic=self.communicator())
        period = self.get_property("Sensor.Measure.DeltaSeconds", 60)

        # get specific sensor properties
        sensor = self.get_property("Sensor.Description")
        if sensor is None:
            return 1
        source, place, lat, lon = map(str.strip, sensor.split(";"))

        # create ephem observer, which is fixed
        self.observer = ephem.Observer()
        self.observer.lat = lat
        self.observer.lon = lon
        self.last_is_twilight = self.is_twilight

        topic = self.get_property("Sensor.Publish.TopicName", "Twilight")
        meta = dict(
            quality=255,
            expiration=period,
            latitude=lat,
            longitude=lon,
            place=place,
        )
        self.pub = broker.get_publisher(
            source=source, transducer_type="TwilightSensor", meta=meta)

        # setup Ctrl+C and signal handlers
        loop = asyncio.get_event_loop()

        def stop():
            logger.info("Stopping service...")
            for task in asyncio.Task.all_tasks():
                task.cancel()

        for s in (signal.SIGINT, signal.SIGTERM):
            loop.add_signal_handler(s, stop)

        logger.info("Service running...")
        loop.create_task(run_interval(period)(self.publish_events)())
        loop.run_forever()
        loop.close()

    def publish_events(self):
        if not self.publish_everytime:
            if self.is_twilight == self.last_is_twilight:
                return

        self.last_is_twilight = self.is_twilight
        self.pub.publish(self.is_twilight)
        logger.info("Publishing event: {}".format(self.is_twilight))

    @property
    def is_twilight(self):
        sun = ephem.Sun()
        self.observer.date = datetime.datetime.utcnow()
        sun.compute(self.observer)
        degrees = sun.alt * 180 / math.pi

        # -6º: civil, -12º: nautical, -18º: astronomical
        return degrees < -6

    def get_property(self, key, default=None):
        props = self.communicator().getProperties()
        retval = props.getProperty(key)
        if retval is "":
            logging.warn(" property '{}' not set!".format(key))
            if default is not None:
                logging.warn(" - using default value: {}".format(default))
            return default
        if default is None:
            return retval
        return default.__class__(retval)


if __name__ == "__main__":
    TwilightSensor().main(sys.argv)
