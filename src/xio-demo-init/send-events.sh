#!/bin/bash

echo "NOTE: Please, ensure you are connected to uclmiot wifi"

CFG="--Ice.Config=pike.config"
MR_CLIENT="../dummies/smart-screen/messagerender-client.py"
SPK_CLIENT="../dummies/speaker/messagerender-client.py"
PSGET="prop-tool -p PropertyServer $CFG get "

# Set screens to proper messages
SCREEN1_PRX=$($PSGET "0A06175100000111|proxy" | tr -d '\"') # primera del pasillo
SCREEN2_PRX=$($PSGET "0A06175100000112|proxy" | tr -d '\"') # laboratorio
SCREEN3_PRX=$($PSGET "0A06175100000113|proxy" | tr -d '\"') # segunda del pasillo
SCREEN4_PRX=$($PSGET "0A06175100000114|proxy" | tr -d '\"') # abajo

echo "- setting 0A06175100000111 screen..."
$MR_CLIENT "$SCREEN1_PRX" down
echo "- setting 0A06175100000112 screen..."
$MR_CLIENT "$SCREEN2_PRX" left
echo "- setting 0A06175100000113 screen..."
$MR_CLIENT "$SCREEN3_PRX" left
echo "- setting 0A06175100000114 screen..."
$MR_CLIENT "$SCREEN4_PRX" down

# Set speaker
echo "- setting 0A06175100000104 speaker..."
SPEAKER_PRX=$($PSGET "0A06175100000104|proxy" | tr -d '\"')
$SPK_CLIENT $CFG "$SPEAKER_PRX" "evacuation"