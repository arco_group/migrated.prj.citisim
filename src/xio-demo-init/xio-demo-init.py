#!/usr/bin/python3

import sys
import Ice
import logging
import time
from mijia import LumiGateway
from libcitisim import Broker


class DemoInitService(Ice.Application):
    def run(self, args):
        self._armed = False
        self._cur_color = (0, 0, 0, 0)

        broker = Broker(ic=self.communicator())
        sourceid = broker.props.getProperty("Service.SourceID")
        gw_key = broker.props.getProperty("Mijia.Gateway.Key")
        gw_sid = broker.props.getProperty("Mijia.Gateway.SID")
        self.alarm = broker.get_publisher(sourceid, "AlarmNotifier")

        self.gw = LumiGateway(passwd=(gw_key or None), sid=(gw_sid or None))
        self.gw.start()

        switch = list(self.gw.get_devices("switch").values())[0]
        cube = list(self.gw.get_devices("sensor_cube.aqgl01").values())[0]

        switch.on_click(self.on_switch_click)
        switch.on_double_click(self.on_switch_double_click)
        cube.on_shake_air(self.on_cube_shake_air)
        cube.on_flip90(self.on_cube_flip90)

        logging.info("Waiting events...")
        broker.wait_for_events()
        self.armed = False

    def send_earthquake_alarm(self):
        logging.info("- 'Earthquake' event published")
        self.alarm.publish("Earthquake")

    def send_gas_leak_alarm(self):
        logging.info("- 'GasLeak' event published")
        self.alarm.publish("GasLeak")

    @property
    def armed(self):
        return self._armed

    @armed.setter
    def armed(self, value):
        if value and not self._armed:
            self._cur_color = (1, 0, 0, 100)
            logging.info("- system now armed!")
        elif not value and self._armed:
            self._cur_color = (0, 0, 0, 0)
            logging.info("- system disarmed")

        self.gw.set_light_color(*self._cur_color)
        self._armed = value

    def on_switch_click(self, event, dev, *args):
        self.armed = True

    def on_switch_double_click(self, event, dev, *args):
        self.armed = False

    def on_cube_shake_air(self, event, dev, *args):
        if not self.armed:
            return
        self.send_earthquake_alarm()
        self.flick((0, 1, 0, 100))

    def on_cube_flip90(self, event, dev, *args):
        if not self.armed:
            return
        self.send_gas_leak_alarm()
        self.flick((0, 0, 1, 100))

    def flick(self, color, count=3, timeout=0.1):
        for _ in range(count):
            self.gw.set_light_color(0, 0, 0, 0)
            time.sleep(0.1)
            self.gw.set_light_color(*color)
            time.sleep(0.1)
        self.gw.set_light_color(*self._cur_color)


if __name__ == "__main__":
    DemoInitService(1).main(sys.argv)