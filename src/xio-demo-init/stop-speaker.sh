#!/bin/bash

echo "NOTE: Please, ensure you are connected to uclmiot wifi"

CFG="--Ice.Config=pike.config"
MR_CLIENT="../dummies/smart-screen/messagerender-client.py"
SPK_CLIENT="../dummies/speaker/messagerender-client.py"
PSGET="prop-tool -p PropertyServer $CFG get "

# Reset speaker
echo "- clearing 0A06175100000104 speaker..."
SPEAKER_PRX=$($PSGET "0A06175100000104|proxy" | tr -d '\"')
$SPK_CLIENT $CFG "$SPEAKER_PRX" ""
