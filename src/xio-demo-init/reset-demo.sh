#!/bin/bash

echo "NOTE: Please, ensure you are connected to uclmiot wifi"

CFG="--Ice.Config=pike.config"
MR_CLIENT="../dummies/smart-screen/messagerender-client.py"
SPK_CLIENT="../dummies/speaker/messagerender-client.py"
PSGET="prop-tool -p PropertyServer $CFG get "
ISPUB_EPS="-t:tcp -h pike.esi.uclm.es -p 9193"

# Get related proxies
echo "- retrieving proxies..."

# SCREEN1_PRX=$($PSGET "0A06175100000111|proxy" | tr -d '\"') # primera del pasillo
# SCREEN2_PRX=$($PSGET "0A06175100000112|proxy" | tr -d '\"') # laboratorio
# SCREEN3_PRX=$($PSGET "0A06175100000113|proxy" | tr -d '\"') # segunda del pasillo
# SCREEN4_PRX=$($PSGET "0A06175100000114|proxy" | tr -d '\"') # abajo
# SPEAKER_PRX=$($PSGET "0A06175100000104|proxy" | tr -d '\"')

SCREEN1_TPC=$($PSGET "0A06175100000111|private-channel" | tr -d '\"') # primera del pasillo
SCREEN2_TPC=$($PSGET "0A06175100000112|private-channel" | tr -d '\"') # laboratorio
SCREEN3_TPC=$($PSGET "0A06175100000113|private-channel" | tr -d '\"') # segunda del pasillo
SCREEN4_TPC=$($PSGET "0A06175100000114|private-channel" | tr -d '\"') # abajo
SPEAKER_TPC=$($PSGET "0A06175100000104|private-channel" | tr -d '\"')

SCREEN1_PRX="IceStorm/$SCREEN1_TPC.publish $ISPUB_EPS"
SCREEN2_PRX="IceStorm/$SCREEN2_TPC.publish $ISPUB_EPS"
SCREEN3_PRX="IceStorm/$SCREEN3_TPC.publish $ISPUB_EPS"
SCREEN4_PRX="IceStorm/$SCREEN4_TPC.publish $ISPUB_EPS"
SPEAKER_PRX="IceStorm/$SPEAKER_TPC.publish $ISPUB_EPS"

# Reset screens to Citsim logo
echo "- clearing 0A06175100000111 screen..."
echo "  using prx: '$SCREEN1_PRX'"
$MR_CLIENT "$SCREEN1_PRX" citi
echo "- clearing 0A06175100000112 screen..."
echo "  using prx: '$SCREEN2_PRX'"
$MR_CLIENT "$SCREEN2_PRX" citi
echo "- clearing 0A06175100000113 screen..."
echo "  using prx: '$SCREEN3_PRX'"
$MR_CLIENT "$SCREEN3_PRX" citi
echo "- clearing 0A06175100000114 screen..."
echo "  using prx: '$SCREEN4_PRX'"
$MR_CLIENT "$SCREEN4_PRX" citi

# Reset speaker
echo "- clearing 0A06175100000104 speaker..."
echo "  using prx: '$SPEAKER_PRX'"
$SPK_CLIENT $CFG "$SPEAKER_PRX" ""
