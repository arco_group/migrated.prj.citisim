#/bin/bash

if [ $# != 2 ]; then
    echo "Usage: $0 <lamp-id> <state>"
    exit 1
fi

LAMP=$1
STATE=$2
CFG="--Ice.Config=pike.config"
PSGET="prop-tool -p PropertyServer $CFG get "

PRX=$($PSGET "0A06175100000$LAMP|proxy" | tr -d '\"')
digitalsink-client "$PRX" $STATE
