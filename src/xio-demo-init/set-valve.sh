#/bin/bash

if [ $# != 1 ]; then
    echo "Usage: $0 <state>"
    exit 1
fi

STATE=$1
CFG="--Ice.Config=pike.config"
PSGET="prop-tool -p PropertyServer $CFG get "

PRX=$($PSGET "0A06175100000107|proxy" | tr -d '\"')
digitalsink-client "$PRX" $STATE
