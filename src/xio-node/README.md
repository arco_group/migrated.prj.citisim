
Overview
=========

Provision to install and configure a Citisim-XIO node on a Raspberry Pi.

**Note:** You must be able to access it from ssh using public key. The SSH host configuration name must be 'stanley'.


