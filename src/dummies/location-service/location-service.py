#!/usr/bin/python3

import os
import sys
import Ice
import json
import logging
from libcitisim import Broker, SmartServices

logging.getLogger().setLevel(logging.INFO)


class LocationServiceI(SmartServices.LocationService):
    def __init__(self, broker):
        self.broker = broker
        self.places = {
            "CELL_CORRIDOR.0": [
                "DD007357000011120001", "DD007357000011120002", "DD007357000011120003"
            ],
            "CELL_LABORATORY.14": [
                "DD007357000011120004",
            ],
            "ITSI": [
                "DD007357000011120005", "DD007357000011120006",
            ]
        }

    def getMeetingPointForPlace(self, placeid, current):
        return "CELL_8CCRX3VJ+X3P"

    def listTransducersByType(self, place, type, current):
        retval = []
        props = self.broker.get_property("")
        for oid, props in props.items():
            if props.get("transducer-type") == type:
                devices = self.places.get(place, [])
                if oid in devices:
                    retval.append(oid)
        return retval


class LocationService(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        broker = Broker(ic=ic)
        adapter = ic.createObjectAdapter("LocationService.Adapter")
        adapter.activate()

        servant = LocationServiceI(broker)

        prx = adapter.add(servant, Ice.stringToIdentity("LocationService"))
        logging.info(f"LocationService ready, prx: '{prx}'")
        try:
            ic.waitForShutdown()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    LocationService(1).main(sys.argv)
