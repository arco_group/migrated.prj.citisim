#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import SmartObject


class Client(Ice.Application):
    def run(self, args):
        if len(args) != 3:
            print("Usage: {} <proxy> <b1,b2,b3,b4...>".format(args[0]))
            return 1

        ic = self.communicator()
        proxy = ic.stringToProxy(args[1])
        proxy = SmartObject.DataSinkPrx.uncheckedCast(proxy)
        value = self.get_value(args)

        proxy.notify(value, "DummyClient", {})

    def get_value(self, args):
        return list(map(int, args[2].split(",")))


if __name__ == "__main__":
    exit(Client().main(sys.argv))
