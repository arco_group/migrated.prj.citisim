#!/usr/bin/python3

import os
import sys
import Ice
from libcitisim import SmartEnvironment
from argparse import ArgumentParser


class OccupancyMonitorClient(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return -1

        ic = self.communicator()

        prx = ic.stringToProxy(self.args.proxy)
        prx = SmartEnvironment.OccupancyMonitorPrx.uncheckedCast(prx)
        print(prx.getOccupiedPlaces())

    def parse_args(self, args):
        parser = ArgumentParser(args[0])
        parser.add_argument("-p", "--proxy", default="OccupancyMonitor",
            help="Proxy to a valid OccupancyMonitor server")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    OccupancyMonitorClient(1).main(sys.argv)

