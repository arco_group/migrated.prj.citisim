var _ = console.info.bind(console);

class SVGMap {
    constructor(target_id, name) {
        this.target_id = target_id;
        this.name = name;
        this.person_count = 0;
    }

    async init(url) {
        await this.load(url);
        this.connectEvents();
        this.updateName();
    }

    async load(url) {
        var elem = document.getElementById(this.target_id);
        let response = await fetch(url);
        let data = await response.text();

        const parser = new DOMParser();
        const parsed = parser.parseFromString(data, 'image/svg+xml');

        let svg = parsed.getElementsByTagName('svg');
        if (svg.length) {
            svg = svg[0];

            const attr = svg.attributes;
            const attrLen = attr.length;
            for (let i = 0; i < attrLen; i++) {
                if (attr[i].specified) {
                    if ('class' === attr[i].name) {
                        const classes = attr[i].value.replace(/\s+/g, ' ').trim().split(' ');
                        for (let j = 0; j < classes.length; ++j) {
                            elem.classList.add(classes[j]);
                        }
                    }
                    else {
                        elem.setAttribute(attr[i].name, attr[i].value);
                    }
                }
            }

            while (svg.childNodes.length) {
                elem.appendChild(svg.childNodes[0]);
            }
        }
        this.target = elem;
    }

    connectEvents() {
        var boxes = this.getLayer('boxes').find("rect, path");
        var self = this;
        boxes.each(function () {
            var elem = $(this);
            elem.fadeTo(0, 0);
            self.setAsDropTarget(elem, self.onDrop.bind(self));
        });
    }

    setAsDropTarget(elem, callback) {
        elem.on("dragenter", function (ev) {
            ev.preventDefault();
            elem.fadeTo(0, 1);
        });
        elem.on("dragleave", function (ev) {
            elem.fadeTo(0, 0);
        });
        elem.on("dragover", function (ev) {
            ev.preventDefault();
        });
        elem.on("drop", function (ev) {
            elem.fadeTo(0, 0);
            callback(ev, elem);
        });
    }

    onDrop(ev, elem) {
        var placeid = elem.attr("id")
        this.addToPlace(placeid);

        // change place name to match current format
        navigator.sendBeacon('/addPerson',
            JSON.stringify({ "placeid": toCellFormat(placeid)})
        );
    }

    addToPlace(placeid) {
        var l = this.getLayer("persons");
        if (l.find("#dot-" + placeid).length) {
            console.warn("Place " + placeid + " already added, ignoring!");
            return;
        }

        // draw dot inside the place
        var elem = this.getLayer("boxes").find("#" + placeid);
        var pos_el = elem;
        if (elem.prop("tagName") == "path")
            pos_el = $(this.target).find("#" + placeid + "-place");

        var x = parseFloat(pos_el.attr('x')) + parseFloat(pos_el.attr('width') | 0) / 2;
        var y = parseFloat(pos_el.attr('y')) + parseFloat(pos_el.attr('height') | 0) / 2;
        this.drawDot(l, x, y, 'red', "dot-" + placeid);

        this.person_count++;
        this.updateName();
    }

    drawDot(layer, x, y, color, id) {
        var svgns = "http://www.w3.org/2000/svg";
        var circle = document.createElementNS(svgns, 'circle');
        circle.setAttributeNS(null, 'id', id);
        circle.setAttributeNS(null, 'cx', x);
        circle.setAttributeNS(null, 'cy', y);
        circle.setAttributeNS(null, 'r', '5');
        circle.setAttributeNS(null, 'style', 'fill: ' + color);
        layer.append(circle);
    }

    // usually, you would use a query selector with: 'g[inkscape\\:label=boxes]'
    // ... but it does not work :/
    getLayer(name) {
        var found = null;
        $(this.target).find("g").each(function () {
            if ($(this).attr('inkscape:label') == name) {
                found = $(this);
                return false;
            }
        });
        return found;
    }

    updateName() {
        $("#" + this.target_id + "-tab").text(
            this.name + " (" + this.person_count + ")");
    }

    clear() {
        this.getLayer("persons").empty();
        this.person_count = 0;
        this.updateName();
    }
};

$(window).on("load", async function () {
    window.resizeTo(900, 500)

    base = new SVGMap("svg-basement", "Basement")
    await base.init("/static/images/basement.svg");

    floor1 = new SVGMap("svg-1st-floor", "1st Floor")
    await floor1.init("/static/images/1st-floor.svg");

    var changes = new EventSource("/changes");
    changes.onmessage = function (e) {
        var places = JSON.parse(e.data);
        base.clear();
        floor1.clear();
        for (var i in places) {
            var p = toLocalFormat(places[i]);
            var l = getMapByCellID(p);
            l.addToPlace(p);
        }
    };
});

function getMapByCellID(cellid) {
    if (base.getLayer("boxes").find("#" + cellid).length)
        return base;
    if (floor1.getLayer("boxes").find("#" + cellid).length)
        return floor1;
    return null;
};

function onCleanCount() {
    navigator.sendBeacon('/clear');
    base.clear();
    floor1.clear();
};

function toCellFormat(src) {
    return "CELL_" + src.toUpperCase().replace("-", ".");
};

function toLocalFormat(src) {
    return src.slice(5).toLowerCase().replace(".", "-");
};

