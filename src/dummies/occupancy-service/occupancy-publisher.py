#!/usr/bin/python3

import sys
import Ice
import logging
from time import time
from argparse import ArgumentParser
from libcitisim import Broker, SmartEnvironment

logging.basicConfig(level=logging.INFO)


class OccupancyPublisher(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return -1

        ic = self.communicator()
        self.broker = Broker(ic=ic)

        pub = self.broker.get_publisher(
            "DD00735700000002", "OccupancyReporter")
        pub.publish([
            SmartEnvironment.OccupancyChange(
                self.args.area,
                self.args.amount,
                str(int(time()))
            )
        ])
        logging.info("Event sent.")

    def parse_args(self, args):
        parser = ArgumentParser(args[0])
        parser.add_argument("area", type=str, help="Area of interest")
        parser.add_argument("amount", type=int, help="The amount of people there")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    OccupancyPublisher().main(sys.argv)