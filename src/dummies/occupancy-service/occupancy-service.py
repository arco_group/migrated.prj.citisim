#!/usr/bin/python3

import os
import sys
import json
import Ice
import logging
from threading import Event
from argparse import ArgumentParser
from flask import Flask, Response, render_template, send_from_directory, request
from libcitisim import SmartEnvironment

logging.getLogger().setLevel(logging.INFO)


class OccupancyMonitorI(SmartEnvironment.OccupancyMonitor):
    def __init__(self):
        self.events = Event()
        self.clear()

    def getOcupiedPlaces(self, current=None):
        return list(self.places)

    def addPerson(self, placeid):
        self.places.add(placeid)
        self.notify()

    def clear(self):
        self.places = set()
        self.notify()

    def notify(self):
        self.events.set()
        self.events.clear()


class FlaskApp(Flask):
    def __init__(self, servant):
        self.servant = servant
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_url_path=static)

        @self.route('/')
        def index():
            return render_template("occupancy-service.html")

        @self.route('/addPerson', methods=["POST"])
        def add_person():
            try:
                data = json.loads(request.get_data(as_text=True))
                self.servant.addPerson(data["placeid"])
            except Exception as e:
                logging.error("ERROR: invalid request, " + e)
                return ('', 500)
            return ('', 204)

        @self.route('/clear', methods=["POST"])
        def clear():
            self.servant.clear()
            return ('', 204)

        @self.route("/changes")
        def on_change():
            def event_stream():
                while True:
                    places = json.dumps(self.servant.getOcupiedPlaces())
                    yield "data: {}\n\n".format(places)
                    self.servant.events.wait()
            return Response(event_stream(), mimetype="text/event-stream")

        @self.route('/static/<path:path>')
        def get_static_file(path):
            dirname = os.path.join("static", os.path.dirname(path))
            filename = os.path.basename(path)
            return send_from_directory(dirname, filename)


class OccupancyService(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        if self.args.browser:
            self.open_browser()

        servant = OccupancyMonitorI()
        if not self.args.debug:
            self.ic = self.communicator()
            adapter = self.ic.createObjectAdapter("OccupancyService.Adapter")
            adapter.activate()

            prx = adapter.add(servant, Ice.stringToIdentity("OccupancyService"))
            logging.info(f"OccupancyService ready, prx: '{prx}'")

        app = FlaskApp(servant)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def open_browser(self):
        os.system(f"google-chrome --app=http://127.0.0.1:{self.args.port}")

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--browser",  action="store_true",
            help="open url in a browser window")
        parser.add_argument("--port", type=int, default=4993,
            help="port to bind web interface")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    OccupancyService(1).main(sys.argv)
