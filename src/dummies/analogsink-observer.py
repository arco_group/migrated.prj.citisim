#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import SmartObject


class AnalogSinkI(SmartObject.AnalogSink):
    def notify(self, value, source, meta, current):
        print("- notify: {} from '{}'".format(value, source))


class Observer(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("Adapter")
        adapter.activate()

        oid = "AnalogSinkDummy"
        oid = ic.getProperties().getPropertyWithDefault("Servant.OID", oid)
        oid = ic.stringToIdentity(oid)
        proxy = adapter.add(AnalogSinkI(), oid)
        print("Observer ready: '{}'".format(proxy))

        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    exit(Observer().main(sys.argv))
