#include "services.ice"

module SmartObject {
    interface SmartScreen
        extends SmartServices::MessageRender,
                SmartObject::Observable
        {};
};