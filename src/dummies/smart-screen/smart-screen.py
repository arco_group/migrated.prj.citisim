#!/usr/bin/python3

import os
import sys
import Ice
import logging
from threading import Event
from argparse import ArgumentParser
from flask import Flask, Response, render_template, send_from_directory
from libcitisim import Broker

Ice.loadSlice("smart-screen.ice -I/usr/share/slice/citisim --all")
import SmartObject

logging.getLogger().setLevel(logging.INFO)


class MessageRenderI(SmartObject.SmartScreen):
    def __init__(self):
        self.events = Event()
        self.current_msg = None

    def render(self, message_id, current):
        logging.info(f"- render call, message_id: '{message_id}'")
        self.current_msg = message_id
        self.notify()

    def setObserver(self, observer, current):
        pass

    def notify(self):
        self.events.set()
        self.events.clear()


class FlaskApp(Flask):
    def __init__(self, servant):
        self.servant = servant
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_url_path=static)

        @self.route('/')
        def index():
            return render_template("smart-screen.html")

        @self.route("/msg")
        def messages():
            def event_stream():
                while True:
                    self.servant.events.wait()
                    yield "data: {}\n\n".format(self.servant.current_msg)
            return Response(event_stream(), mimetype="text/event-stream")

        @self.route('/static/<path:path>')
        def get_static_file(path):
            dirname = os.path.join("static", os.path.dirname(path))
            filename = os.path.basename(path)
            return send_from_directory(dirname, filename)


class SmartScreen(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        if self.args.browser:
            self.open_browser()

        servant = MessageRenderI()
        if not self.args.debug:
            ic = self.communicator()
            broker = Broker(ic=ic)
            adapter = ic.createObjectAdapter("SmartScreen.Adapter")
            adapter.activate()

            oid = ic.getProperties().getPropertyWithDefault(
                "SmartScreen.ID", "SmartScreen")
            prx = adapter.add(servant, Ice.stringToIdentity(oid))
            broker.hello(str(prx), "SmartScreen")
            logging.info(f"SmartScreen ready, prx: '{prx}'")

        app = FlaskApp(servant)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def open_browser(self):
        os.system(f"google-chrome --app=http://127.0.0.1:{self.args.port}")

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--browser",  action="store_true",
            help="open url in a browser window")
        parser.add_argument("--port", type=int, default=5000,
            help="port to bind web interface")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    SmartScreen(1).main(sys.argv)
