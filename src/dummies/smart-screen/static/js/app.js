_ = console.log.bind(console);
window.resizeTo(800, 500);

var image = document.getElementById("sign");
var led = document.getElementById("led");
var events = new EventSource("/msg");
events.onmessage = function(e) {
    var filename = e.data;
    if (filename.split(".").length == 1)
        filename += ".png";
    image.src = "/static/images/" + filename;
};
events.onopen = function(e) {
    image.src = "/static/images/empty.png";
    led.classList.remove('red');
    led.classList.add('green');
};
events.onerror = function(e) {
    image.src = "/static/images/white-noise.png";
    led.classList.remove('green');
    led.classList.add('red');
};
