#!/usr/bin/python3

import os
import sys
import Ice
from libcitisim import SmartServices


class MessageRenderClient(Ice.Application):
    def run(self, args):
        if len(args) < 2:
            print(f"Usage: {args[0]} <proxy> [message]")
            return 1

        ic = self.communicator()

        prx = ic.stringToProxy(args[1])
        prx = SmartServices.MessageRenderPrx.uncheckedCast(prx)
        if len(args) > 2:
            prx.render(args[2])
        else:
            prx.clear()


if __name__ == "__main__":
    MessageRenderClient(1).main(sys.argv)
