#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import logging
import random
from time import sleep
from libcitisim import Broker, normalize_addr
from argparse import ArgumentParser

logging.basicConfig(level=logging.INFO)


class Publisher(Ice.Application):
    def run(self, args):
        try:
            self.args = self.parse_args(args)
        except SystemExit:
            return -1

        self.broker = Broker("", ic=self.communicator())
        publisher = self.broker.get_publisher(
            source = normalize_addr(self.args.source),
            # topic_name = self.args.topic)
            transducer_type="RelayActuator")

        print("publisher ready")
        count = 0
        try:
            while True and not self.interrupted():
                value = self.get_value()
                publisher.publish(value)
                print("- event sent, value: {0}".format(value))

                if self.args.limit != 0:
                    count += 1
                    if count >= self.args.limit:
                        break

                self.delay()
            self.keep_open()
        except KeyboardInterrupt:
            pass

    def get_value(self):
        if self.args.random_value:
            return random.choice((True, False))
        return self.args.value

    def delay(self):
        secs = self.args.delay
        if self.args.random_delay:
            secs = random.randrange(1, 30)
        print("- delay {} seconds".format(secs))

        while secs > 0 and not self.interrupted():
            sleep(0.1)
            secs -= 0.1

    def keep_open(self):
        if self.args.keep_open and not self.interrupted():
            print('- keep open (but nothing more to do)')
            self.broker.wait_for_events()

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument(
            "-t", "--topic", default="Twilight",
            help="topic name where it will publish")
        parser.add_argument(
            "-s", "--source", default="FFFF:0000:0000:0001",
            help="source ID of this publisher")
        parser.add_argument(
            "-d", "--delay", default=10, type=float,
            help="delay between events")
        parser.add_argument(
            "-v", "--value", default=True, type=bool,
            help="fixed value to send")
        parser.add_argument(
            "-r", "--random-value", default=False, action="store_true",
            help="generate random values")
        parser.add_argument(
            "--random-delay", default=False, action="store_true",
            help="generate random delays")
        parser.add_argument(
            "-l", "--limit", default=0, type=int,
            help="number of events to send (0 = no limit)")
        parser.add_argument(
            "-k", "--keep-open", default=False, action="store_true",
            help="do not exit publisher when finished")
        return parser.parse_args(args[1:])


if __name__ == "__main__":
    exit(Publisher().main(sys.argv))
