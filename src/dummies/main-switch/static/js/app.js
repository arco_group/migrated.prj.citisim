_ = console.log.bind(console);
window.resizeTo(270, 241);

$(function() {
    var events = new EventSource("/msg");
    var main_switch = $("#switch")

    events.onmessage = function(e) {
        if (e.data == "True") {
            main_switch.attr({"src": "/static/images/main-switch-on.png"})
        }
        else {
            main_switch.attr({"src": "/static/images/main-switch-off.png"})
        }
    };
});