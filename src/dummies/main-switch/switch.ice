#include "services.ice"

module SmartObject {
    interface Switch
        extends SmartObject::DigitalSink,
                SmartObject::Observable
        {};
};
