#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
from libcitisim import Broker


class Subscriber:
    def run(self, args):
        config = 'subscriber.config'
        if len(args) > 1:
            config = args[1]
        topic = "Temperature"

        print("Subscribing to '{}'".format(topic))
        broker = Broker(config)
        broker.subscribe(topic, self.on_event)

        print("Waiting events...")
        broker.wait_for_events()

    def on_event(self, value, source, meta):
        print("Event arrived from '{}': {}".format(source, value))


if __name__ == "__main__":
    exit(Subscriber().run(sys.argv))
