#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import Broker, SmartObject
from argparse import ArgumentParser


class DigitalSinkI(SmartObject.DigitalSink):
    def notify(self, value, source, meta, current=None):
        print("- notify: {}, source: '{}', meta: '{}'".format(
            value, source, meta))


class Observer(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return 1

        ic = self.communicator()
        broker = Broker(ic=ic)
        servant = DigitalSinkI()

        props = ic.getProperties()
        oid = "DigitalSinkDummy"
        oid = props.getPropertyWithDefault("Servant.OID", oid)
        oid = ic.stringToIdentity(oid)

        if self.args.to_publisher:
            broker.subscribe_to_publisher(
                self.args.to_publisher, servant.notify)
            print("Observer subscribed to {}".format(self.args.to_publisher))

        else:
            adapter = ic.createObjectAdapter("Adapter")
            adapter.activate()
            proxy = adapter.add(servant, oid)
            print("Observer ready: '{}'".format(proxy))

        self.shutdownOnInterrupt()
        ic.waitForShutdown()

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--to-publisher",
            help="publisher's ID to whom subscribe")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    exit(Observer().main(sys.argv))
