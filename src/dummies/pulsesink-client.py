#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import SmartObject


class Client(Ice.Application):
    def run(self, args):
        if len(args) != 2:
            print("Usage: {} <proxy>".format(args[0]))
            return 1

        ic = self.communicator()
        proxy = ic.stringToProxy(args[1]).ice_encodingVersion(Ice.Encoding_1_0)
        proxy = SmartObject.PulseSinkPrx.uncheckedCast(proxy)

        print(f"Using proxy: '{proxy}'")
        proxy.notify("DummyClient", {})


if __name__ == "__main__":
    exit(Client().main(sys.argv))
