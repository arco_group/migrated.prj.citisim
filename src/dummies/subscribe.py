#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceStorm
from libcitisim import Broker, SmartObject
from argparse import ArgumentParser


class ISSubscriber(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return 1

        ic = self.communicator()
        broker = Broker(ic=ic)

        # get private channel of src
        topic = broker.get_property(self.args.src + " | private-channel")
        topic = broker.topic_manager.retrieve(topic)
        print("Topic: " + str(topic))

        # get proxy of dst
        proxy = broker.get_property(self.args.dst + " | proxy")
        proxy = ic.stringToProxy(proxy)
        print("Observer: " + str(proxy))

        # subscribe them
        topic.unsubscribe(proxy)
        topic.subscribeAndGetPublisher({}, proxy)
        print("Done!")

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("src", help="ID of events source")
        parser.add_argument("dst", help="ID of listener")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    exit(ISSubscriber().main(sys.argv))
