#!/usr/bin/python3 -u

import sys
import Ice
import requests
import json
from libcitisim import Broker, SmartObject
from argparse import ArgumentParser


class Observer(Ice.Application):
    event_url = "http://citisim.prodevelop.es/prodeglobe_web/scene/fakeMsj"

    def run(self, args):
        if not self.parse_args(args):
            return 1

        ic = self.communicator()
        broker = Broker(ic=ic)

        broker.subscribe_to_publisher(
            self.args.source, self.notify)
        broker.wait_for_events()

    def notify(self, value, source, meta):
        print(f"- notify: {value}, source: '{source}', meta: '{meta}'")

        msg = {
            "msj": {
                "value": value,
                "source": source,
                "data": {
                    "Place": "ITSI ROOFTOP"
                }
            },
            "type": source,
            "scene": "0",
            "timestamp": "1624271883179"
        }
        requests.post(
            self.event_url,
            headers={"Content-Type": "application/json"},
            data=json.dumps(msg))

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("-s", "--source",
            help="publisher source to subscribe to")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    exit(Observer().main(sys.argv))
