#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import SmartObject


class Client(Ice.Application):
    def run(self, args):
        if len(args) != 3:
            print("Usage: {} <proxy> <value>".format(args[0]))
            return 1

        ic = self.communicator()
        proxy = ic.stringToProxy(args[1])
        proxy = SmartObject.AnalogSinkPrx.uncheckedCast(proxy)

        proxy.notify(float(args[2]), "DummyClient", {})


if __name__ == "__main__":
    exit(Client().main(sys.argv))
