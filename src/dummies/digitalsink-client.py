#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from libcitisim import SmartObject


class Client(Ice.Application):
    def run(self, args):
        if len(args) != 3:
            print("Usage: {} <proxy> <1|0>".format(args[0]))
            return 1

        ic = self.communicator()
        proxy = ic.stringToProxy(args[1])
        proxy = SmartObject.DigitalSinkPrx.uncheckedCast(proxy)
        state = args[2] != "0"

        proxy.notify(state, "DummyClient", {})


if __name__ == "__main__":
    exit(Client().main(sys.argv))
