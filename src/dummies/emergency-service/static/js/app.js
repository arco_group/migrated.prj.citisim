var _ = console.log.bind(console);

function get_unique_id() {
    return Math.random().toString(36).substring(2) +
        (new Date()).getTime().toString(36);
}

class Table {
    constructor(parent) {
        this.dst = $(parent);
        this.uid = get_unique_id();
        this.count = 0;

        var thead = (
            '<th style="width: 35px">#</th>' +
            '<th class="w-min">Time</th>' +
            '<th class="w-auto">Message</th>'
        );

        this.dst.append(
            '<table class="table table-sm">' +
            ' <thead><tr>' + thead + '</tr>' +
            ' <tbody id="' + this.uid + '"></tbody>' +
            '</table>'
        );
        this.tbody = $("#" + this.uid);
    }

    append(msg, cls) {
        this.tbody.append(
            '<tr class="' + cls + '">' +
            ' <td>' + ++this.count + '</td>' +
            ' <td>' + new Date().toISOString().slice(11) + '</td>' +
            ' <td>' + msg + '</td></tr>'
        );
    }
}

class Board {
    constructor() {
        var table = new Table("#board");
        var connected = null;

        var messages = new EventSource("/msg");
        messages.onmessage = function (e) {
            var msg = JSON.parse(e.data);
            table.append(msg.text, msg.level);
            document.getElementById('board').scrollIntoView(
                {behavior: 'smooth', block: 'end'}
            );
        };
        messages.onopen = function (e) {
            if (connected === false)
                table.append("Connection restablished.", 'info');
            connected = true;
        };
        messages.onerror = function (e) {
            if (connected) {
                table.append("Connection to server lost!", 'warning');
                connected = false;
            }
        };
    }
};

$().ready(function() {
    window.resizeTo(600, 800);
    board = new Board();
});