#!/usr/bin/python3

import os
import sys
import json
import Ice
import IceStorm
import logging
import traceback
from argparse import ArgumentParser
from threading import Thread
from queue import Queue
from flask import Flask, Response, render_template, send_from_directory
from libcitisim import Broker, SmartServices, SmartObject, SmartEnvironment

logging.getLogger().setLevel(logging.INFO)


class WebLogger:
    def __init__(self):
        self.clients = []

    def get_listener_queue(self):
        queue = Queue()
        self.clients.append(queue)
        for c in self.clients[:]:
            if sys.getrefcount(c) < 4:
                self.clients.remove(c)
        return queue

    def debug(self, msg):
        self.append(msg, 'debug')

    def info(self, msg):
        self.append(msg, 'info')

    def warning(self, msg):
        self.append(msg, 'warning')

    def critical(self, msg):
        self.append(msg, 'critical')

    def error(self, msg):
        self.append(msg, 'error')

    def append(self, msg, level):
        msg = json.dumps(dict(
           level = level,
           text = msg
        ))
        for q in self.clients:
            q.put(msg)


class EventSinkI(SmartObject.EventSink):
    def __init__(self):
        self.events = Queue()

    def notify(self, eventName, source, meta, current):
        logging.info(f"- event received: '{eventName}'")
        self.events.put({'name': eventName, 'source': source})


class Planner(Thread):
    def __init__(self, servant, broker, web_logger, services):
        super().__init__()
        self.daemon = True
        self.servant = servant
        self.broker = broker
        self.web_logger = web_logger
        self.services = services
        self.start()

    def run(self):
        self.web_logger.debug("Planner started")
        while True:
            try:
                ev = self.servant.events.get()
                ev_name = ev['name']
                self.web_logger.critical(f"ALARM: '{ev_name}' detected!!")
                self.handle_event(ev_name)
            except Exception as e:
                self.web_logger.error(f"Unexpected error: {e}")
                traceback.print_exc()

    def handle_event(self, ev_name):
        if ev_name.lower() == "earthquake":
            self.handle_earthquake()
        elif ev_name.lower() == "gas leak":
            self.handle_gas_leak()
        else:
            self.web_logger.warning(f"Unknown event ({ev_name}), nothing done!")

    def handle_earthquake(self):
        self.web_logger.info("Retrieving occupied areas...")
        places = self.services["occupancy"].getOcupiedPlaces()
        logging.info(places)
        if len(places) == 0:
            self.web_logger.info("- All monitored areas are empty, nothing to do")
            return

        location = self.services["location"]
        routing = self.services["routing"]
        route_pub = self.services["route_publisher"]

        for p in places:
            self.web_logger.info(f"Occuppied place: {p}, getting evacuation point...")
            ev_point = location.getMeetingPointForPlace(p)
            self.web_logger.info(f"- EP for {p} is {ev_point}, getting evacuation route...")

            route = routing.getPath(p, ev_point)
            if not route:
                self.web_logger.warning(f"- Can not obtain EV route!")
                continue

            self.web_logger.info(f"- EV route obtained from {p} to {ev_point}")
            self.web_logger.info(f"- Publishing EV route...")
            route_pub.publish(route)

        logging.info(self.services)

    def handle_gas_leak(self):
        location = self.services["location"]
        self.web_logger.info(f"Getting affected actuators...")
        gas_valves = location.listTransducersByType("ITSI", "ValveActuator")
        electrical_switches = location.listTransducersByType("ITSI", "SwitchActuator")

        if gas_valves:
            self.web_logger.info(f"- Gas valve available, closing it")
            for devid in gas_valves:
                device = self.get_device(devid)
                device.notify(False, "EmergencyService", {})

        if electrical_switches:
            self.web_logger.info(f"- Electrical switch available, closing it")
            for devid in electrical_switches:
                device = self.get_device(devid)
                device.notify(False, "EmergencyService", {})

    def get_device(self, devid):
        prx = self.broker.get_property(f"{devid} | proxy")
        prx = self.broker._ic.stringToProxy(prx)
        return SmartObject.DigitalSinkPrx.uncheckedCast(prx)


class FlaskApp(Flask):
    def __init__(self, web_logger):
        self.web_logger = web_logger
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_url_path=static)

        @self.route('/')
        def index():
            return render_template("emergency-service.html")

        @self.route('/static/<path:path>')
        def get_static_file(path):
            dirname = os.path.join("static", os.path.dirname(path))
            filename = os.path.basename(path)
            return send_from_directory(dirname, filename)

        @self.route("/msg")
        def messages():
            def event_stream():
                messages = self.web_logger.get_listener_queue()
                while True:
                    yield "data: {}\n\n".format(messages.get())
            return Response(event_stream(), mimetype="text/event-stream")


class EmergencyService(Ice.Application):
    def run(self, args):
        self.services = {}

        if not self.parse_args(args[1:]):
            return 1

        if self.args.browser:
            self.open_browser()

        servant = EventSinkI()
        web_logger = WebLogger()
        self.broker = None

        if not self.args.debug:
            self.ic = self.communicator()
            adapter = self.ic.createObjectAdapter("EmergencyService.Adapter")
            adapter.activate()
            self.broker = Broker(ic=self.ic)

            prx = adapter.add(servant, Ice.stringToIdentity("EmergencyService"))
            logging.info(f"EmergencyService ready, prx: '{prx}'")
            self.subscribe_proxy(prx, self.args.topic)
            self.get_service_proxies()

        Planner(servant, self.broker, web_logger, self.services)

        app = FlaskApp(web_logger)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def subscribe_proxy(self, proxy, topic_name):
        topic = self.get_topic(topic_name)
        try:
            topic.subscribeAndGetPublisher({}, proxy)
        except IceStorm.AlreadySubscribed:
            topic.unsubscribe(proxy)
            topic.subscribeAndGetPublisher({}, proxy)

    def get_topic(self, name):
        mgr = self.ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.uncheckedCast(mgr)
        try:
            return mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            return mgr.create(name)

    def get_service_proxies(self):
        prx = self.ic.propertyToProxy("OccupancyService.Proxy")
        self.services["occupancy"] = \
            SmartEnvironment.OccupancyMonitorPrx.uncheckedCast(prx)
        prx = self.ic.propertyToProxy("LocationService.Proxy")
        self.services["location"] = \
            SmartServices.LocationServicePrx.uncheckedCast(prx)
        prx = self.ic.propertyToProxy("RoutingService.Proxy")
        self.services["routing"] = \
            SmartServices.RoutingServicePrx.uncheckedCast(prx)

        prop_key = "EmergencyService.SourceID"
        sourceid = self.ic.getProperties().getProperty(prop_key)
        if not sourceid:
            logging.warning(f" Invalid sourceID, please provide the {prop_key}!")

        self.services["route_publisher"] = self.broker.get_publisher(
            sourceid, "RouteService"
        )

    def open_browser(self):
        os.system(f"google-chrome --app=http://127.0.0.1:{self.args.port}")

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--browser",  action="store_true",
            help="open url in a browser window")
        parser.add_argument("--port", type=int, default=4991,
            help="port to bind web interface")
        parser.add_argument('--topic', default="Alarm",
            help="topic name where listen for system events")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    EmergencyService(1).main(sys.argv)
