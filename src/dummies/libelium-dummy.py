#!/usr/bin/python3 -u

import sys
import time
import random
import json
import logging
from threading import Thread
from argparse import ArgumentParser

import Ice

from libcitisim import Broker

logging.basicConfig(level=logging.INFO)


class Publisher(Thread):
    def __init__(self, broker, source_id, spec, meta):
        super().__init__()

        self.source_id = source_id
        self.spec = spec
        self.meta = meta
        self.daemon = True

        self.delay = int(spec["delay"])
        self.pool = spec["value_pool"]
        self.counter = 0

        self.publisher = broker.get_publisher(
            source=source_id, transducer_type=spec["transducer-type"], meta=meta)

    def run(self):
        logging.info(f" Publisher for {self.source_id} started")
        while True:
            self.publish()
            time.sleep(self.delay)

    def publish(self):
        value = self.get_next_value()
        self.publisher.publish(value)

    def get_next_value(self):
        if self.counter >= len(self.pool):
            self.counter = 0

        val = self.pool[self.counter]
        self.counter += 1

        if random.random() <= self.spec["th_prob"]:
            prec = 100
            a, b = [x * prec for x in self.spec["th_range"]]
            val += random.randint(a, b) / prec

        return val


class LibeliumDummy(Ice.Application):
    def run(self, args):
        try:
            self.args = self.parse_args(args)
        except SystemExit:
            return -1

        ic = self.communicator()
        self.broker = Broker("", ic=ic)
        self.specs = json.load(open(self.args.spec))

        pubs = self.create_publishers()
        for p in pubs:
            p.start()

        self.shutdownOnInterrupt()
        ic.waitForShutdown()

    def create_publishers(self):
        pubs = []
        for source_id, spec in self.specs["sensors"].items():
            publisher = Publisher(self.broker, source_id, spec, self.specs["metadata"])
            pubs.append(publisher)
        return pubs

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument(
            "-s", "--spec", default="libelium-spec.json",
            help="specification file with node dummies")

        return parser.parse_args(args[1:])


if __name__ == "__main__":
    sys.exit(LibeliumDummy().main(sys.argv))
