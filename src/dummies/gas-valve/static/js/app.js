_ = console.log.bind(console);
window.resizeTo(270, 241);

$(function() {
    var events = new EventSource("/msg");
    var valve = $("#valve")

    events.onmessage = function(e) {
        if (e.data == "True") {
            valve.attr({"src": "/static/images/gas-valve-open.png"})
        }
        else {
            valve.attr({"src": "/static/images/gas-valve-close.png"})
        }
    };
});