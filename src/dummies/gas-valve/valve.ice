#include "services.ice"

module SmartObject {
    interface Valve
        extends SmartObject::DigitalSink,
                SmartObject::Observable
        {};
};
