#!/usr/bin/python3

import sys
import Ice
import IceStorm
from argparse import ArgumentParser
from libcitisim import SmartObject


class EventSinkI(SmartObject.EventSink):
    def notify(self, eventName, source, meta, current):
        print(f"- event received: '{eventName}'")


class EventSinkSubscriber(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        self.ic = self.communicator()
        adapter = self.ic.createObjectAdapter("EventSink.Adapter")
        adapter.activate()

        prx = adapter.addWithUUID(EventSinkI())
        print(f"Servant proxy: '{prx}'")
        topic = self.get_topic(self.args.topic)
        topic.subscribeAndGetPublisher({}, prx)

        # self.shutdownOnInterrupt()
        print("Ready, waiting events...")
        self.ic.waitForShutdown()

    def get_topic(self, name):
        mgr = self.ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.uncheckedCast(mgr)
        try:
            return mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            return mgr.create(name)

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument('--topic', required=True,
            help="topic name where publish event")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    EventSinkSubscriber(1).main(sys.argv)
