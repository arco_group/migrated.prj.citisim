#!/usr/bin/python3

import os
import sys
import json
import Ice
import IceStorm
from argparse import ArgumentParser
from flask import Flask, Response, request, render_template, send_from_directory
from libcitisim import SmartObject


class FlaskApp(Flask):
    def __init__(self, sink):
        self.sink = sink
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_url_path=static)

        @self.route('/')
        def index():
            return render_template("eventsink-client.html")

        @self.route('/handler', methods=["POST"])
        def on_click():
            try:
                data = json.loads(request.get_data(as_text=True))
                self.send_event(data["type"])
            except Exception as e:
                print("ERROR: invalid request,", e)
                return ('', 500)
            return ('', 204)

        @self.route('/static/<path:path>')
        def get_static_file(path):
            dirname = os.path.join("static", os.path.dirname(path))
            filename = os.path.basename(path)
            return send_from_directory(dirname, filename)

    def send_event(self, name):
        print(f"- send event: '{name}'")
        self.sink.notify(name, "dummy-eventsink-client", {})


class EventSinkClient(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        if self.args.browser:
            self.open_browser()

        self.ic = self.communicator()
        sink = None if self.args.debug else self.get_proxy()

        app = FlaskApp(sink)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def open_browser(self):
        os.system(f"google-chrome --app=http://127.0.0.1:{self.args.port}")

    def get_proxy(self):
        if self.args.proxy:
            prx = self.ic.stringToProxy(self.args.proxy)
        else:
            prx = self.get_topic_publisher(self.args.topic)
        return SmartObject.EventSinkPrx.uncheckedCast(prx)

    def get_topic_publisher(self, name):
        mgr = self.ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        assert mgr is not None, "Missing configuration for IceStorm/TopicManager"

        mgr = IceStorm.TopicManagerPrx.uncheckedCast(mgr)
        try:
            topic = mgr.retrieve(name)
        except IceStorm.NoSuchTopic:
            topic = mgr.create(name)
        return topic.getPublisher()

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--browser",  action="store_true",
            help="open url in a browser window")
        parser.add_argument("--port", type=int, default=4990,
            help="port to bind web interface")
        prx_group = parser.add_mutually_exclusive_group(required=True)
        prx_group.add_argument('--topic',
            help="topic name where publish event")
        prx_group.add_argument('--proxy',
            help="proxy of sink to invoke")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    EventSinkClient(1).main(sys.argv)