#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

from libcitisim import SmartObject


class SetObserver(Ice.Application):
    def run(self, args):
        if len(args) < 3:
            print("Usage: {} <subject_proxy> <observer>".format(args[0]))
            return 1

        subject = self.get_subject_proxy(args[1])
        subject.setObserver(args[2])
        print("+'{}'\n+------> '{}'".format(subject, args[2]))

    def get_subject_proxy(self, strprx):
        ic = self.communicator()
        proxy = ic.stringToProxy(strprx)
        return SmartObject.ObservablePrx.uncheckedCast(proxy)


if __name__ == "__main__":
    SetObserver().main(sys.argv)
