#include "services.ice"

module SmartObject {
    interface Speaker
        extends SmartServices::MessageRender,
                SmartObject::Observable
        {};
};