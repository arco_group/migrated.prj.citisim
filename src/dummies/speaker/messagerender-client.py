#!/usr/bin/python3

import os
import sys
import Ice
from libcitisim import SmartServices


class MessageRenderClient(Ice.Application):
    def run(self, args):
        if len(args) != 3:
            print(f"Usage: {args[0]} <proxy> <message-id>")
            return -1

        ic = self.communicator()
        msg = args[2]

        prx = ic.stringToProxy(args[1])
        prx = SmartServices.MessageRenderPrx.uncheckedCast(prx)
        if not msg:
            prx.clear()
        else:
            prx.render(args[2])


if __name__ == "__main__":
    MessageRenderClient(1).main(sys.argv)
