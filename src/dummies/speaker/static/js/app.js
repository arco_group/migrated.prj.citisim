_ = console.log.bind(console);
window.resizeTo(330, 164);

$(function() {

    var silence_img = "/static/images/muted.png";
    var volume_img = "/static/images/un-muted.png";
    var idle = true;

    var events = new EventSource("/msg");
    audio = new Audio();
    audio.loop = true;
    audio.muted = true;

    var btn = $("#listen-btn img")
    btn.click(function() {
        audio.play();  // play allways, avoid 'user not interactuated...'
        if (audio.muted) {
            audio.muted = false;
            btn.attr("src", volume_img);
        }
        else {
            audio.muted = true;
            btn.attr("src", silence_img);
        }
    });

    events.onmessage = function(e) {
        var filename = e.data;
        if (filename.split(".").length == 1)
            filename += ".mp3";

        var msg = $("#message-id");
        if (idle) {
            btn.attr("src", silence_img);
            $(".separator").removeClass("idle").addClass("playing");
            msg.removeClass("dimm");
        }

        msg.text(e.data);
        audio.src = "/static/audio/" + filename;
        audio.muted = true;
        audio.play();
    };
});