#!/usr/bin/python3

import os
import sys
import Ice
import logging
from libcitisim import Broker, SmartServices

logging.getLogger().setLevel(logging.INFO)


class VisualSignaling:
    def __init__(self, broker, location_srv):
        self.broker = broker
        self.location_srv = location_srv

    def configureRoute(self, route):
        logging.info("Evacuation route received, setting visual env...")

        devices = set()
        for place in route:
            trans = self.location_srv.listTransducersByType(place, "SmartScreen")
            devices.update(trans)
        logging.info("- smart-screen devices: " + str(devices))

        # NOTE: the service should retrieve this information about what to put where
        # in a more intelligent and dynamic way...
        configuration = {
            "DD007357000011120001": "alarm-exit-front.gif",
            "DD007357000011120002": "alarm-exit-left.gif"
        }
        self.deploy_signals(configuration)

    def deploy_signals(self, config):
        logging.info("- settings: " + str(config))

        for devid, msg in config.items():
            device = self.get_device(devid)
            if device is None:
                logging.error(f"Proxy property of {devid} not defined, can not connect!")
            device.render(msg)

    def get_device(self, devid):
        prx = self.broker.get_property(f"{devid} | proxy")
        prx = self.broker._ic.stringToProxy(prx)
        return SmartServices.MessageRenderPrx.uncheckedCast(prx)


class AudioSignaling:
    def __init__(self, broker, location_srv):
        self.broker = broker
        self.location_srv = location_srv

    def configureRoute(self, route):
        logging.info("Evacuation route received, setting audible env...")

        devices = set()
        for place in route:
            trans = self.location_srv.listTransducersByType(place, "Speaker")
            devices.update(trans)
        logging.info("- speaker devices: " + str(devices))

        self.deploy_signals(devices, "evacuate-message")

    def deploy_signals(self, devices, message):
        logging.info(f"- sending message '{message}' to devices...")

        for devid in devices:
            device = self.get_device(devid)
            if device is None:
                logging.error(f"Proxy property of {devid} not defined, can not connect!")
            device.render(message)

    def get_device(self, devid):
        prx = self.broker.get_property(f"{devid} | proxy")
        prx = self.broker._ic.stringToProxy(prx)
        return SmartServices.MessageRenderPrx.uncheckedCast(prx)



class SignalingService(Ice.Application):
    def run(self, args):

        self.ic = self.communicator()
        broker = Broker(ic=self.ic)
        location_srv = self.get_service_proxy("LocationService")
        self.signals = VisualSignaling(broker, location_srv)
        self.speakers = AudioSignaling(broker, location_srv)

        route_src = broker.props.getProperty("RoutePublisher.SourceID")
        broker.subscribe_to_publisher(route_src, self.on_evacuation_route)
        logging.info(f"SignalingService ready, waiting events...")
        try:
            broker.wait_for_events()
        except KeyboardInterrupt:
            pass

    def get_service_proxy(self, name):
        key = name + ".Proxy"
        prx = self.ic.propertyToProxy(key)
        if prx is None:
            logging.error(f" missing property: {key}")
            return

        if name == "LocationService":
            return SmartServices.LocationServicePrx.checkedCast(prx)
        return prx

    def on_evacuation_route(self, route):
        self.signals.configureRoute(route)
        self.speakers.configureRoute(route)


if __name__ == "__main__":
    SignalingService(1).main(sys.argv)
