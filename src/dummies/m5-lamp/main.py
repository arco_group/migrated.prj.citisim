from machine import Pin
from neopixel import NeoPixel
from sys import stdin


class Lamp:
    def __init__(self):
        self.np = NeoPixel(Pin(26), 1)
        self.off()

    def off(self):
        self.np.fill((0, 0, 0))
        self.np.write()

    def on(self):
        self.np.fill((250, 250, 250))
        self.np.write()


l = Lamp()
while True:
    try:
        d = stdin.read(1)
        if d == "1":
            l.on()
        elif d == "0":
            l.off()
    except KeyboardInterrupt:
        break

