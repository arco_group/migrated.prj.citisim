#include "iot.ice"

module SmartObject {
    interface Lamp
        extends SmartObject::DigitalSink,
                SmartObject::Observable
        {};
};
