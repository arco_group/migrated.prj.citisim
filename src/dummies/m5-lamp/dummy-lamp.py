#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import logging
from serial import Serial
import Ice
from libcitisim import Broker

Ice.loadSlice("lamp.ice -I/usr/share/slice/citisim --all")
import SmartObject

logging.basicConfig(level=logging.INFO)


class LampI(SmartObject.Lamp):
    def __init__(self):
        self.dev = Serial("/dev/ttyUSB0", 115200)

    def notify(self, value, source, meta, current=None):
        logging.info(f"- notify call, value: '{value}'")
        self.dev.write(b'1' if value else b'0')


class DummyLamp(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        broker = Broker(ic=ic)
        source_id = broker.props.getProperty("Lamp.SourceID")
        servant = LampI()

        broker.subscribe_to_publisher(
            source_id, servant.notify
        )
        pub = f"IceStorm/{source_id}.private.publish -t:tcp -h pike.esi.uclm.es -p 9193"
        # broker.set_property(f"{source_id}|proxy", str(pub))

        logging.info("Ready, waiting events...")
        broker.wait_for_events()


if __name__ == "__main__":
    exit(DummyLamp().main(sys.argv))
