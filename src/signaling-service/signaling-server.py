#!/usr/bin/python3

import sys
import Ice
import logging
from time import time
from libcitisim import Broker, SmartServices, PropertyService

logging.basicConfig(level=logging.INFO)


class Signaling:
    def __init__(self, broker, location, settings):
        self.broker = broker
        self.location = location
        self.settings = settings

    def deploy_signal(self, devid, use_topic=False):
        signalid = self.settings.get(devid)
        if signalid is None:
            logging.warning("  device {} has not settings, ignored".format(devid))
            return

        proxy = self.get_device(devid, use_topic)
        if proxy is None:
            logging.warning("  could not get proxy for {}, ignored".format(devid))
            return

        try:
            logging.info("  sending message '{}' to {}...".format(signalid, devid))
            proxy.render(signalid)
        except Exception as e:
            logging.warning("  there were an error with device proxy:")
            logging.warning("  " + str(e))

    def get_device(self, devid, use_topic=False):
        prx = None
        if use_topic:
            try:
                topic = self.broker.get_property(devid + " | private-channel")
                topic = self.broker.get_topic(topic)
                prx = topic.getPublisher().ice_encodingVersion(Ice.Encoding_1_0)
            except PropertyService.PropertyException:
                return None
        else:
            try:
                prx = self.broker.get_property(devid + " | proxy")
                prx = self.broker._ic.stringToProxy(prx)
            except PropertyService.PropertyException:
                return None

        return SmartServices.MessageRenderPrx.uncheckedCast(prx)

    def get_transduces_in_route(self, route, type):
        devices = set()
        for place in route:
            trans = self.location.listTransducersByType(place, type)
            devices.update(trans)
        return devices


class VisualSignaling(Signaling):
    def configureRoute(self, route):
        logging.info("- evacuation route received, setting visual env...")

        devices = self.get_transduces_in_route(route, "SmartScreen")
        if not devices:
            logging.info("  NO smart-screen devices available, nothing to do.")
            return

        logging.info("  smart-screen devices: " + str(devices))
        for dev in devices:
            self.deploy_signal(dev, use_topic=True)


class AudioSignaling(Signaling):
    def configureRoute(self, route):
        logging.info("- evacuation route received, setting audible env...")

        devices = self.get_transduces_in_route(route, "Speaker")
        if not devices:
            logging.info("  NO speaker devices available, nothing to do.")
            return

        logging.info("  speaker devices: " + str(devices))
        for dev in devices:
            self.deploy_signal(dev)


class Settings:
    validity = 5  # timeout to again retrieve props

    def __init__(self, type_, broker):
        self.type = type_
        self.broker = broker
        self.props = {}
        self.last_retrieve = time() - 1000

    def update(self):
        key = "signaling-settings | " + self.type.lower()
        try:
            self.props.update(self.broker.get_property(key))
        except PropertyService.PropertyException:
            logging.error(" missing PS property: {}".format(key))

    def get(self, key):
        if time() - self.last_retrieve > self.validity:
            self.update()
        return self.props.get(key)


class SignalingService(Ice.Application):
    def run(self, args):
        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)
        location = self.get_service_proxy("LocationService")

        visual_settings = Settings("Visual", self.broker)
        audio_settings = Settings("Audio", self.broker)
        self.signals = VisualSignaling(self.broker, location, visual_settings)
        self.speakers = AudioSignaling(self.broker, location, audio_settings)

        self.broker.subscribe("EvacuationRoute", self.on_evacuation_route)
        logging.info("SignalingService ready, waiting events...")
        try:
            self.broker.wait_for_events()
        except KeyboardInterrupt:
            pass

    def get_service_proxy(self, name):
        key = name + ".Proxy"
        prx = self.ic.propertyToProxy(key)
        if prx is None:
            logging.error(" missing property: {}".format(key))
            return

        if name == "LocationService":
            return SmartServices.LocationServicePrx.checkedCast(prx)
        return prx

    def on_evacuation_route(self, route):
        try:
            self.signals.configureRoute(route)
            self.speakers.configureRoute(route)
        except Exception as e:
            logging.warning("  ERROR handling route! " + str(e))


if __name__ == "__main__":
    SignalingService().main(sys.argv)
