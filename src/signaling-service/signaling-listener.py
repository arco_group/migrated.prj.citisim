#!/usr/bin/python3

import sys
import Ice
from libcitisim import Broker


class Listener(Ice.Application):
    def run(self, args):
        if len(args) != 2:
            print("Usage: {} <source-id>".format(args[0]))
            return 1

        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)

        def callback(messageID):
            print("set screen image to " + messageID)

        print("Waiting events...")
        self.broker.subscribe_to_publisher(args[1], callback)
        self.broker.wait_for_events()


if __name__ == "__main__":
    Listener(1).main(sys.argv)


