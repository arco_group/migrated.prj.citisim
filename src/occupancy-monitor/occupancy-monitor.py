#!/usr/bin/python3

import sys
import Ice
import logging
from libcitisim import Broker, SmartEnvironment

logging.basicConfig(level=logging.INFO)


class OccupancyMonitorI(SmartEnvironment.OccupancyMonitor):
    def __init__(self):
        self.areas = {}

    def notify(self, changes, current=None):
        for c in changes:
            current = self.areas.get(c.areaOfInterest)
            if current is not None and c.amount == 0:
                self.areas.pop(c.areaOfInterest)
                logging.info("area now empty: {}".format(c.areaOfInterest))
                continue

            if c.amount > 0:
                self.areas[c.areaOfInterest] = c.amount
                logging.info("area updated: {}: {}".format(c.areaOfInterest, c.amount))

    def getOccupiedPlaces(self, current):
        return list(self.areas.keys())


class OccupancyMonitorServer(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        self.broker = Broker(ic=ic)

        monitor = OccupancyMonitorI()
        self.broker.subscribe("OccupancyChanges", monitor.notify)
        prx = self.broker.adapter.add(monitor, ic.stringToIdentity("OccupancyMonitor"))

        logging.info("Proxy: '{}'".format(prx))
        logging.info("Service ready, waiting events...")
        self.broker.wait_for_events()


if __name__ == "__main__":
    OccupancyMonitorServer().main(sys.argv)