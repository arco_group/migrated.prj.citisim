#!/usr/bin/python3

import unittest
import sys
import logging
from doublex import Spy, assert_that, called, Stub, ANY_ARG, Mock
from xiaomi_publisher import DoorSensor, MotionSensor, WeatherSensor, Switch


class XiaomiPublisherTests(unittest.TestCase):
    def TestMotionSensor(self):
        dev = Stub()
        publisher = Spy()

        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234": "1234"}
        meta = None
        
        with Spy() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234"], 
                transducer_type="PIRSensor", 
                meta=meta
            ).returns(publisher)

        sut = MotionSensor(broker, dev, source_ids, meta)
        sut.publish_motion(event=None, dev=dev)

        assert_that(broker.get_publisher, called())
        assert_that(publisher.publish, called())
        
    def TestDoorSensor(self):
        dev = Stub()
        publisher = Spy()

        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234": "1234"}
        meta=None

        with Spy() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234"], 
                transducer_type="DoorSensor", 
                meta=meta
            ).returns(publisher)
        
        sut = DoorSensor(broker, dev, source_ids, meta)
        sut.publish_door_open(event=None, dev=dev)

        assert_that(broker.get_publisher, called())
        assert_that(publisher.publish, called())

    def TestWeatherSensorTemperature(self):
        dev = Stub()
        publisher_temperature = Spy()
        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234.Temperature": "1", 
                      "Sensor.SID.1234.Pressure": "2", 
                      "Sensor.SID.1234.Humidity": "3"}
        meta = None

        with Mock() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Temperature"], 
                transducer_type="TemperatureSensor", 
                meta=meta
            ).returns(publisher_temperature)
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Pressure"], 
                transducer_type="PressureSensor", 
                meta=meta
            )
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Humidity"], 
                transducer_type="HumiditySensor", 
                meta=meta
            )

        sut = WeatherSensor(broker, dev, source_ids, meta=None)
        sut.publish_temperature(event=None, dev=dev, value=3000)

        assert_that(broker.get_publisher, called())
        assert_that(publisher_temperature.publish, called().with_args(30))

    def TestWeatherSensorPressure(self):
        dev = Stub()
        publisher_pressure = Spy()
        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234.Temperature": "1", 
                      "Sensor.SID.1234.Pressure": "2", 
                      "Sensor.SID.1234.Humidity": "3"}
        meta = None

        with Mock() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Temperature"], 
                transducer_type="TemperatureSensor", 
                meta=meta
            )
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Pressure"], 
                transducer_type="PressureSensor", 
                meta=meta
            ).returns(publisher_pressure)
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Humidity"], 
                transducer_type="HumiditySensor", 
                meta=meta
            )

        sut = WeatherSensor(broker, dev, source_ids, meta=None)
        sut.publish_pressure(event=None, dev=dev, value=120000)

        assert_that(broker.get_publisher, called())
        assert_that(publisher_pressure.publish, called().with_args(120))

    def TestWeatherSensorHumidity(self):
        dev = Stub()
        publisher_humidity = Spy()
        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234.Temperature": "1", 
                      "Sensor.SID.1234.Pressure": "2", 
                      "Sensor.SID.1234.Humidity": "3"}
        meta = None

        with Mock() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Temperature"], 
                transducer_type="TemperatureSensor", 
                meta=meta
            )
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Pressure"], 
                transducer_type="PressureSensor", 
                meta=meta
            )
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234.Humidity"], 
                transducer_type="HumiditySensor", 
                meta=meta
            ).returns(publisher_humidity)

        sut = WeatherSensor(broker, dev, source_ids, meta=None)
        sut.publish_humidity(event=None, dev=dev, value=5000)

        assert_that(broker.get_publisher, called())
        assert_that(publisher_humidity.publish, called().with_args(50))

    def TestSwitchSinglePressed(self):
        dev = Stub()
        publisher = Spy()

        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234": "1234"}
        meta=dict(timestamp=None)

        with Spy() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234"], 
                transducer_type="SwitchActuator", 
                meta=meta
            ).returns(publisher)
        
        sut = Switch(broker, dev, source_ids, meta)
        sut.publish_click_event(event=None, dev=dev)

        assert_that(broker.get_publisher, called())
        assert_that(publisher.publish, called())        

    def TestSwitchDoublePressed(self):
        dev = Stub()
        publisher = Spy()

        dev.sid = "1234"
        source_ids = {"Sensor.SID.1234": "1234"}
        meta=dict(timestamp=None)

        with Spy() as broker:
            broker.get_publisher(
                source=source_ids["Sensor.SID.1234"], 
                transducer_type="SwitchActuator", 
                meta=meta
            ).returns(publisher)
        
        sut = Switch(broker, dev, source_ids, meta)
        sut.publish_double_click_event(event=None, dev=dev)

        assert_that(broker.get_publisher, called())
        assert_that(publisher.publish, called())        


