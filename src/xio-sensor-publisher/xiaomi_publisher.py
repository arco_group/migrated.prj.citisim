#!/usr/bin/python3

import sys
import Ice
import time
import ast
import logging

from mijia import LumiGateway
from libcitisim import Broker


logging.basicConfig(level=logging.INFO)


class WeatherSensor:
    def __init__(self, broker, dev, source_ids, meta):
        dev.on_temperature(self.publish_temperature)
        dev.on_pressure(self.publish_pressure)
        dev.on_humidity(self.publish_humidity)
        pub = []
        pub.append(broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid + ".Temperature"],
            transducer_type="TemperatureSensor",
            meta=meta
        ))
        pub.append(broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid + ".Pressure"],
            transducer_type="PressureSensor",
            meta=meta
        ))
        pub.append(broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid + ".Humidity"],
            transducer_type="HumiditySensor",
            meta=meta
        ))

        self.publishers = pub
        self.sids = source_ids
        self.temperature_publisher = 0
        self.pressure_publisher = 1
        self.humidity_publisher = 2

    def publish_temperature(self, event, dev, value):
        value = value/100

        logging.info("[ {} ]: Temperature {}ºC".format(
            self.sids["Sensor.SID." + dev.sid + ".Temperature"], value))
        self.publishers[self.temperature_publisher].publish(value)

    def publish_humidity(self, event, dev, value):
        value = value/100

        logging.info("[ {} ]: Humidity {}%".format(
            self.sids["Sensor.SID." + dev.sid + ".Humidity"], value))
        self.publishers[self.humidity_publisher].publish(value)

    def publish_pressure(self, event, dev, value):
        value = value/1000

        logging.info("[ {} ]: Pressure {}kPa".format(
            self.sids["Sensor.SID." + dev.sid + ".Pressure"], value))
        self.publishers[self.pressure_publisher].publish(value)


class MotionSensor:
    def __init__(self, broker, dev, source_ids, meta):
        dev.on_motion(self.publish_motion)
        
        self.publisher = broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid],
            transducer_type="PIRSensor",
            meta=meta
        )
        self.sid = source_ids["Sensor.SID." + dev.sid]

    def publish_motion(self, event, dev):
        logging.info("[ {} ]: Motion detected".format(self.sid))
        self.publisher.publish(True)


class DoorSensor:
    def __init__(self, broker, dev, source_ids, meta):
        dev.on_open(self.publish_door_open)
        
        self.publisher = broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid],
            transducer_type="DoorSensor",
            meta=meta
        )
        self.sid = source_ids["Sensor.SID." + dev.sid]

    def publish_door_open(self, event, dev):
        logging.info("[ {} ]: Door opened".format(self.sid))
        self.publisher.publish(True)


class Switch:
    def __init__(self, broker, dev, source_ids, meta):
        dev.on_click(self.publish_click_event)
        dev.on_double_click(self.publish_double_click_event)

        self.publisher = broker.get_publisher(
            source=source_ids["Sensor.SID." + dev.sid],
            transducer_type="SwitchActuator",
            meta=dict(timestamp=None)
        )
        self.sid = source_ids["Sensor.SID." + dev.sid]

    def publish_click_event(self, event, dev):
        logging.info("[ {} ]: Switch Pressed".format(self.sid))
        self.publisher.publish(True)

    def publish_double_click_event(self, event, dev):
        logging.info("[ {} ]: Switch Double Pressed".format(self.sid))
        self.publisher.publish(False)


class XiaomiPublisher(Ice.Application):
    def run(self, args):
        self.devices_types = {
            "motion": MotionSensor,
            "weather.v1": WeatherSensor,
            "magnet": DoorSensor,
            "switch": Switch
        }

        self.broker = Broker(ic=self.communicator())

        password = self.broker.props.getProperty("Mijia.Gateway.Password")
        sid = self.broker.props.getProperty("Mijia.Gateway.Sid")
        meta = self.broker.props.getProperty("Mijia.Gateway.Metadata")

        self.meta = ast.literal_eval(meta) if meta else {}
        self.gw = LumiGateway(passwd=password, sid=sid)
        self.gw.start()
        self.setup_devices()
        self.gw.wait_until_break()

    def setup_devices(self):
        devices = self.gw.get_devices()

        for key, dev in devices.items():
            logging.info("{}: {}".format(key, dev))
            if dev.type in self.devices_types.keys():
                source_ids = self.broker.props.getPropertiesForPrefix(
                    "Sensor.SID." + dev.sid)

                if source_ids != {}:
                    self.devices_types[dev.type](
                        self.broker, dev, source_ids, self.meta)
        logging.info("")
            

if __name__ == "__main__":
    XiaomiPublisher(1).main(sys.argv)

