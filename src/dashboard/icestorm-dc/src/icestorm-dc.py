#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceStorm
import logging
import asyncio
import signal
import struct
from time import time
from threading import Lock

from libcitisim import SmartObject
from slices import Dashboard


logger = logging.getLogger("icestorm-dc")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


def run_interval(delta):
    def decorator(fn):
        async def _async_call(*args, **kwargs):
            while True:
                try:
                    fn(*args, **kwargs)
                    await asyncio.sleep(delta)
                except asyncio.CancelledError:
                    asyncio.get_event_loop().stop()
                    break
        return _async_call
    return decorator


class GlobalEventCounter:
    def __init__(self):
        self.count = 0
        self.lock = Lock()

    def get_count(self):
        with self.lock:
            return self.count

    def flush(self):
        with self.lock:
            self.count = 0

    def inc(self):
        with self.lock:
            self.count += 1

#
# FIXME: use a collocated adpater and register servants of supported
# types, then make invocations to check if they are valid, capture
# Unmarshall exceptions. Call using a unique UUID per invocation, use
# default servant with categories (one per interface), and store
# arguments using the UUID.
#
# NOTE: the following is the implementation of that approach, but it
# fails on Ice 3.6.3, raising a segfault on the ice_invoke that
# produces an un-marshalling error
#
# class Singleton(type):
#     _instance = None
#
#     def __call__(cls, *args, **kwargs):
#         if cls._instance is None:
#             cls._instance = super(Singleton, cls).__call__(*args, **kwargs)
#         return cls._instance
#
#
# class AnalogSinkI(SmartObject.AnalogSink):
#     def __init__(self):
#         self.events = {}
#
#     def notify(self, value, source, meta, current):
#         self.events[current.id] = dict(
#             value=value, source=source, meta=meta)
#
#     def pop_event(self, proxy):
#         try:
#             return self.events.pop(proxy.ice_getIdentity())
#         except (KeyError, IndexError) as e:
#             return None
#
#
# class IntrospectionTools(metaclass=Singleton):
#     def __init__(self, ic):
#         # Note: collocated adapters (name as "", no endpoints) do not query
#         # for default servants or locators
#         adapter = ic.createObjectAdapterWithEndpoints(
#             "DummyAdapter", "tcp -h 127.0.0.1")
#         adapter.activate()
#         self.baseProxy = adapter.createProxy(Ice.Identity("DummyObj"))
#
#         self.servants = {
#             "AnalogSink": AnalogSinkI(),
#         }
#         for category, servant in self.servants.items():
#             adapter.addDefaultServant(servant, category)
#
#     def get_unique_proxy(self, category):
#         oid = Ice.Identity(Ice.generateUUID(), category)
#         return self.baseProxy.ice_identity(oid)
#
#     def pop_event(self, proxy):
#         cat = self.proxy.ice_getIdentity().category
#         return self.servants[cat].pop_event(proxy)
#
#
# class IntrospectedMessage:
#     def __init__(self, params, current):
#         self.params = params
#         self.current = current
#
#         self.ic = current.adapter.getCommunicator()
#         self.tools = IntrospectionTools(self.ic)
#
#         self.value = None
#         self.source = None
#         self.meta = None
#
#     def is_AnalogSink(self):
#         # first test: operation mismatch
#         if self.current.operation != "notify":
#             return False
#
#         # second test: argument un-marshaling
#         try:
#             prx = self.tools.get_unique_proxy("AnalogSink")
#             prx.ice_invoke(self.current.operation, self.current.mode,
#                            self.params, self.current.ctx)
#             evt = self.tools.pop_event(prx)
#             self.__dict__.update(evt)
#             return True
#         except Exception as e:
#             return False


class IntrospectedMessage:
    def __init__(self, params, current):
        self.current = current
        self.params = params
        self._size = len(params)

    def is_AnalogSink(self):
        # signature: notify(float value, string source, Metadata data)
        # min size: 4 + 1 + 1 = 6 bytes
        self._idx = 0
        self.value = None
        self.source = None
        self.meta = None

        if not self._common_checks(min_size=6):
            return False

        # try to unmarshal message params
        try:
            value = self.readFloat()
            source = self.readString()
            meta = self.readMetadata()

            # ok, store them and return True
            self.value = value
            self.source = source
            self.meta = meta
            return True
        except Ice.UnmarshalOutOfBoundsException:
            return False

    def is_DigitalSink(self):
        # signature: notify(bool value, string source, Metadata data)
        # min size: 1 + 1 + 1 = 3 bytes
        self._idx = 0
        self.value = None
        self.source = None
        self.meta = None

        if not self._common_checks(min_size=3):
            return False

        # try to unmarshal message params
        try:
            value = self.readByte()
            source = self.readString()
            meta = self.readMetadata()

            # ok, store them and return True
            self.value = value
            self.source = source
            self.meta = meta
            return True
        except Ice.UnmarshalOutOfBoundsException:
            return False

    def _common_checks(self, min_size):
        # operation mismatch
        if self.current.operation != "notify":
            return False

        # invalid encapsulation of params
        if not self.readEncap():
            return False

        # minimum signature message size not reached
        if self._idx + min_size > self._size:
            return False

        return True

    def readEncap(self):
        # format: int size, byte major, byte minor, bytes params
        # min size: 4 + 1 + 1 = 6 bytes
        if self._size < 6:
            return False

        try:
            size = self.readInt()
            self.major = self.readByte()
            self.minor = self.readByte()
            if self.major != 1 or self.minor not in (0, 1):
                return False
            if self._idx + (size - 6) > self._size:
                return False
            return True

        except Ice.UnmarshalOutOfBoundsException:
            return False

    def readSize(self):
        size = self.readByte()
        if size == 255:
            size = self.readInt()
        return size

    def readByte(self):
        if self._idx >= self._size:
            raise Ice.UnmarshalOutOfBoundsException()
        value = self.params[self._idx]
        self._idx += 1
        return value

    def readInt(self):
        if self._idx + 4 > self._size:
            raise Ice.UnmarshalOutOfBoundsException()

        # Note: Ice is little-endian
        value = self.params[self._idx:self._idx+4]
        value = struct.unpack("<i", value)[0]
        self._idx += 4
        return value

    def readFloat(self):
        if self._idx + 4 > self._size:
            raise Ice.UnmarshalOutOfBoundsException()

        # Note: Ice is little-endian
        value = self.params[self._idx:self._idx+4]
        value = struct.unpack("<f", value)[0]
        self._idx += 4
        return value

    def readString(self):
        if self._idx >= self._size:
            raise Ice.UnmarshalOutOfBoundsException()

        size = self.readSize()
        if self._idx + size > self._size:
            raise Ice.UnmarshalOutOfBoundsException()

        value = self.params[self._idx:self._idx+size]
        try:
            value = value.decode("utf-8")
        except UnicodeDecodeError:
            # not the expected string
            raise Ice.UnmarshalOutOfBoundsException()

        self._idx += size
        return value

    def readMetadata(self):
        # dict format: size + (key, value) 'size' times
        # pair: <MetadataField, string>
        meta = {}
        size = self.readByte()
        for i in range(size):
            key = self.readMetadataField()
            value = self.readString()
            meta[key] = value
        return meta

    def readMetadataField(self):
        # this is an enum, with less than 255 items, so use a byte
        try:
            value = self.readByte()
            return SmartObject.MetadataField._enumerators[value]
        except KeyError:
            raise Ice.UnmarshalOutOfBoundsException()


class DynamicSubscriber(Ice.Blobject):
    def __init__(self, topic, event_counter, sensor_manager):
        self.topic = topic
        self.event_counter = event_counter
        self.sensor_manager = sensor_manager

    def ice_invoke(self, params, current):
        logger.debug(" [IN] topic '{}', operation: '{}'".format(
            self.topic, current.operation))
        self.event_counter.inc()

        msg = IntrospectedMessage(params, current)
        if msg.is_AnalogSink():
            iface = "SmartObject::Analogsink"
        elif msg.is_DigitalSink():
            iface = "SmartObject::Digitalsink"
        else:
            iface = "unknown"

        self.sensor_manager.update_sensor(msg, self.topic, iface)
        return True, bytes()


class SubscriberManager:
    def __init__(self, ic, event_counter, sensor_manager):
        self.ic = ic
        self.event_counter = event_counter
        self.sensor_manager = sensor_manager
        self.icestorm = None
        self.topics = {}

        self.adapter = self.ic.createObjectAdapter("IceStormDC.Adapter")
        self.adapter.activate()

    def set_icestorm(self, icestorm):
        self.icestorm = icestorm

    def sync(self, topics):
        # remove old topics
        for k in list(self.topics.keys()):
            if k not in topics:
                self.topics.pop(k)

        # add new topics
        for name, prx in topics.items():
            if name not in self.topics:
                self._new_topic(name, prx)

    def _new_topic(self, name, proxy):
        logger.info("New topic: '{}', subscribing observer...".format(name))
        subscriber = self._create_subscriber(name)
        proxy.subscribeAndGetPublisher({}, subscriber)
        self.topics[name] = proxy

    def _create_subscriber(self, topic_name):
        prx = self.adapter.addWithUUID(
            DynamicSubscriber(
                topic_name, self.event_counter, self.sensor_manager))
        return prx.ice_oneway()


class Sensor:
    def __init__(self, source, topic):
        self.source = source
        self.topic = topic
        self.interface = ""
        self.name = ""  # FIXME: get from PS

        self.status = Dashboard.Status.UNKNOWN
        self.notified = False
        self.last_event_time = None
        self.avg_time = None
        self.events = []

    def get_info(self):
        info = Dashboard.SensorInfo(
            source=self.source,
            name=self.name,
            sensorStatus=self.status,
            topic=self.topic,
            iface=self.interface,
            events=self.events
        )
        return info

    def update_status(self):
        if self.last_event_time is None or self.avg_time is None:
            self.status = Dashboard.Status.UNKNOWN
            return

        # we allow up to 15 seconds of drift
        elapsed = time() - self.last_event_time
        if elapsed > (self.avg_time + 15):
            if self.status != Dashboard.Status.DOWN:
                self.notified = False
            self.status = Dashboard.Status.DOWN
        else:
            self.status = Dashboard.Status.UP

    def add_event(self, value):
        if value is None:
            value = 0.0
        self.events.append(value)

        if self.last_event_time is None:
            self.last_event_time = time()
            return

        elapsed = time() - self.last_event_time
        self.last_event_time = time()
        if self.avg_time is None:
            self.avg_time = elapsed
            return

        # if status was DOWN, ignore elapsed time
        if self.status != Dashboard.Status.DOWN:
            self.avg_time = (self.avg_time + elapsed) / 2

        self.status = Dashboard.Status.UP

    def flush(self):
        self.events = []
        if self.status == Dashboard.Status.DOWN:
            self.notified = True


class SensorManager:
    def __init__(self):
        self.topics = {}

    def get_all_sensors(self):
        retval = []
        for sensors in self.topics.values():
            retval.extend(sensors.values())
        return retval

    def update_sensor(self, msg, topic, interface):
        source = msg.source
        if not source:
            source = "unknown"

        sensor = self.get_sensor(topic, source)
        sensor.interface = interface
        sensor.add_event(msg.value)

    def get_sensor(self, topic, source):
        sensors = self.topics.get(topic)
        if sensors is None:
            sensors = {}
            self.topics[topic] = sensors

        sensor = sensors.get(source)
        if sensor is None:
            sensor = Sensor(source, topic)
            sensors[source] = sensor

        return sensor


class IceStormDC(Ice.Application):
    def run(self, args):
        self.ic = self.communicator()
        self.loop = asyncio.get_event_loop()
        self.event_counter = GlobalEventCounter()
        self.sensor_manager = SensorManager()
        self.subscriber_manager = SubscriberManager(
            self.ic, self.event_counter, self.sensor_manager)

        try:
            return self._run_service(args)
        except AssertionError as e:
            logger.error(e)
            return -1

    def _run_service(self, args):
        self.manager = self._get_topic_manager()
        self.subscriber_manager.set_icestorm(self.manager)
        self.dash = self._get_dashboard_sink()

        # setup Ctrl+C and signal handlers
        def stop():
            for task in asyncio.Task.all_tasks():
                task.cancel()

        for s in (signal.SIGINT, signal.SIGTERM):
            self.loop.add_signal_handler(s, stop)

        logger.info("Service running...")
        self.loop.create_task(self._refresh_topic_subscription())
        self.loop.create_task(self._notify_status())
        self.loop.run_forever()
        self.loop.close()

    @run_interval(60)
    def _refresh_topic_subscription(self):
        try:
            topics = self.manager.retrieveAll()
            self.subscriber_manager.sync(topics)
        except Exception as ex:
            logger.error(ex)

    @run_interval(5)
    def _notify_status(self):
        # notify global event count
        try:
            self.dash.updateEventCount(self.event_counter.get_count())
            self.event_counter.flush()
        except Exception as e:
            logger.error("Can't update dashboard:" + str(e))

        for sensor in self.sensor_manager.get_all_sensors():
            if sensor.status == Dashboard.Status.DOWN and sensor.notified:
                continue

            logger.debug(
                "[OUT] sensor, topic: {}, source: {}, status: {}".format(
                    sensor.topic, sensor.source, sensor.status))
            try:
                sensor.update_status()
                self.dash.updateSensor(sensor.get_info())
                sensor.flush()
            except Exception as e:
                logger.error("Can't update dashboard:" + str(e))

    def _get_topic_manager(self):
        key = "IceStormDC.TopicManager.Proxy"
        manager = self.ic.propertyToProxy(key)
        assert manager is not None, \
            "'{}' property not correctly defined.".format(key)

        return IceStorm.TopicManagerPrx.checkedCast(manager)

    def _get_dashboard_sink(self):
        key = "DashboardDS.Proxy"
        dash = self.ic.propertyToProxy(key)
        assert dash is not None, \
            "'{}' property not correctly defined.".format(key)

        # Remove default router (used to reach IceStorm TopicManager)
        dash = dash.ice_router(None)
        return Dashboard.DataSinkPrx.checkedCast(dash)


if __name__ == '__main__':
    IceStormDC().main(sys.argv)
