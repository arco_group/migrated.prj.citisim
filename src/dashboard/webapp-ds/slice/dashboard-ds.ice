// -*- mode: c++; coding: utf-8 -*-

module Dashboard {

    enum Status { ACTIVE, INACTIVE, UP, DOWN, UNKNOWN };
    enum ActivationMode { MANUAL, ALWAYS, ONDEMAND, SESSION };
    sequence<float> FloatSeq;

    struct NodeInfo {
        string name;
        Status nodeStatus;
        int uptime;
        int downtime;
        FloatSeq load;
    };

    struct ServiceInfo {
        string name;
        string node;
        Status serviceStatus;
        ActivationMode mode;
    };

    struct SensorInfo {
        string source;
        string name;
        Status sensorStatus;
        string topic;
        string iface;
        FloatSeq events;
    };

    interface DataSink {
        void updateNode(NodeInfo node);
        void updateService(ServiceInfo service);
        void updateSensor(SensorInfo sensor);
        void updateEventCount(int count);
    };
};
