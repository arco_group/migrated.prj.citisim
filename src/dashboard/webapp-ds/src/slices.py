# -*- mode: python; coding: utf-8 -*-

import os
import Ice

pwd = os.path.abspath(os.path.dirname(__file__))
slice_path = os.path.join(pwd, "../slice/dashboard-ds.ice")
if not os.path.exists(slice_path):
    slice_path = "/usr/share/slice/citisim/dashboard-ds.ice"

Ice.loadSlice(slice_path)
import Dashboard
