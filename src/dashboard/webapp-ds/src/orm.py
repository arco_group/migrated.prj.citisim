# -*- mode: python; coding: utf-8 -*-

import os
import sys
import django

pwd = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(pwd, "../../webapp/"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dashboard.settings")
django.setup()

from app import models
