#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import logging
from datetime import date
from orm import models
from slices import Dashboard

logger = logging.getLogger("dashboard-ds")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

STATUS_MAP = {
    Dashboard.Status.UP: models.STATUS_ON,
    Dashboard.Status.ACTIVE: models.STATUS_ON,
    Dashboard.Status.DOWN: models.STATUS_OFF,
    Dashboard.Status.INACTIVE: models.STATUS_OFF,
    Dashboard.Status.UNKNOWN: models.STATUS_UNK,
}

MODE_MAP = {
    Dashboard.ActivationMode.MANUAL: models.ACT_MODE_MAN,
    Dashboard.ActivationMode.ALWAYS: models.ACT_MODE_ALW,
    Dashboard.ActivationMode.ONDEMAND: models.ACT_MODE_OND,
    Dashboard.ActivationMode.SESSION: models.ACT_MODE_SES,
}


class DataSinkI(Dashboard.DataSink):
    def updateNode(self, node, current):
        dbnode = models.Node.objects.get_or_create(name=node.name)[0]
        dbnode.status = STATUS_MAP.get(node.nodeStatus, models.STATUS_UNK)
        dbnode.up_time += node.uptime
        dbnode.down_time += node.downtime
        dbnode.load_avg = self._merge_items(
            dbnode.load_avg, node.load, lambda x: x*100)
        dbnode.save()

    def updateService(self, service, current):
        dbservice = models.Service.objects.get_or_create(name=service.name)[0]
        try:
            node = models.Node.objects.get(name=service.node)
        except models.Node.DoesNotExist:
            node = None
        dbservice.node = node
        dbservice.status = STATUS_MAP.get(
            service.serviceStatus, models.STATUS_UNK)
        dbservice.activation_mode = MODE_MAP.get(
            service.mode, models.ACT_MODE_MAN)
        dbservice.save()

    def updateSensor(self, sensor, current):
        dbsensor = models.Sensor.objects.get_or_create(
            source=sensor.source, topic=sensor.topic)[0]

        dbsensor.name = sensor.name
        dbsensor.status = STATUS_MAP.get(
            sensor.sensorStatus, models.STATUS_UNK)
        dbsensor.topic = sensor.topic
        dbsensor.interface = sensor.iface
        dbsensor.event_count += len(sensor.events)
        dbsensor.last_events = self._merge_items(
            dbsensor.last_events, sensor.events)
        dbsensor.save()

    def updateEventCount(self, count, current):
        events = models.EventCount.objects.get_or_create(
            date=date.today())[0]
        events.add_to_count(count)
        events.save()

    def _merge_items(self, old, new, fn=lambda x: x):
        fields = []
        if old:
            fields = old.split(",")
        for x in new:
            x = str(int(fn(x)))
            fields.append(x)

        # get only up to last 50 elements
        return ",".join(fields[-50:])


class DashboardDS(Ice.Application):
    def run(self, args):
        try:
            return self._run_service(args)
        except AssertionError as e:
            logger.error(e)
            return -1

    def _run_service(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapter("DashboardDS.Adapter")
        adapter.activate()

        prx = adapter.add(DataSinkI(), ic.stringToIdentity("DashboardDS"))
        logger.info("Service ready: '{}'".format(prx))

        logger.info("Waiting for events...")
        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == '__main__':
    DashboardDS().main(sys.argv)
