#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceGrid
import logging
import asyncio
import signal
from time import time

from slices import Dashboard


logger = logging.getLogger("icegrid-dc")
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

# we use 'avg1' of IceGrid.Admin.getNodeLoad()
LOAD_RANGE_TIME = 1 * 60


def run_interval(delta):
    def decorator(fn):
        async def _async_call(*args, **kwargs):
            while True:
                try:
                    fn(*args, **kwargs)
                    await asyncio.sleep(delta)
                except asyncio.CancelledError:
                    asyncio.get_event_loop().stop()
                    break
        return _async_call
    return decorator


def get_status(state):
    if state == IceGrid.ServerState.Active:
        return Dashboard.Status.UP
    if state in [IceGrid.ServerState.Inactive,
                 IceGrid.ServerState.Destroyed]:
        return Dashboard.Status.DOWN
    return Dashboard.Status.UNKNOWN


class ReuseConnectionRouter(Ice.Router):
    def __init__(self, proxy):
        self._clientProxy = proxy

    def getClientProxy(self, current=None):
        return self._clientProxy, False

    def getServerProxy(self, current):
        return None

    def addProxies(self, proxies, current):
        return []


class RegistryObserverI(IceGrid.RegistryObserver):
    def registryDown(self, name, current):
        pass

    def registryInit(self, registries, current):
        pass

    def registryUp(self, node, current):
        pass


class NodeObserverI(IceGrid.NodeObserver):
    def __init__(self, node_manager, service_manager):
        self.node_manager = node_manager
        self.service_manager = service_manager

    def nodeInit(self, nodes, current):
        for n in nodes:
            node = self.node_manager.get_node(n.info.name)
            node.up()

            for s in n.servers:
                service = self.service_manager.get_service(
                    s.id, node=node.name)
                service.status = get_status(s.state)

    def nodeDown(self, name, current):
        node = self.node_manager.get_node(name)
        node.down()

    def nodeUp(self, node, current):
        node = self.node_manager.get_node(node.info.name)
        node.up()

    def updateAdapter(self, name, updatedInfo, current):
        pass

    def updateServer(self, node_name, info, current):
        service = self.service_manager.get_service(info.id)
        service.status = get_status(info.state)


class ApplicationObserverI(IceGrid.ApplicationObserver):
    def __init__(self, node_manager, service_manager, registry):
        self.node_manager = node_manager
        self.service_manager = service_manager
        self.registry = registry

    def applicationAdded(self, serial, desc, current):
        self._sync_node_manager()

    def applicationInit(self, serial, applications, current):
        self._sync_node_manager()

    def applicationRemoved(self, serial, name, current):
        self._sync_node_manager()

    def applicationUpdated(self, serial, desc, current):
        self._sync_node_manager()

    def _sync_node_manager(self):
        nodes = self.registry.getAllNodeNames()
        self.node_manager.sync_nodes(nodes)
        services = self.registry.getAllServerIds()
        self.service_manager.sync_services(services)


class AdapterObserverI(IceGrid.AdapterObserver):
    def adapterAdded(self, info, current):
        pass

    def adapterInit(self, adapters, current):
        pass

    def adapterRemoved(self, oid, current):
        pass

    def adapterUpdated(self, info, current):
        pass


class ObjectObserverI(IceGrid.ObjectObserver):
    def objectAdded(self, info, current):
        pass

    def objectInit(self, objects, current):
        pass

    def objectRemoved(self, oid, current):
        pass

    def objectUpdated(self, info, current):
        pass


class Node:
    def __init__(self, name, admin):
        self.name = name
        self.status = Dashboard.Status.UNKNOWN
        self.admin = None

        # flush-able stats
        self.uptime = 0
        self.downtime = 0
        self.load_range = LOAD_RANGE_TIME
        self.load = []
        self.last_check = time()

        if admin is not None:
            self.admin = IceGrid.AdminPrx.uncheckedCast(
                admin.ice_invocationTimeout(2500))

    def down(self):
        self.status = Dashboard.Status.DOWN

    def up(self):
        self.status = Dashboard.Status.UP

    def update_load(self):
        if self.admin is None:
            logger.error("can not update node, admin not provided")
            return

        try:
            load = self.admin.getNodeLoad(self.name)
            self.status = Dashboard.Status.UP
            self.load.append(load.avg1)
        except IceGrid.NodeUnreachableException as ex:
            self.status = Dashboard.Status.DOWN

    def update_uptime(self):
        elapsed = time() - self.last_check
        self.last_check = time()
        if self.status == Dashboard.Status.UP:
            self.uptime += elapsed
        elif self.status == Dashboard.Status.DOWN:
            self.downtime += elapsed

    def flush(self):
        self.load = []
        self.uptime = 0
        self.downtime = 0

    def get_info(self):
        info = Dashboard.NodeInfo(
            name=self.name,
            nodeStatus=self.status,
            uptime=self.uptime,
            downtime=self.downtime,
            load=self.load
        )
        return info


class NodeManager:
    def __init__(self):
        self.registry = None
        self.nodes = {}

    def set_registry(self, registry):
        self.registry = registry

    def get_all_nodes(self):
        return self.nodes.values()

    def get_node(self, name):
        node = self.nodes.get(name, None)
        if node is None:
            node = self._new_node(name)
        return node

    def _new_node(self, name):
        node = Node(name, self.registry)
        if self.registry is not None:
            self.nodes[name] = node
        return node

    def sync_nodes(self, names):
        for k in list(self.nodes.keys()):
            if k not in names:
                self.nodes.pop(k)
        for k in names:
            if k not in self.nodes:
                self._new_node(k)


class Service:
    def __init__(self, name, node="unknown", mode="manual"):
        self.name = name
        self.node = node
        self.status = Dashboard.Status.UNKNOWN

        self.mode = {
            'manual': Dashboard.ActivationMode.MANUAL,
            'always': Dashboard.ActivationMode.ALWAYS,
            'on-demand': Dashboard.ActivationMode.ONDEMAND,
            'session': Dashboard.ActivationMode.SESSION,
        }[mode]

    def get_info(self):
        info = Dashboard.ServiceInfo(
            name=self.name,
            node=self.node,
            serviceStatus=self.status,
            mode=self.mode,
        )
        return info


class ServiceManager:
    def __init__(self):
        self.services = {}
        self.registry = None

    def set_registry(self, registry):
        self.registry = registry

    def get_all_services(self):
        return self.services.values()

    def get_service(self, name, node=None):
        service = self.services.get(name)
        if service is None:
            service = self._new_service(name, node)
        return service

    def _new_service(self, name, node=None):
        mode = "manual"
        if self.registry is not None:
            info = self.registry.getServerInfo(name)
            node = info.node
            mode = info.descriptor.activation

        if node is None:
            node = "<unknown>"

        status = Dashboard.Status.UNKNOWN
        if self.registry is not None:
            status = self.registry.getServerState(name)
            status = get_status(status)

        service = Service(name, node, mode)
        service.status = status
        self.services[name] = service
        return service

    def sync_services(self, names):
        for k in list(self.services.keys()):
            if k not in names:
                self.services.pop(k)
        for k in names:
            if k not in self.services:
                self._new_service(k)


class IceGridDC(Ice.Application):
    def run(self, args):
        self.ic = self.communicator()
        self.loop = asyncio.get_event_loop()
        self.node_manager = NodeManager()
        self.service_manager = ServiceManager()

        try:
            return self._run_service(args)
        except AssertionError as e:
            logger.error(e)
            return -1

    def _run_service(self, args):
        self.registry = self._get_registry_admin()
        self.node_manager.set_registry(self.registry)
        self.service_manager.set_registry(self.registry)
        self.dash = self._get_dashboard_sink()

        # setup Ctrl+C and signal handlers
        def stop():
            logger.info("Stopping service...")
            for task in asyncio.Task.all_tasks():
                task.cancel()

        for s in (signal.SIGINT, signal.SIGTERM):
            self.loop.add_signal_handler(s, stop)

        logger.info("Service running...")
        self.loop.create_task(self._retrieve_node_load())
        self.loop.create_task(self._notify_status())
        self.loop.run_forever()
        self.loop.close()

    @run_interval(LOAD_RANGE_TIME)
    def _retrieve_node_load(self):
        nodes = self.registry.getAllNodeNames()
        for name in nodes:
            try:
                node = self.node_manager.get_node(name)
                node.update_load()
            except Exception as ex:
                logger.error(ex)

    @run_interval(5)
    def _notify_status(self):
        for node in self.node_manager.get_all_nodes():
            try:
                node.update_uptime()
                self.dash.updateNode(node.get_info())
                node.flush()
            except Exception as e:
                logger.error("Can't update dashboard:" + str(e))

        for service in list(self.service_manager.get_all_services()):
            try:
                self.dash.updateService(service.get_info())
            except Exception as e:
                logger.error("Can't update dashboard:" + str(e))

    def _get_registry_admin(self):
        key = "IceGridDC.Registry.Proxy"
        registry = self.ic.propertyToProxy(key)
        assert registry is not None, \
            "'{}' property not correctly defined.".format(key)

        props = self.ic.getProperties()
        registry = IceGrid.RegistryPrx.checkedCast(registry)

        # reuse connection to registry for all communications, useful when
        # Registry is inside a firewall and only 4061 port is open
        alloc_adapter = self.ic.createObjectAdapter("")
        router = Ice.RouterPrx.uncheckedCast(
            alloc_adapter.addWithUUID(ReuseConnectionRouter(registry)))
        self.ic.setDefaultRouter(router)

        # create session and setup ACM heartbeats to avoid closing
        creds = props.getProperty("IceGridDC.Registry.Credentials")
        user, passwd = creds.split(" ")
        session = registry.createAdminSession(user, passwd)
        acm_timeout = registry.getACMTimeout()
        registry.ice_getConnection().setACM(
            acm_timeout, Ice.Unset, Ice.ACMHeartbeat.HeartbeatAlways)

        admin = session.getAdmin()
        self._setup_icegrid_observers(registry, session, admin)
        return admin

    def _setup_icegrid_observers(self, registry, session, admin):
        endps = self.ic.getProperties().getPropertyWithDefault(
            "CallbackAdapter.Endpoints", "tcp")
        adapter = self.ic.createObjectAdapterWithEndpoints(
            "CallbackAdapter", endps)
        adapter.activate()

        registry_observer = IceGrid.RegistryObserverPrx.uncheckedCast(
            adapter.add(RegistryObserverI(), Ice.Identity("obs-reg")))
        node_observer = IceGrid.NodeObserverPrx.uncheckedCast(
            adapter.add(NodeObserverI(self.node_manager, self.service_manager),
                        Ice.Identity("obs-node")))
        application_observer = IceGrid.ApplicationObserverPrx.uncheckedCast(
            adapter.add(ApplicationObserverI(self.node_manager,
                                             self.service_manager,
                                             admin),
                        Ice.Identity("obs-app")))
        adapter_observer = IceGrid.AdapterObserverPrx.uncheckedCast(
            adapter.add(AdapterObserverI(), Ice.Identity("obs-adap")))
        object_observer = IceGrid.ObjectObserverPrx.uncheckedCast(
            adapter.add(ObjectObserverI(), Ice.Identity("obs-obj")))

        session.setObservers(
            registry_observer,
            node_observer,
            application_observer,
            adapter_observer,
            object_observer,
        )

    def _get_dashboard_sink(self):
        key = "DashboardDS.Proxy"
        dash = self.ic.propertyToProxy(key)
        assert dash is not None, \
            "'{}' property not correctly defined.".format(key)

        # Remove default router (used to reach IceGrid Registry)
        dash = dash.ice_router(None)
        return Dashboard.DataSinkPrx.checkedCast(dash)


if __name__ == '__main__':
    IceGridDC().main(sys.argv)
