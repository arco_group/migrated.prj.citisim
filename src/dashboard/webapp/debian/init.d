#!/bin/sh
### BEGIN INIT INFO
# Provides:          citisim-dashboard
# Required-Start:    $local_fs $network $remote_fs $syslog
# Required-Stop:     $local_fs $network $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Citisim Dashboard Webapp
# Description:       This service runs the Dashboard of Citisim
### END INIT INFO

# Author: Arco Research Group <arco.uclm@gmail.com>

DIR=/usr/share/citisim-dashboard/webapp/
DAEMON=$(which django-admin)
DAEMON_NAME=citisim-dashboard-webapp
export DJANGO_SETTINGS_MODULE=dashboard.settings
export PYTHONPATH='/usr/share/citisim-dashboard/webapp/'

# Add any command line options for your daemon here
HOST=$('grep' host /etc/default/citisim-dashboard | cut -d= -f2 | awk '{print $1}')
PORT=$('grep' port /etc/default/citisim-dashboard | cut -d= -f2 | awk '{print $1}')
DAEMON_OPTS="runserver $HOST:$PORT --insecure"

# This next line determines what user the script runs as.
DAEMON_USER=ice

# The process ID of the script when it runs is stored here:
PIDFILE=/var/run/$DAEMON_NAME.pid

. /lib/lsb/init-functions

do_start () {
    start-stop-daemon \
	--start --background --no-close \
	--pidfile $PIDFILE --make-pidfile \
	--user $DAEMON_USER --chuid $DAEMON_USER \
	--startas $DAEMON -- $DAEMON_OPTS
}

do_stop () {
    start-stop-daemon --stop --pidfile $PIDFILE --retry 10
}

case "$1" in

    start|stop)
        do_${1}
        ;;

    restart|reload|force-reload)
        do_stop
        do_start
        ;;

    status)
        status_of_proc "$DAEMON_NAME" "$DAEMON" && exit 0 || exit $?
        ;;
    *)
        echo "Usage: /etc/init.d/$DAEMON_NAME {start|stop|restart|status}"
        exit 1
        ;;

esac
exit 0
