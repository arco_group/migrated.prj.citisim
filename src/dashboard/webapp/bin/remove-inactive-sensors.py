#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import os

try:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dashboard.settings")
    import django
    django.setup()
except ImportError:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    )

from django.db.models import Q
from app.models import Sensor


class Cleaner:
    def __init__(self):
        active_sensors_q = Q(status="active") | Q(status="on")
        others = Sensor.objects.all().exclude(active_sensors_q)
        others.delete()


if __name__ == "__main__":
    Cleaner()
