#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import os
import sys

try:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dashboard.settings")
    import django
    django.setup()
except ImportError:
    raise ImportError(
        "Couldn't import Django. Are you sure it's installed and "
        "available on your PYTHONPATH environment variable? Did you "
        "forget to activate a virtual environment?"
    )

from django.contrib.auth.models import User
from faker import Factory
from random import choice, randint
from datetime import datetime, timedelta, date
from django.utils import timezone
from uuid import uuid4
from app.models import (
    Node, Service, Sensor, EventCount,
    STATUS_CHOICES, ACT_CHOICES, SENSOR_STATUS_CHOICES,
    EVENTS_PER_DAY, EVENTS_DELTA
)

fake = Factory.create()


COMMON_TOPICS = [
    "Temperature", "Anemometer", "Battery", "CarbonDioxide",
    "Humidity", "Occupancy", "OccupancyChanges", "Ozone",
    "PersonMovement", "Power", "Vibration", "WindVane",
]


class Populator:
    def __init__(self):
        self.remove_all()
        self.create_superuser()
        self.create_nodes(3)
        self.create_services(7)
        self.create_sensors(5)
        self.create_event_stats()

    def create_superuser(self):
        print("Creating admin user...")
        User.objects.create_superuser('admin', 'admin@example.com', 'qwerty')

    def create_nodes(self, count=None):
        if count is None:
            count = randint(1, 5)

        print("Creating {} Node objects...".format(count))
        self.nodes = []
        for i in range(count):
            n = Node(
                status=choice(STATUS_CHOICES)[0],
                name=fake.domain_word(),
                up_time=fake.random_int() * 10,
                down_time=fake.random_int(),
                load_avg=self.gen_events(limit=50),
            )
            n.save()
            self.nodes.append(n)

    def create_services(self, count=None):
        if count is None:
            count = randint(5, 15)

        print("Creating {} Service objects...".format(count))
        self.services = []
        for i in range(count):
            s = Service(
                node=choice(self.nodes),
                name=(fake.job().split(" ")[0].strip(",") +
                      choice([" Server", " Service", " Notifier"])),
                status=choice(STATUS_CHOICES)[0],
                activation_mode=choice(ACT_CHOICES)[0],
            )
            s.save()
            self.services.append(s)

    def create_sensors(self, count=None):
        if count is None:
            count = randint(0, 10)

        print("Creating {} Sensor objects...".format(count))
        self.sensors = []
        for i in range(count):
            iface = choice(
                ["", "SmartObject::DigitalSink", "SmartObject::AnalogSink"]
            )
            s = Sensor(
                name=fake.job().split(" ")[0].strip(",") + " Sensor",
                source=self.gen_source_id(),
                status=choice(SENSOR_STATUS_CHOICES)[0],
                topic=choice(COMMON_TOPICS),
                interface=iface,
                event_count=randint(0, 10000),
                last_events="" if not iface else self.gen_events(
                    limit=300, boolean=iface.endswith("DigitalSink")),
            )
            s.save()
            self.sensors.append(s)

    def create_event_stats(self, num_days=45):
        holder = "[___/___]"
        print("Creating events... {}".format(holder), end='')

        day = date.today()
        start = timezone.make_aware(datetime(day.year, day.month, day.day))
        today_elapsed = timezone.now() - start
        today_elems = today_elapsed // timedelta(seconds=EVENTS_DELTA)

        for i in range(num_days):
            delta = timedelta(days=i)
            num_events = EVENTS_PER_DAY
            today_start = start - delta
            today_mark = today_start + timedelta(seconds=85800)

            if i == 0:
                num_events = today_elems
                today_mark = today_start + timedelta(seconds=num_events*60*10)
            elif i == num_days-1:
                num_events = EVENTS_PER_DAY - today_elems
                today_start += timedelta(seconds=num_events * EVENTS_DELTA)

            record = EventCount.objects.get_or_create(
                date=day-delta, defaults={'start': today_start})[0]

            record.events = self.gen_events(num_events, start_value=100)
            record.last_time_mark = today_mark
            record.save()
            print(("\b" * len(holder)) +
                  "[{: 3}/{: 3}]".format(i+1, num_days), end='')
            sys.stdout.flush()
        print('')

    def remove_all(self):
        print("Removing all previous objects...")
        User.objects.all().delete()
        Node.objects.all().delete()
        Service.objects.all().delete()
        Sensor.objects.all().delete()
        EventCount.objects.all().delete()

    def gen_events(self, count=None, limit=30, start_value=0, boolean=False):
        if count is None:
            count = randint(0, limit)

        records = []
        last = start_value
        for i in range(count):
            if boolean:
                last = choice([0, 1])
            else:
                last = max(last + randint(-5, 5), 0)
            records.append(last)
        return ",".join(map(str, records))

    def gen_source_id(self, size=8):
        return uuid4().hex[:size].upper()


if __name__ == "__main__":
    Populator()
