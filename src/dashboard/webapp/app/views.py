# -*- mode: python; coding: utf-8 -*-

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from .models import Node, Service, Sensor, EventFilter


@login_required
def index(request):
    evt_filter = request.GET.get("evt_filter", "day")
    active_sensors_q = Q(status="active") | Q(status="on")
    context = {
        "nodes": Node.objects.all(),
        "services": Service.objects.all(),
        "active_sensors": Sensor.objects.filter(active_sensors_q),
        "other_sensors": Sensor.objects.all().exclude(active_sensors_q),
        "events": EventFilter(evt_filter),
    }
    return render(request, "app/dashboard.html", context)


@login_required
def remove_service(request, service_id):
    try:
        Service.objects.get(id=service_id).delete()
    except Service.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse("index"))


@login_required
def remove_node(request, node_id):
    try:
        Node.objects.get(id=node_id).delete()
    except Node.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse("index"))


@login_required
def remove_sensor(request, sensor_id):
    try:
        Sensor.objects.get(id=sensor_id).delete()
    except Sensor.DoesNotExist:
        pass
    return HttpResponseRedirect(reverse("index"))
