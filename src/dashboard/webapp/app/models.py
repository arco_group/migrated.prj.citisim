# -*- mode: python; coding: utf-8 -*-

from django.db import models
from django.utils.timezone import now
from datetime import timedelta, date

# possible status of nodes/services
STATUS_ON = "on"
STATUS_OFF = "off"
STATUS_UNK = "unknown"
STATUS_ACT = "active"    # specific for sensors
STATUS_INA = "inactive"  # specific for sensors

STATUS_CHOICES = (
    (STATUS_ON, STATUS_ON),
    (STATUS_OFF, STATUS_OFF),
    (STATUS_UNK, STATUS_UNK),
)

SENSOR_STATUS_CHOICES = STATUS_CHOICES + (
    (STATUS_ACT, STATUS_ACT),
    (STATUS_INA, STATUS_INA),
)

# activation modes
ACT_MODE_ALW = "always"
ACT_MODE_MAN = "manual"
ACT_MODE_OND = "on-demand"
ACT_MODE_SES = "session"

ACT_CHOICES = (
    (ACT_MODE_ALW, ACT_MODE_ALW),
    (ACT_MODE_MAN, ACT_MODE_MAN),
    (ACT_MODE_OND, ACT_MODE_OND),
    (ACT_MODE_SES, ACT_MODE_SES),
)

EVENTS_DELTA = 600    # seconds of each record on csv
EVENTS_PER_DAY = int(60 * 60 * 24 / EVENTS_DELTA)


class Node(models.Model):
    name = models.CharField(max_length=64, blank=False)
    status = models.CharField(
        max_length=4, default=STATUS_OFF, choices=STATUS_CHOICES)
    up_time = models.IntegerField(default=0)
    down_time = models.IntegerField(default=0)
    load_avg = models.TextField(blank=True)

    def is_online(self):
        return self.status == STATUS_ON

    def online_time(self):
        total = (self.up_time + self.down_time)
        if total is 0:
            return 0
        return int((self.up_time * 100) / (self.up_time + self.down_time))

    def online_str(self):
        if not self.up_time:
            return "0 seconds"
        return self._seconds_to_str(self.up_time)

    def offline_time(self):
        return 100 - self.online_time()

    def offline_str(self):
        if not self.down_time:
            return "0 seconds"
        return self._seconds_to_str(self.down_time)

    def _seconds_to_str(self, secs):
        days = secs // 86400
        hours = (secs - days * 86400) // 3600
        minutes = (secs - days * 86400 - hours * 3600) // 60
        seconds = secs - days * 86400 - hours * 3600 - minutes * 60
        result = ""

        if days:
            result += "{0} day{1}, ".format(days, "s" * (days != 1))
        if hours:
            result += "{0} hour{1}, ".format(hours, "s" * (hours != 1))
        if minutes:
            result += "{0} minute{1}, ".format(minutes, "s" * (minutes != 1))
        if seconds:
            result += "{0} second{1}, ".format(seconds, "s" * (seconds != 1))
        return result[:-2]


class Service(models.Model):
    name = models.CharField(max_length=64, blank=False)
    node = models.ForeignKey(Node, blank=True, null=True)
    status = models.CharField(
        max_length=10, blank=False, default=STATUS_UNK, choices=STATUS_CHOICES)
    activation_mode = models.CharField(
        max_length=10, blank=False, default=ACT_MODE_MAN, choices=ACT_CHOICES)

    def get_status(self):
        if self.node and self.node.status == STATUS_OFF:
            return STATUS_OFF
        return self.status


class Sensor(models.Model):
    name = models.CharField(max_length=64, blank=True)
    source = models.CharField(max_length=32, blank=True)
    status = models.CharField(
        max_length=10, blank=False, default=STATUS_UNK,
        choices=SENSOR_STATUS_CHOICES)
    topic = models.CharField(max_length=64)
    interface = models.CharField(max_length=128, blank=True)
    event_count = models.IntegerField(default=0)
    last_events = models.TextField(blank=True)

    def get_status(self):
        if self.status == STATUS_ON:
            return STATUS_ACT
        if self.status == STATUS_OFF:
            return STATUS_INA
        return self.status


class EventCount(models.Model):
    date = models.DateField(default=date.today)
    start = models.DateTimeField(default=now)
    last_time_mark = models.DateTimeField(default=now)
    events = models.TextField(blank=True)

    def add_to_count(self, c):
        # each record on 'events' is the count for a period of time
        events = list(map(int, self.events.split(","))) if self.events else []
        delta = timedelta(seconds=EVENTS_DELTA)
        elapsed = now() - (self.last_time_mark or 0)

        # add zeroes of passed events
        if elapsed > delta:
            new_last_time_mark = self.last_time_mark
            for i in range(elapsed // delta):
                events.append(0)
                new_last_time_mark += delta
            self.last_time_mark = new_last_time_mark

        # update last record and store it
        if not events:
            events.append(0)
        events[-1] += c
        self.events = ",".join(map(str, events))


class EventFilter:
    def __init__(self, filter):
        self.filter = filter
        self.interval = EVENTS_DELTA
        self.start = now()
        self.data = ""

        try:
            if filter == "day":
                self._get_last_day()
            elif filter == "week":
                self._get_days(7, 7)
            elif filter == "month":
                self._get_days(30, 30)
            elif filter == "all":
                self._get_all()
        except EventCount.DoesNotExist:
            pass

    def _get_last_day(self):
        # retrieve last day record
        counter = EventCount.objects.latest('date')
        self.start = counter.start
        events = counter.events.split(",") if counter.events else []

        # if there are few elements, we would need the previous day
        if len(events) < EVENTS_PER_DAY:
            try:
                prev_day = counter.date - timedelta(days=1)
                previous = EventCount.objects.get(date=prev_day)
                p_events = previous.events.split(",") \
                    if previous.events else []
                need_events = EVENTS_PER_DAY - len(events)
                take_events = min(len(p_events), need_events)
                self.start -= timedelta(seconds=take_events*EVENTS_DELTA)
                events = p_events[-take_events:] + events
            except EventCount.DoesNotExist:
                # if there are no previous day, then nothing can be done
                pass

        # stringfy all events and return
        self.data = ",".join(events)

    def _get_days(self, num_days, reduce=0):
        # retrieve up to 'num_days' days
        counters = [EventCount.objects.latest('date')]
        day = counters[0].start
        for i in range(num_days - 1):
            try:
                day -= timedelta(days=1)
                counters.insert(0, EventCount.objects.get(date=day))
            except EventCount.DoesNotExist:
                break

        # get starting time of first day and all events
        self.start = counters[0].start
        events = []
        for c in counters:
            events.extend(c.events.split(",") if c.events else [])

        # reduce the number of points: just add them together
        if len(events) > EVENTS_PER_DAY * 4 and reduce > 1:
            result = []
            c = 0
            for i in range(len(events)):
                if i and i % reduce == 0:
                    result.append(str(c))
                    c = 0
                c += int(events[i])
            result.append(str(c))
            events = result

            # adjust the event interval
            self.interval = EVENTS_DELTA * reduce

        # stringfy all events and return
        self.data = ",".join(events)

    def _get_all(self):
        days = EventCount.objects.count()
        total_evts = EVENTS_PER_DAY * days
        goal_evts = EVENTS_PER_DAY
        reduce = total_evts // goal_evts
        return self._get_days(days, reduce)
