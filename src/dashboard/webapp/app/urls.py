# -*- mode: python; coding: utf-8 -*-

from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^service/(?P<service_id>\d+)/remove/$',
        views.remove_service, name='service-remove'),
    url(r'^node/(?P<node_id>\d+)/remove/$',
        views.remove_node, name='node-remove'),
    url(r'^sensor/(?P<sensor_id>\d+)/remove/$',
        views.remove_sensor, name='sensor-remove'),
]
