Overview
========

Services' dashboard for Citisim project.

* `icegrid-dc`: IceGrid Data Collector, retrieves information about
  services and nodes from an IceGrid registry.

* `icestorm-dc`: IceStorm Data Collector, retrieves data about events
  and sensors from an IceStorm instance.

* `webapp`: Web Application to show the dashboard.

* `webapp-ds`: Web Application Data Sink, listens for incoming
  invocations from data collectors, and updates the `webapp`
  databases.


Installation
============

Use the Debian package, available on [pike's](http://pike.esi.uclm.es)
repository:

```sh
$ apt install citisim-dashboard
```

Running
=======

Once deployed, you can open the dashboard url, at
[http://127.0.0.1:9000](http://127.0.0.1:9000).
