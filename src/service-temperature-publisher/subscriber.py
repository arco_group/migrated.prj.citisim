#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import os
import time
import Ice
import IceStorm

pwd = os.path.abspath(os.path.dirname(__file__))
slice_path = os.path.join(pwd, "slice/iot.ice")
if not os.path.exists(slice_path):
    slice_path = "/usr/share/slice/citisim/iot.ice"

Ice.loadSlice(slice_path)
import SmartObject  # noqa
from SmartObject import MetadataField as MF  # noqa


class AnalogSinkI(SmartObject.AnalogSink):
    def notify(self, value, source, meta, current):
        human_ts = int(meta[MF.Timestamp])
        human_ts = time.strftime('%Y/%m/%d %H:%M:%S', time.localtime(human_ts))

        print(
            "new notify: \n" +
            "- temp: {} ºC\n".format(round(value, 2)) +
            "- from: '{}'\n".format(source) +
            "- located: \n" +
            "  - latitude: {}\n".format(meta[MF.Latitude]) +
            "  - longitude: {}\n".format(meta[MF.Longitude]) +
            "  - altitude: {}\n".format(meta[MF.Altitude]) +
            "  - place: '{}'\n".format(meta[MF.Place]) +
            "- meta: \n" +
            "  - timestamp: {} ({})\n".format(meta[MF.Timestamp], human_ts) +
            "  - quality: {}\n".format(meta[MF.Quality]) +
            "  - expiration: {}\n".format(meta[MF.Expiration]) +
            "---\n"
        )


class Subscriber(Ice.Application):
    def run(self, args):
        servant_prx = self.register_servant()
        print("Subscribed proxy: '{}'".format(servant_prx))
        self.subscribe_to_topic(servant_prx, topic="Temperature")

        print("Ready, waiting events...")
        self.shutdownOnInterrupt()
        self.communicator().waitForShutdown()

    def register_servant(self):
        ic = self.communicator()
        adapter = ic.createObjectAdapter("Adapter")
        adapter.activate()
        return adapter.addWithUUID(AnalogSinkI()).ice_oneway()

    def subscribe_to_topic(self, proxy, topic):
        topic = self.get_topic(topic)
        topic.subscribeAndGetPublisher({}, proxy)

    def get_topic(self, topic_name):
        ic = self.communicator()
        mgr = ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        mgr = IceStorm.TopicManagerPrx.checkedCast(mgr)

        try:
            return mgr.retrieve(topic_name)
        except IceStorm.NoSuchTopic:
            return mgr.create(topic_name)


if __name__ == "__main__":
    Subscriber().main(sys.argv)
