#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import requests
import Ice
from time import sleep
from libcitisim import Broker


class OpenWeatherSensor:
    def __init__(self, apiid, lat, lon):
        self.query = (
            "http://api.openweathermap.org/data/2.5/weather?" +
            "APPID={}&lat={}&lon={}&units=metric".format(apiid, lat, lon)
        )

    def update(self):
        response = requests.get(self.query)
        data = response.json()
        if data["cod"] != 200:
            print("ERROR: {}".format(data['message']))
            return False

        self.lat = data["coord"]["lat"]
        self.lon = data["coord"]["lon"]
        self.dt = data["dt"]
        self.temperature = data["main"]["temp"]

        return True


class Publisher(Ice.Application):
    def run(self, args):
        ic = self.communicator()        
        self.broker = Broker(ic=ic) 

        period = self.get_property("Measure.Periodicity", 300)
        apikey = self.get_property("Service.API.Key")
        sensor_desc = self.get_property("Sensor.Description")
        if sensor_desc is None:
            print("ERROR: property 'Sensor.Description' is mandatory!")
            return -1

        source, place, lat, lon = map(str.strip, sensor_desc.split(";"))
        sensor = OpenWeatherSensor(apikey, lat, lon)
        pub = self.broker.get_publisher(
            source = source,
            transducer_type = "TemperatureSensor"
        )

        print("Ready, starting event loop...")
        while True:
            sleep(period)
            if not sensor.update():
                continue

            meta = {        
                "latitude": sensor.lat,
                "longitude": sensor.lon,
                "place": place,
                "timestamp": sensor.dt,
                "quality": '0',
                "expiration": str(period),
            }

            temp = float(sensor.temperature)
            pub.publish(temp, meta)
            print("- publish event, {}° C".format(temp))

    def get_property(self, key, default=None):
        retval = self.broker.props.getProperty(key)
        if retval is "":
            print("Warning: property '{}' not set!".format(key))
            if default is not None:
                print(" - using default value: {}".format(default))
            return default
        if default is None:
            return retval
        return default.__class__(retval)


if __name__ == "__main__":
    Publisher(1).main(sys.argv)
