Overview
========

This folder contains the Persistence Service, which is a service to
receive and store events from IceStorm topics, in order to be able to
treat them later. It listens on a list of topics (given by its names),
and uses a common database to store the event itself and some more
meta-information related to the event (like the receive timestamp).

This service subscribers will only use Citisim interfaces.


Installation
============

This service is provided as a Debian package, so to install it, you
just need to run the following command:

    $ sudo apt install citisim-persistence-service


Setup
=====

Either if you use IceGrid to launch the service, or you launch it
manually, you will need to provide the following configuration
properties:

* `IceStorm.TopicManager.Proxy`: this is the proxy of the IceStorm
  service that you will use to receive events.

* `PersistenceService.Topics`: list of topic names and its interfaces,
  separated by commas, on which the service will be subscribed. The
  format of each tuple is `topic_name:module.interface`.

* `PersistenceService.Adapter.Endpoints`: the endpoints where service
  subscribers will be listening.


If you want to launch it manually, just put these properties in a
config file (i.e. `persistence-service.config`) and call it, like
this:

    $ persistence-service --Ice.Config=persistence-service.config
    
    
Deployed instance
=================

Credentials:

* User: admin
* Password: r6QokAcDiDtU 
* Old? password: AdRSy3zv

