# -*- mode: python; coding: utf-8 -*-

from django.contrib import admin
from django.shortcuts import render
from event import models


class AdminSite(admin.AdminSite):
    site_header = "Citisim Persistence Service"
    site_title = "Persistence Service"
    site_url = None
    index_title = "Event types"


site = AdminSite(name='admin')


class InfoFieldsMixin:
    def timestamp(self, o):
        return o.info.timestamp

    def topic(self, o):
        return o.info.topic

    def source(self, o):
        return o.info.source.name

    def interface(self, o):
        return o.info.interface_name

    def metadata(self, o):
        return o.info.metadata


@admin.register(models.DigitalEvent, site=site)
class DigitalEventAdmin(admin.ModelAdmin, InfoFieldsMixin):
    list_display = ('timestamp', 'topic', 'source', 'interface', 'value', 'metadata')
    list_filter = ('info__topic', 'info__source__name', 'info__interface_name')
    list_per_page = 20


@admin.register(models.AnalogEvent, site=site)
class AnalogEventAdmin(admin.ModelAdmin, InfoFieldsMixin):
    list_display = ('timestamp', 'topic', 'source', 'interface', 'value', 'metadata')
    list_filter = ('info__topic', 'info__source__name', 'info__interface_name')
    list_per_page = 20
    actions = ['plot_events']

    def _t(self, ts):
        return ts.strftime('%Y-%m-%dT%H:%M:%S')

    def plot_events(self, request, queryset):
        context = {
            'title': "Plot events",
            'data': [(self._t(x.info.timestamp), x.value) for x in queryset]
        }
        return render(request, "plots.html", context)

    plot_events.short_description = 'Show events plot'


@admin.register(models.PulseEvent, site=site)
class PulseEventAdmin(admin.ModelAdmin, InfoFieldsMixin):
    list_display = ('timestamp', 'topic', 'source', 'interface', 'metadata')
    list_filter = ('info__topic', 'info__source__name', 'info__interface_name')
    list_per_page = 20


@admin.register(models.DataEvent, site=site)
class DataEventAdmin(admin.ModelAdmin, InfoFieldsMixin):
    list_display = ('timestamp', 'topic', 'source', 'interface', 'value', 'metadata')
    list_filter = ('info__topic', 'info__source__name', 'info__interface_name')
    list_per_page = 20
