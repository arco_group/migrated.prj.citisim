# -*- mode: python; coding: utf-8 -*-

from django.db import models


DATA_TYPES = (
    ("FLOAT", "Float 64bits"),
    ("BOOL", "Boolean 8bits"),
    ("BINARY", "Binary data"),
)


class Source(models.Model):
    name = models.CharField(max_length=64, unique=True)


class EventInfo(models.Model):
    topic = models.CharField(max_length=64)
    timestamp = models.DateTimeField(auto_now_add=True)
    interface_name = models.CharField(max_length=256)

    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    data_type = models.CharField(max_length=6, choices=DATA_TYPES)
    metadata = models.TextField()


class AnalogEvent(models.Model):
    info = models.ForeignKey(EventInfo, on_delete=models.CASCADE)
    value = models.FloatField()


class DigitalEvent(models.Model):
    info = models.ForeignKey(EventInfo, on_delete=models.CASCADE)
    value = models.BooleanField()


class PulseEvent(models.Model):
    info = models.ForeignKey(EventInfo, on_delete=models.CASCADE)


class DataEvent(models.Model):
    info = models.ForeignKey(EventInfo, on_delete=models.CASCADE)
    value = models.BinaryField()
