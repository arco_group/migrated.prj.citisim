#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceStorm
import json
from libcitisim import SmartObject
from orm import models


class ServantMixin:
    def __init__(self, topic_name):
        self._topic = topic_name

    def _create_info(self, source, iface, type_, meta):
        try:
            meta = {str(k): v for k, v in meta.items()}
            meta = json.dumps(meta)
        except Exception as e:
            print("WARN: There were an error on meta information, discarded")
            print(meta)
            print(e)
            meta = "{}"

        source = models.Source.objects.get_or_create(name=source)[0]
        info = models.EventInfo(
            topic = self._topic,
            interface_name = iface,
            source = source,
            data_type = type_,
            metadata = meta,
        )
        info.save()
        return info


class DigitalSinkI(SmartObject.DigitalSink, ServantMixin):
    def __init__(self, *args):
        ServantMixin.__init__(self, *args)

    def notify(self, value, source, meta, current):
        print("DigitalSink.notify() called, value: {}, source: '{}'".format(value, source))

        info = self._create_info(source, "SmartObject.DigitalSink", "BOOL", meta)
        event = models.DigitalEvent(info=info, value=value)
        event.save()


class AnalogSinkI(SmartObject.AnalogSink, ServantMixin):
    def __init__(self, *args):
        ServantMixin.__init__(self, *args)

    def notify(self, value, source, meta, current):
        print("AnalogSink.notify() called, value: {}, source: '{}'".format(value, source))

        info = self._create_info(source, "SmartObject.AnalogSink", "FLOAT", meta)
        event = models.AnalogEvent(info=info, value=value)
        event.save()


class PersistenceService(Ice.Application):
    def run(self, args):
        try:
            return self._run_service(args)
        except AssertionError as e:
            print("ERROR: {}".format(e))
            return -1

    def _run_service(self, args):
        ic = self.communicator()

        # create object adapter
        adapter = ic.createObjectAdapter("PersistenceService.Adapter")
        adapter.activate()

        # get topics from properties
        topic_list = self._get_topic_list(ic)

        # for each tuple:
        for name, type_ in topic_list.items():
            # - create servant of given type and add to adapter
            servant = self._new_servant(type_, name)
            proxy = adapter.addWithUUID(servant)

            # - retrieve topic
            topic = self._get_topic(ic, name)

            # - subscribe servant to topic
            print("- subscribing {} servant to topic '{}'".format(type_, name))
            topic.subscribeAndGetPublisher({}, proxy.ice_oneway())

        # wait for events
        print("Ok, waiting for events...")
        self.shutdownOnInterrupt()
        ic.waitForShutdown()

    def _get_topic_list(self, ic):
        props = ic.getProperties()
        topic_list = props.getProperty('PersistenceService.Topics')
        assert topic_list, "You must specify a list of topics on configuration file."

        try:
            items = topic_list.split(",")
            return dict(map(lambda i: i.split(":"), items))
        except ValueError:
            raise AssertionError("Invalid format of Topics property.")

    def _new_servant(self, type_, topic_name):
        try:
            return {
                'SmartObject.DigitalSink': DigitalSinkI,
                'SmartObject.AnalogSink': AnalogSinkI,
            }[type_](topic_name)
        except KeyError as e:
            raise AssertionError(
                "Unknown interface ({}) defined on Topics property.".format(e))

    def _get_topic(self, ic, name):
        manager = self._get_topic_manager(ic)
        try:
            return manager.create(name)
        except IceStorm.TopicExists:
            return manager.retrieve(name)

    def _get_topic_manager(self, ic):
        try:
            return self._topic_manager
        except AttributeError:
            pass

        manager = ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        assert manager, "Property IceStorm.TopicManager.Proxy not defined."
        self._topic_manager = IceStorm.TopicManagerPrx.checkedCast(manager)
        print("- using IceStorm: '{}'".format(self._topic_manager))
        return self._topic_manager


if __name__ == '__main__':
    exit(PersistenceService().main(sys.argv))
