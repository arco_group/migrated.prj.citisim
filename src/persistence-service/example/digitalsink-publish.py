#!/usr/bin/env python3
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
import IceStorm
from libcitisim import SmartObject


class DigitalSinkPublish(Ice.Application):
    def run(self, args):
        if len(args) != 3:
            print("Usage: {} <topic> <1|0>".format(args[0]))
            return 1

        ic = self.communicator()
        topic = self._get_topic(ic, args[1])
        publisher = SmartObject.DigitalSinkPrx.uncheckedCast(topic.getPublisher())

        state = args[2] != "0"
        publisher.notify(state, "DummyClient", {})

    def _get_topic(self, ic, name):
        manager = self._get_topic_manager(ic)
        try:
            return manager.create(name)
        except IceStorm.TopicExists:
            return manager.retrieve(name)

    def _get_topic_manager(self, ic):
        try:
            return self._topic_manager
        except AttributeError:
            pass

        manager = ic.propertyToProxy("IceStorm.TopicManager.Proxy")
        assert manager, "Property IceStorm.TopicManager.Proxy not defined."
        self._topic_manager = IceStorm.TopicManagerPrx.checkedCast(manager)
        return self._topic_manager


if __name__ == '__main__':
    exit(DigitalSinkPublish().main(sys.argv))
