
The goal of this package it to tests the performance of an IceStorm instance. It will:

* Run a publisher sending events in a increasing-rate fashion and get the CPU/Memory factors from the VM

* Run an increasing number of publishers (which publish in a fixed rate) and get CPU/Memory factors from the VM


