#!/usr/bin/python3

import sys
from libcitisim import Broker


class Observer:
    def __init__(self):
        broker = Broker("observer.config")
        broker.subscribe("BinStatus", self.on_binStatus)

        if len(sys.argv) > 1:
            broker.subscribe_to_pu(sys.argv[1], self.on_binStatus)

        print("Ready, waiting events...")
        broker.wait_for_events()

    def on_binStatus(self, *args, **kwargs):
        print("on binStatus event")

    def on_specific(self, *args, **kwargs):
        print("on specific event")


if __name__ == "__main__":
    Observer()