// -*- mode: c++; coding: utf-8 -*-

#include <iot/node.ice>
#include <citisim/iot.ice>

module IoT {
    interface IoTNode extends 
        IoT::NodeAdmin,  
        IoT::WiFiAdmin, 
        SmartObject::Observable, 
        SmartObject::AnalogSink {
    };
};
