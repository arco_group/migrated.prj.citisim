#include <Arduino.h>
#include <Ticker.h>

#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h> 
#include <EEPROM.h>  

#include <IceC/IceC.h>
#include <IceC/Locator.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

#include "iot-node.h"

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;

// servants
IoT_IoTNode node;

// proxies
Ice_ObjectPrx publisher;

// pinout
#define TRIGGER 9
#define ECHO 10
#define ENABLE 13
#define UPDATE_TIME 10 * 60 * 1000000 // update time in us
#define MEASUREMENTS_NUMBER 10

// variables to save measures
int distance = 0;
int measures_number;
int mean_distance;

void IoT_NodeAdminI_restart(IoT_NodeAdminPtr self)
{
  async_restart_node();
}

void IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self)
{
  async_factory_reset();
}

void IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self,
                              Ice_String ssid,
                              Ice_String key)
{
  store_wifi_settings(ssid, key);
}

void SmartObject_ObservableI_setObserver(SmartObject_ObservablePtr self, Ice_String Observer)
{
  char observer_prx[Observer.size];
  strncpy(observer_prx, Observer.value, Observer.size);
  Ice_Communicator_stringToProxy(&ic, observer_prx, &publisher);    
}

int read_distance()
{
  unsigned long duration;
  int distance;

  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);

  duration = pulseIn(ECHO, HIGH);

  distance = duration * 0.0343 / 2;
  Serial.printf("Measured distance %d\n", distance);

  return distance;
}

void send_reading(int value_readed)
{
  SmartObject_Metadata meta;
  SmartObject_Metadata_init(meta, NULL, 0);

  new_Ice_String(source, STR(TRANSDUCER_ADDR));
  SmartObject_AnalogSink_notify(&publisher, value_readed, source, meta);
  Serial.printf("Sensor: %s - Value: %d\n", STR(TRANSDUCER_ADDR), value_readed);
}

void setup()
{
  const char zero = {0};
  Serial.begin(9600);
  Serial.flush();

  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);
  pinMode(ENABLE, OUTPUT);

  digitalWrite(TRIGGER, LOW);
  digitalWrite(ECHO, LOW);
  digitalWrite(ENABLE, HIGH);

  delay(2000);

  Serial.println("Booting.....");

  delay(1000);

  IceC_Storage_begin();
  setup_wireless();
  setup_ota();

  Ice_initialize(&ic);
  TCPEndpoint_init(&ic);

  // Set Citisim locator
  Ice_Communicator_setDefaultLocator(&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");

  // setup adapter and register servants
  Ice_Communicator_createObjectAdapterWithEndpoints(&ic, "Adapter", "tcp -p 4455", &adapter);

  Ice_ObjectAdapter_activate(&adapter);
  IoT_IoTNode_init(&node);

  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, STR(TRANSDUCER_ADDR));


  if (is_online)
  {
    Ice_ObjectPrx welcome;
    Ice_Communicator_stringToProxy(&ic, "WelcomeServer -o @ WelcomeServiceAdapter", &welcome);
    String proxy = STR(TRANSDUCER_ADDR) " -o -e 1.0:tcp -h " + get_local_ip() + " -p 4455";
    new_Ice_String(address, proxy.c_str());
    new_Ice_String(type, STR(TRANSDUCER_TYPE));
    SmartObject_WelcomeService_hello(&welcome, address, type);
  }

  Serial.println("----- BOOT DONE -----");

  led_blink(5, 50);    
  digitalWrite(LED_BUILTIN, LOW);
}

void loop()
{  
  Ice_Communicator_loopIteration(&ic);
  handle_ota();
  check_buttons();

  if(is_online) {

    for(int i = 0; i < MEASUREMENTS_NUMBER; i++) 
      distance += read_distance();
  
    mean_distance = distance / MEASUREMENTS_NUMBER;
    send_reading(mean_distance);
  
    ESP.deepSleep(UPDATE_TIME);

  }
}