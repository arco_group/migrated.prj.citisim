#!/usr/bin/python3

import sys
import Ice
import logging
from pprint import pprint
from argparse import ArgumentParser
from libcitisim import SmartServices

logging.getLogger().setLevel(logging.INFO)


class LocationClient(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return -1

        ic = self.communicator()
        if self.args.proxy:
            location = ic.stringToProxy(self.args.proxy)
            if location is None:
                logging.error("Invalid location proxy!")
                return -1

        else:
            key = "LocationService.Proxy"
            location = ic.propertyToProxy(key)
            if location is None:
                logging.warning("Missing property: {}, and no --proxy given!".format(key))
                logging.warning("Using default: 'LocationService'")
                location = ic.stringToProxy("LocationService")

        self.location = SmartServices.LocationServicePrx.checkedCast(location)
        self.run_command()

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("-p", "--place", required=True,
            help="PlaceID for filtering transducers")
        parser.add_argument("--proxy", type=str,
            help="Proxy of LocationService (use this instead property)")
        action = parser.add_mutually_exclusive_group(required=True)
        action.add_argument("--list", action="store_true",
            help="List all transducers of given PLACE")
        action.add_argument("--list-type", type=str, dest="type",
            help="List transducers of a specific TYPE")
        action.add_argument("--set-mp", type=str,
            help="Set the meeting point (MP) for given PLACE")
        action.add_argument("--get-mp", action="store_true",
            help="Get the meeting point of given PLACE")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True

    def run_command(self):
        if self.args.list:
            pprint(self.location.listTransducers(self.args.place))

        elif self.args.type:
            pprint(self.location.listTransducersByType(
                self.args.place, self.args.type))

        elif self.args.get_mp:
            mp = self.location.getMeetingPointForPlace(self.args.place)
            if not mp:
                print("Meeting point for '{}' not defined!".format(self.args.place))
            else:
                print("Meeting point for '{}': {}".format(self.args.place, mp))

        elif self.args.set_mp:
            self.location.setMeetingPointForPlace(self.args.place, self.args.set_mp)
            print("Meeting point set.")

        else:
            logging.error("Invalid operation!")


if __name__ == "__main__":
    LocationClient().main(sys.argv)
