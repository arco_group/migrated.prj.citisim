#!/usr/bin/python3

import sys
import Ice
import logging
from libcitisim import Broker, SmartServices, PropertyService

logging.getLogger().setLevel(logging.INFO)


class LocationServiceI(SmartServices.LocationService):
    def __init__(self, broker):
        self.broker = broker

    def listTransducers(self, placeID, current=None):
        placeID = placeID.lower()
        all_props = self.broker.get_property("")
        result = []

        # 'place' may be a single string or a list of places, which
        # are from bigger areas to smaller ones
        for tid, props in all_props.items():
            place = props.get("place")
            if place is None:
                continue
            elif isinstance(place, str):
                if place.lower() == placeID:
                    result.append(tid)
                    continue
            elif isinstance(place, list):
                for p in place:
                    if p.lower() == placeID:
                        result.append(tid)
                        break

        self.cache = all_props
        return result

    def listTransducersByType(self, placeID, transducerType, current):
        transducerType = transducerType.lower()
        tids = self.listTransducers(placeID)
        result = []

        for tid in tids:
            props = self.cache[tid]
            if props.get("transducer-type", "").lower() == transducerType:
                result.append(tid)

        return result

    def setMeetingPointForPlace(self, placeID, mpName, current):
        self.broker.set_property("meeting-points | " + placeID, mpName)

    def getMeetingPointForPlace(self, placeID, current):
        try:
            return self.broker.get_property("meeting-points | " + placeID)
        except PropertyService.PropertyException:
            return None


class LocationService(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        broker = Broker(ic=ic)
        servant = LocationServiceI(broker)
        prx = broker.adapter.add(servant, Ice.stringToIdentity("LocationService"))

        logging.info("LocationService ready, prx: '{}'".format(prx))
        broker.wait_for_events()


if __name__ == "__main__":
    LocationService().main(sys.argv)
