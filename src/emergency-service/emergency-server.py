#!/usr/bin/python3

import sys
import Ice
import logging
from libcitisim import Broker, SmartEnvironment, SmartServices, SmartObject

logging.getLogger().setLevel(logging.INFO)


class Planner:
    def __init__(self, broker, services):
        self.broker = broker
        self.__dict__.update(services)

    def onEvent(self, name, source, meta):
        logging.info("- event '{}' from '{}'".format(name, source))

        try:
            if name.lower() == "earthquake":
                self.handle_earthquake()
            elif name.lower() == "gasleak":
                self.handle_gas_leak()
            else:
               logging.warning("  unknown event, nothing done!")
        except Exception as e:
            logging.warning("ERROR on handling event!")
            logging.warning(str(e))

    def handle_earthquake(self):
        logging.info("  retrieving occupied areas...")
        places = self.occupancy.getOccupiedPlaces()

        if len(places) == 0:
            logging.info("  all monitored areas are empty, nothing to do")
            return

        for p in places:
            logging.info("  occuppied place: {}, getting evacuation point...".format(p))
            ev_point = self.location.getMeetingPointForPlace(p)
            if not ev_point:
                logging.warning(">>> NO EVACUATION POINT DEFINED FOR {}!!".format(p))
                continue

            logging.info("  EP for {} is {}, getting evacuation route...".format(
                p, ev_point))

            route = self.routing.getPath(p, ev_point)
            if not route:
                logging.warning("  can not obtain EV route!")
                continue

            logging.info("  EV route obtained from {} to {}".format(p, ev_point))
            logging.info("  publishing EV route...")
            self.route_publisher.publish(route)

    def handle_gas_leak(self):
        # FIXME: get placeID dynamically
        logging.info("  getting affected actuators...")
        gas_valves = self.location.listTransducersByType("ITSI", "ValveActuator")
        electrical_switches = self.location.listTransducersByType("ITSI", "SwitchActuator")

        if not gas_valves and not electrical_switches:
            logging.info("  no devices found, nothing to do.")
            return

        if gas_valves:
            logging.info("  gas valve(s) available, closing...")
            for devid in gas_valves:
                device = self.get_device(devid)
                logging.info("  - id: {}, dev: {}".format(devid, device))
                device.notify(False, "EmergencyService", {})
                logging.info("  - id: {}, done".format(devid))

        if electrical_switches:
            logging.info("  electrical switch available, closing it")
            for devid in electrical_switches:
                device = self.get_device(devid)
                device.notify(False, "EmergencyService", {})

    def get_device(self, devid):
        prx = self.broker.get_property("{} | proxy".format(devid))
        prx = self.broker._ic.stringToProxy(prx).ice_timeout(10000)
        return SmartObject.DigitalSinkPrx.uncheckedCast(prx)


class EmergencyService(Ice.Application):
    def run(self, args):
        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)
        self.services = self.get_services()

        planner = Planner(self.broker, self.services)
        self.broker.subscribe("Alarm", planner.onEvent)

        logging.info("System ready, waiting events...")
        self.broker.wait_for_events()

    def get_services(self):
        services = {}
        prx = self.get_property_proxy("OccupancyMonitor.Proxy")
        services["occupancy"] = \
            SmartEnvironment.OccupancyMonitorPrx.uncheckedCast(prx)
        prx = self.get_property_proxy("LocationService.Proxy")
        services["location"] = \
            SmartServices.LocationServicePrx.uncheckedCast(prx)
        prx = self.get_property_proxy("RoutingService.Proxy")
        services["routing"] = \
            SmartServices.RoutingServicePrx.uncheckedCast(prx)

        sourceid = self.get_property("EmergencyService.SourceID")
        services["route_publisher"] = self.broker.get_publisher(
            sourceid, "RouteService"
        )
        return services

    def get_property_proxy(self, key, required=True):
        prx = self.get_property(key, required)
        return self.ic.stringToProxy(prx)

    def get_property(self, key, required=True, default=None):
        props = self.ic.getProperties()
        value = props.getProperty(key)
        if required and not value:
            logging.warning(" missing property, please provide the '{}'!".format(key))
        return value


if __name__ == "__main__":
    EmergencyService(1).main(sys.argv)
