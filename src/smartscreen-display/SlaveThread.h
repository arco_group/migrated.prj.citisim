#include <QThread>
#include <QMutex>
#include <QSerialPort>

class SlaveThread : public QThread
{
    Q_OBJECT

public:
    explicit SlaveThread(QObject *parent = nullptr);
    ~SlaveThread();

    void startSlave(const QString &portName, int waitTimeout);
    void send(const QByteArray &s);

signals:
    void request(const QString &s);
    void error(const QString &s);
    void timeout(const QString &s);
    void data(const QByteArray &s);
	
private:
    void run() override;

    QSerialPort serial;
    QString m_portName;
    int m_waitTimeout = 0;
    QMutex m_mutex;
    bool m_quit = false;
};
