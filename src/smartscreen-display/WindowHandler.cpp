#include "WindowHandler.h"

#include <QGraphicsPixmapItem>

#include <iostream>
using namespace std;

WindowHandler::WindowHandler() {
	w.setWindowFlags(Qt::Window | Qt::FramelessWindowHint);
	  
	view = new QGraphicsView();
	view->setFrameStyle(QFrame::NoFrame);
	view->setScene(&scene);
	w.setCentralWidget(view);

	pictogram[0] = QPixmap("figs/left.png");
	pictogram[1] = QPixmap("figs/right.png");
	pictogram[2] = QPixmap("figs/down.png");
    pictogram[3] = QPixmap("figs/not.png");
    pictogram[4] = QPixmap("figs/citisim.png");

    for (int i = 0; i < 5; i++)
	  pixmap[i] = new QGraphicsPixmapItem(pictogram[i]);
    //	scene.addItem(pixmap[0]);
    current_pictogram = scene.addPixmap(pictogram[3]);
  }

void WindowHandler::show() {
	w.showFullScreen();	
    w.show();	
  }

void WindowHandler::showPictogram(unsigned char pictogram_id) {
  cout << "pictogram : " << pictogram_id  << endl;
  current_pictogram->setPixmap(pictogram[pictogram_id]);
}
