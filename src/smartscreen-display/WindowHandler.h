#ifndef __WINDOW_HANDLER__
#define __WINDOW_HANDLER__

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>

class WindowHandler {

public:
  WindowHandler();
  void show();
  void showPictogram(unsigned char pictogram_id);
  
private:
  QMainWindow w;
  QGraphicsScene scene;
  QGraphicsView *view;
  QPixmap pictogram[5];
  QGraphicsPixmapItem *pixmap[5];
  QGraphicsPixmapItem *current_pictogram;
};

#endif
