#include "SlaveThread.h"
#include "WindowHandler.h"

class CommController : public QThread
{
  Q_OBJECT

 public:
  explicit CommController(const QString &portName, int waitTimeout,
						  WindowHandler *w);
  
 private:
  //  void run() override;
  void handleData(const QByteArray &s);
  void FSM(char token);
  void runCommand(unsigned char command, unsigned char arg);
  
  SlaveThread m_thread;
  WindowHandler *w;
  unsigned char command;
  unsigned char currentPictogram;
};
