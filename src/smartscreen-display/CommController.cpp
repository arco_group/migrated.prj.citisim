#include "CommController.h"
#include <iostream>

/**
   Message format: 0x55 0xaa <cmd> <arg>
   where command can be any of:
           1 -> getStatus(). It returns the current state of the panel
		   2 -> setBackLight(value). Sets the dimming of the backlight in a range from 0 to 100
		   3 -> clearScreen()
		   4 -> setPictogramId(pictogramId), where the Id corresponds to:
		            0 -> left
					1 -> right
					2 -> down
					3 -> not
		   5 -> getPictogramId(). Returns the pictogram code
**/

#define IDLE_STATE     0
#define HEADER_1_STATE 1
#define CMD_STATE      2
#define ARG_STATE      3


#define SET_BACKLIGHT_CMD    0
#define CLEAR_SCREEN_CMD     1
#define SET_PICTOGRAM_CMD    2
#define GET_PICTOGRAM_ID_CMD 3
//#define GET_STATUS_CMD       

char SERIAL_MSG_REPLY[4] = {0x55, 0xaa, 0, 0xd};

using namespace std;

CommController::CommController(const QString &portName, int waitTimeout,
							   WindowHandler *w)  {
  this->w = w;
  m_thread.startSlave(portName, waitTimeout);
  connect(&m_thread, &SlaveThread::data, this, &CommController::handleData);  
  cout << "comm controller created" << endl;
}

void CommController::handleData(const QByteArray &s) {

  QByteArray::const_iterator it = s.begin();
  while (it != s.end()) {
	cout << "byte: " << hex << (int)*it << endl;
	this->FSM(*it);
	it++;
  }  
}

void CommController::FSM(char token) {
  static int state = 0;
  unsigned char t;
  static unsigned char command;

  t = (unsigned char)token;
  switch (state) {
  case IDLE_STATE:
    if (t == 0x55)
	  state = HEADER_1_STATE;
	break;

  case HEADER_1_STATE:
    if (t == 0xaa)
	  state = CMD_STATE;
	else
	  state = IDLE_STATE;
	break;

  case CMD_STATE:
	command = t;
	state = ARG_STATE;
	break;

  case ARG_STATE:
	runCommand(command, t);
	state = IDLE_STATE;
  }	
	
}

void CommController::runCommand(unsigned char command, unsigned char arg) {
  switch (command) {

	/* Resuelto en el Lopy?	
  case GET_STATUS_CMD:
	// send(1);
	break;
	*/

  case SET_BACKLIGHT_CMD:
	// TODO
	break;
	
  case CLEAR_SCREEN_CMD:
	// TODO
	break;

  case SET_PICTOGRAM_CMD:
    if ( arg < 5) {
	  w->showPictogram(arg);
	  currentPictogram = arg;
	}
	break;

  case GET_PICTOGRAM_ID_CMD:
    SERIAL_MSG_REPLY[2] = currentPictogram;
    m_thread.send(QByteArray(SERIAL_MSG_REPLY, 4));
	break;
  }
}
