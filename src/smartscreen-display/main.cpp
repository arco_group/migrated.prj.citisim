#include <QApplication>

#include "CommController.h"
#include "WindowHandler.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

        WindowHandler w;
        CommController *comm = new CommController("/dev/ttyUSB0", 10, &w);
        w.show();

    return app.exec();
}

