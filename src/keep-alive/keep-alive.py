#!/usr/bin/python3

import os
import sys
import Ice
import logging
from argparse import ArgumentParser
from time import sleep
from libcitisim import Broker, PropertyService

logging.basicConfig(level=logging.INFO)


class AliveKeeperService(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return 1

        self.ic = self.communicator()
        self.sourceids = {}
        self.broker = Broker(ic=self.ic)

        try:
            logging.info("sleeping time is: {}".format(self.args.sleep))
            while True:
                self.process_devices()
                sleep(self.args.sleep)
        except KeyboardInterrupt:
            pass

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--sleep", default=3, type=int,
            help="time between checks")

        try:
            self.args = parser.parse_args(args[1:])
        except SystemExit:
            return False
        return True

    def process_devices(self):
        self.update_settings()
        for sid in self.sourceids:
            devip = self.get_ip(sid)
            if devip is None:
                logging.warning("device '{}' is not available!".format(sid))
                continue

            self.do_ping(sid, devip)

    def do_ping(self, sid, ip):
        response = os.system("ping -t2 -c1 " + ip + " > /dev/null 2>&1")
        if response != 0:
            logging.warning("device '{}' (ip: '{}') does not answer!".format(
                sid, ip))

    def get_ip(self, sid):
        try:
            prx = self.broker.get_property(sid + " | proxy")
            return self.ic.stringToProxy(prx).ice_getEndpoints()[0].getInfo().host
        except (PropertyService.PropertyException, IndexError):
            return None

    def update_settings(self):
        try:
            key = "keep-alive|source-ids"
            self.sourceids = self.broker.get_property(key)
        except PropertyService.PropertyException:
            logging.warning(
                " no source IDs defined (no '{}' property in PS)!".format(key))


if __name__ == "__main__":
    AliveKeeperService(1).main(sys.argv)


