// -*- mode: c++; coding: utf-8 -*-

#include <iot/node.ice>
#include <citisim/iot.ice>

module Citisim {
    interface Valve extends
        IoT::NodeAdmin,
        IoT::WiFiAdmin,
        SmartObject::Observable,
        SmartObject::DigitalSink {
    };
};