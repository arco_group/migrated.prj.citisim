// -*- mode: cpp; coding: utf-8 -*-

#include <Arduino.h>
#include <ESP8266WiFi.h>

#include <IceC/IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

// NOTE: remember to run 'make' to generate this file
#include "valve-node.h"

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;

// servants
Citisim_Valve node;

bool current_state;

void
set_relay_state(bool state) {
  digitalWrite(STATUS_LED, state ? LOW : HIGH);
  digitalWrite(RELAY, state ? HIGH : LOW);
  current_state = state;
}

void setup() {
  Serial.begin(115200);
  Serial.flush();

  pinMode(STATUS_LED, OUTPUT);
  pinMode(RELAY, OUTPUT);

  delay(1000);
  Serial.println("\n------Booting------\n");

  // setup WiFi, Ice, Endpoints...
  IceC_Storage_begin();
  setup_wireless();
  // setup_ota();

  Ice_initialize(&ic);
  TCPEndpoint_init(&ic);

  // set Citisim locator
  Ice_Communicator_setDefaultLocator
    (&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");

  // setup adapter and register servants
  Ice_Communicator_createObjectAdapterWithEndpoints
    (&ic, "Adapter", "tcp -p 4455", &adapter);
  Ice_ObjectAdapter_activate(&adapter);
  Citisim_Valve_init(&node);

  // register twice to be able to connect it even if ADDR is unknown
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, STR(TRANSDUCER_ADDR));

  // register on Welcome Service
  if (is_online) {
    Ice_ObjectPrx welcome;
    Ice_Communicator_stringToProxy(&ic, "WelcomeServer -o @ WelcomeServiceAdapter", &welcome);
    String proxy = STR(TRANSDUCER_ADDR) " -o -e 1.0:tcp -h " + get_local_ip() + " -p 4455";
    new_Ice_String(address, proxy.c_str());
    new_Ice_String(type, STR(TRANSDUCER_TYPE))
    SmartObject_WelcomeService_hello(&welcome, address, type);
  }

  // everything is fine, say it to the world!
  Serial.println("\n------Boot done!------\n");
  led_blink(5, 50);
  set_relay_state(false);
}

void loop() {
  if (digitalRead(PIN_BUTTON) == 0)
    set_relay_state(!current_state);

  Ice_Communicator_loopIteration(&ic);
  // handle_ota();
  check_buttons();
}

void
SmartObject_DigitalSinkI_notify(SmartObject_DigitalSinkPtr self,
    Ice_Bool value, Ice_String source, SmartObject_Metadata meta) {
  set_relay_state(value);
}

// redirect callbacks (needed here to overwrite __weak__ defs)
void IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) { async_restart_node(); }
void IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) { async_factory_reset(); }
void IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self, Ice_String ssid, Ice_String key) {
    store_wifi_settings(ssid, key);
}