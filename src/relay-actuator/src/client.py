#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("../../../slice/iot.ice")
import SmartObject  # noqa


class Client(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        if len(args) != 3:
            print("Usage: {} <proxy> <0|1>".format(args[0]))
            return 1

        relay = ic.stringToProxy(args[1])
        relay = relay.ice_encodingVersion(Ice.Encoding_1_0)
        relay = SmartObject.DigitalSinkPrx.uncheckedCast(relay)

        value = args[2] != "0"
        relay.notify(value, "Client", {})


if __name__ == "__main__":
    Client().main(sys.argv)
