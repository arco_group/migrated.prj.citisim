#!/usr/bin/python3

import os
import sys
import Ice
import logging
from threading import Event
from argparse import ArgumentParser
from flask import Flask, request, Response
from libcitisim import Broker, SmartObject, PropertyService

logging.getLogger().setLevel(logging.INFO)


class FlaskApp(Flask):
    def __init__(self):
        super().__init__(__name__)
        self.ev = Event()
        self.name = ""

        @self.route('/')
        def index():
            self.ev.wait()
            return Response(self.name)

    def on_event(self, value, source, meta):
        logging.info("event {} received!".format(value))
        if value == "GasLeak":
            self.name = value
            self.ev.set()
            self.ev.clear()


# FIXME: it only supports one single source and of type DigitalSink
#        make it generic!

class HTTPCache(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)
        app = FlaskApp()

        key = "Source.Topic"
        topic_name = self.broker.props.getProperty(key)
        if not topic_name:
            logging.error("Please, provide the '{}' property".format(key))
            return 1

        self.broker.subscribe(topic_name, app.on_event)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--port", type=int, default=5002,
            help="port to bind web interface")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    HTTPCache(1).main(sys.argv)
