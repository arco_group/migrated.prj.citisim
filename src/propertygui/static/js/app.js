_ = console.info.bind(console);

$(document).ready(function () {
    load_props();
    window.setInterval(load_props, 3000);
});

var current = null;
function load_props() {
    $.getJSON( "/getprops", function(props) {
        var jsonprops = JSON.stringify(props);
        if (jsonprops === current)
            return;

        current = jsonprops;
        view = $('#content').jsonViewer(props, {
            rootCollapsable: false,
            collapsed: false,
        });
    });
}

function expand_toggle() {
    view.find('a.json-toggle').click();
}