#!/usr/bin/python3

import os
import sys
import Ice
import logging
from argparse import ArgumentParser
from flask import Flask, render_template, send_from_directory

Ice.loadSlice("/usr/share/slice/PropertyService/PropertyService.ice")
import PropertyService

logging.getLogger().setLevel(logging.INFO)


class FlaskApp(Flask):
    def __init__(self, ps):
        self.ps = ps
        pwd = os.path.abspath(os.path.dirname(__file__))
        static = os.path.join(pwd, "static")
        if not os.path.exists(static):
            pwd = "/usr/share/propertygui/"
            static = os.path.join(pwd, "static")
        super().__init__(__name__, template_folder=pwd, static_folder=static)

        @self.route('/')
        def index():
            return render_template("propertygui.html")

        @self.route('/getprops')
        def get_properties():
            if self.ps is None:
                return ("", 204)

            props = self.ps.get("")
            return (props, 202)

        @self.route('/static/<path:path>')
        def get_static_file(path):
            dirname = os.path.join("static", os.path.dirname(path))
            filename = os.path.basename(path)
            return send_from_directory(dirname, filename)


class PropertyGUI(Ice.Application):
    def run(self, args):
        if not self.parse_args(args[1:]):
            return 1

        if self.args.browser:
            self.open_browser()

        ps = None
        ic = self.communicator()

        if not self.args.debug:
            key = "PropertyServer.Proxy"
            ps = ic.propertyToProxy(key)
            if ps is None:
                logging.error(f"Missing configuration property: {key}")
                return -1

            ps = PropertyService.PropertyServerPrx.checkedCast(ps)

        app = FlaskApp(ps)
        app.debug = self.args.debug
        app.run(threaded=True, host="0.0.0.0", port=self.args.port)

    def open_browser(self):
        os.system(f"google-chrome --app=http://127.0.0.1:{self.args.port}")

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--debug", action="store_true",
            help="run flask in debug mode (disables Ice support)")
        parser.add_argument("--browser",  action="store_true",
            help="open url in a browser window")
        parser.add_argument("--port", type=int, default=4996,
            help="port to bind web interface")

        try:
            self.args = parser.parse_args(args)
        except SystemExit:
            return False
        return True


if __name__ == "__main__":
    PropertyGUI(1).main(sys.argv)
