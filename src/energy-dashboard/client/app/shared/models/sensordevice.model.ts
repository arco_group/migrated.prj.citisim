export class Sensordevice {
  _id?: string;
  sector?: string;
  citisimId?: string;
  name?: string;
  longitude?: number;
  latitude?: number;
  altitude?: number;
  description?: string;
}
