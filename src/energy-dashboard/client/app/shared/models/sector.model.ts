export class Sector {
  _id?: string;
  name?: string;
  longitude?: number;
  latitude?: number;
  altitude?: number;
  description?: string;
}
