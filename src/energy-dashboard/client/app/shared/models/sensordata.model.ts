export class Sensordata {
  _id?: string;
  type?: string;
  temperature?: number;
  voltage?: number;
  place?: string;
  soruce?: string;
  date?: string;
}
