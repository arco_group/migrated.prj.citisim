import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Sensordata } from '../shared/models/sensordata.model';

@Injectable()
export class SensordataService {

  constructor(private http: HttpClient) { }

  getSensordatas(): Observable<Sensordata[]> {
    return this.http.get<Sensordata[]>('/api/sensordatas');
  }

  countSensordatas(): Observable<number> {
    return this.http.get<number>('/api/sensordatas/count');
  }

  addSensordata(sensordata: Sensordata): Observable<Sensordata> {
    return this.http.post<Sensordata>('/api/sensordata', sensordata);
  }

  getSensordata(sensordata: String): Observable<Sensordata> {
    return this.http.get<Sensordata>(`/api/sensordata/${sensordata}`);
  }

  editSensordata(sensordata: Sensordata): Observable<any> {
    return this.http.put(`/api/sensordata/${sensordata._id}`, sensordata, { responseType: 'text' });
  }

  deleteSensordata(sensordata: Sensordata): Observable<any> {
    return this.http.delete(`/api/sensordata/${sensordata._id}`, { responseType: 'text' });
  }

  getSensordataswithfilter(init: Number, end: Number, type: String, source: String, interval: Number):
  Observable<Array<{date: string, value: number, source: string}>> {
    return this.http.
    get<Array<{date: string, value: number, source: string}>>(`/api/sensordatas/${init}/${end}/${type}/${source}/${interval}`);
  }

  getAVGPF(sources: String): Observable<Array<{_id: string, powerFactorAVG: number}>> {
    return this.http.get<Array<{_id: string, powerFactorAVG: number}>>(`/api/avgpowerfactor/${sources}`);
  }

  getAVGVoltage(sources: String): Observable<Array<{_id: string, voltageAVG: number}>> {
    return this.http.get<Array<{_id: string, voltageAVG: number}>>(`/api/avgvoltage/${sources}`);
  }

  getEnergyMonth(month: number): Observable<Array<{_id: string, count: number}>> {
    return this.http.get<Array<{_id: string, count: number}>>(`/api/energyMonth/${month}`);
  }

  getSensordatasFirstDate(): Observable<Sensordata> {
    return this.http.get<Sensordata>('/api/sensordatasfirstdate');
  }


  getSensordatasPredictiveValue(source: String, init: String, end: String): Observable<number> {
    return this.http.get<number>(`/api/predictivevalue/${source}/${init}/${end}/`);
  }

  getSensordatasPredictiveArray(source: String, init: String, end: String): Observable<any> {
    return this.http.get<any>(`/api/predictivedata/${source}/${init}/${end}/`);
  }

  /*router.route('/sensordatas/:init/:end/:type/:source/:interval').get(sensordataCtrl.getwithfilter);
  router.route('/avgvoltage/:sources').get(sensordataCtrl.getavgvoltage);
  router.route('/failvoltage/:sources').get(sensordataCtrl.getfailvoltage);*/
}
