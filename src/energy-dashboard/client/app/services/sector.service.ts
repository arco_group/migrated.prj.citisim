import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Sector } from '../shared/models/sector.model';

@Injectable()
export class SectorService {

  constructor(private http: HttpClient) { }

  getSectors(): Observable<Sector[]> {
    return this.http.get<Sector[]>('/api/sectors');
  }

  countSectors(): Observable<number> {
    return this.http.get<number>('/api/sectors/count');
  }

  addSector(sector: Sector): Observable<Sector> {
    return this.http.post<Sector>('/api/sector', sector);
  }

  getSector(sector: Sector): Observable<Sector> {
    return this.http.get<Sector>(`/api/sector/${sector._id}`);
  }

  editSector(sector: Sector): Observable<any> {
    return this.http.put(`/api/sector/${sector._id}`, sector, { responseType: 'text' });
  }

  deleteSector(sector: Sector): Observable<any> {
    return this.http.delete(`/api/sector/${sector._id}`, { responseType: 'text' });
  }

}
