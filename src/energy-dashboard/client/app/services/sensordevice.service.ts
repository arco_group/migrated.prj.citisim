import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Sensordevice } from '../shared/models/sensordevice.model';

@Injectable()
export class SensordeviceService {

  constructor(private http: HttpClient) { }

  getSensordevices(): Observable<Sensordevice[]> {
    return this.http.get<Sensordevice[]>('/api/sensordevices');
  }

  countSensordevices(): Observable<number> {
    return this.http.get<number>('/api/sensordevices/count');
  }

  addSensordevice(sensordevice: Sensordevice): Observable<Sensordevice> {
    return this.http.post<Sensordevice>('/api/sensordevice', sensordevice);
  }

  getSensordevice(sensordevice: Sensordevice): Observable<Sensordevice> {
    return this.http.get<Sensordevice>(`/api/sensordevice/${sensordevice._id}`);
  }

  getSensordevicesbysector(sector: String): Observable<Sensordevice[]> {
    return this.http.get<Sensordevice[]>(`/api/sensordevicesbysector/${sector}`);
  }

  getSensordevicesbytype(sector: String, type: String): Observable<Sensordevice[]> {
    return this.http.get<Sensordevice[]>(`/api/sensordevicesbytype/${sector}/${type}`);
  }

  editSensordevice(sensordevice: Sensordevice): Observable<any> {
    return this.http.put(`/api/sensordevice/${sensordevice._id}`, sensordevice, { responseType: 'text' });
  }

  deleteSensordevice(sensordevice: Sensordevice): Observable<any> {
    return this.http.delete(`/api/sensordevice/${sensordevice._id}`, { responseType: 'text' });
  }

}
