import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { SensordataService } from '../services/sensordata.service';
import { SensordeviceService } from '../services/sensordevice.service';
import { SectorService } from '../services/sector.service';
import { ToastComponent } from '../shared/toast/toast.component';
import { Sensordata } from '../shared/models/sensordata.model';
import { Sensordevice } from '../shared/models/sensordevice.model';
import { Sector } from '../shared/models/sector.model';
import { Chart } from 'angular-highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  sensordata = new Sensordata();
  sensordatas: Sensordata[] = [];
  sensordevice = new Sensordevice();
  sensordevices: Sensordevice[] = [];
  sector = new Sector();
  sectors: Sector[] = [];


  isLoading = true;
  isEditing = false;
  butDisabled = true;
  startDate: Date = new Date();
  endDate: Date = new Date();

  chart = new Chart();
  powerFactorChart = new Chart();
  energyChart = new Chart();
  energyExpectedChart = new Chart();
  activePowerChart = new Chart();
  sectorName = '';
  sensordeviceName = '';

  mode = 'All';
  literalMode = 'Energy';

  avgVoltage = 0;

  energyPrice = 0;
  energyPriceExpected = 0;
  energyPrice1 = 0;
  energyPrice2 = 0;
  energyPrice3 = 0;
  energyPrice4 = 0;

  priceCurrent1 = 0;
  priceCurrent2 = 0;
  priceCurrent3 = 0;
  priceCurrent4 = 0;

  priceLast1 = 0;
  priceLast2 = 0;
  priceLast3 = 0;
  priceLast4 = 0;
  priceLast = 0;
  priceLastExpected = 0;

  energyDatas = [];
  price = 0;
  priceExpected = 0;

  randNum = (Math.random() * 0.2) + 0.8;

  totalExpected = 0;
  totalLastExpected = 0;
  // Maps vars
  title = 'Plane';
  lat = 38.99770772836433;
  lng  = -3.919684914125355;

  // Vars Heat Map
  heatData = {};
  heatmapConfig = {
      maxOpacity: .2,
      minOpacity: 0,
      blur: .4
  };

  // Gauge vars
  public canvasWidth = 400;
  public needleValue = 0;
  public centralLabel = '';
  public name = 'Gauge chart';
  public bottomLabel = '1';
  public options = {
    hasNeedle: true,
    needleColor: 'gray',
    needleUpdateSpeed: 1000,
    arcColors: ['red', 'lightgray', 'red'],
    arcDelimiters: [40, 60],
    rangeLabel: ['0', '1.6'],
    needleStartValue: 0.5,
  };

  constructor(private sensordataService: SensordataService,
              private sectorService: SectorService,
              private sensordeviceService: SensordeviceService,
              private formBuilder: FormBuilder,
              public toast: ToastComponent,
              public datepipe: DatePipe) { }

  ngOnInit() {
    this.startDate.setDate(this.startDate.getDate() - 1);
    this.getSectors();
    this.drawAllCharts();
    function generateRandomData(len) {
      var max = 100;
      var min = 1;
      var maxX = 450;
      //console.log ("medidas");
      console.log (document.body.clientWidth);
      console.log (document.body.clientHeight)
      var maxY = 150;
      var data = [{x: 243, y: 144, value: 50, radius: 2}, {x: 103, y: 141, value: 19, radius: 26}, {x: 205, y: 159, value: 72, radius: 20}, {x: 493, y: 75, value: 66, radius: 14}, {x: 104, y: 111, value: 10, radius: 8}, {x: 237, y: 101, value: 18, radius: 30}, {x: 134, y: 93, value: 72, radius: 9}, {x: 253, y: 82, value: 99, radius: 42}, {x: 264, y: 72, value: 8, radius: 2}, {x: 366, y: 72, value: 88, radius: 26},{x: 283, y: 80, value: 27, radius: 16}, {x: 208, y: 116, value: 35, radius: 16}, {x: 391, y: 148, value: 18, radius: 31},
{x: 109, y: 144, value: 86, radius: 6}, {x: 320, y: 80, value: 71, radius: 14}, {x: 203, y: 167, value: 98, radius: 21}, {x: 186, y: 122, value: 74, radius: 45}, {x: 444, y: 93, value: 59, radius: 21}, {x: 74, y: 176, value: 81, radius: 23}, {x: 285, y: 70, value: 32, radius: 49}, {x: 108, y: 147, value: 43, radius: 38}, {x: 278, y: 62, value: 93, radius: 36}, {x: 228, y: 64, value: 60, radius: 49}, {x: 209, y: 172, value: 60, radius: 11}, {x: 251, y: 84, value: 41, radius: 36}, {x: 301, y: 138, value: 17, radius: 25}, {x: 248, y: 72, value: 36, radius: 26}, {x: 333, y: 190, value: 48, radius: 9}, {x: 492, y: 179, value: 64, radius: 12}, {x: 69, y: 137, value: 10, radius: 19}, {x: 205, y: 59, value: 12, radius: 47}, {x: 300, y: 137, value: 54, radius: 29}, {x: 119, y: 162, value: 94, radius: 33}, {x: 489, y: 52, value: 29, radius: 39}, {x: 231, y: 134, value: 39, radius: 11}, {x: 211, y: 80, value: 2, radius: 46}, {x: 123, y: 156, value: 11, radius: 43}, {x: 216, y: 79, value: 61, radius: 5}, {x: 415, y: 65, value: 51, radius: 31}, {x: 109, y: 192, value: 63, radius: 16}, {x: 209, y: 197, value: 59, radius: 6}, {x: 153, y: 137, value: 96, radius: 23}, {x: 295, y: 158, value: 99, radius: 18},
{x: 367, y: 118, value: 39, radius: 49}, {x: 367, y: 103, value: 17, radius: 11}, {x: 493, y: 108, value: 89, radius: 30}, {x: 313, y: 146, value: 99, radius: 24}, {x: 308, y: 181, value: 29, radius: 18}, {x: 300, y: 152, value: 81, radius: 27},
{x: 390, y: 59, value: 41, radius: 39}, {x: 154, y: 103, value: 31, radius: 25}, {x: 155, y: 142, value: 37, radius: 47}, {x: 164, y: 109, value: 9, radius: 1},{x: 194, y: 183, value: 81, radius: 2}, {x: 411, y: 175, value: 54, radius: 20},{x: 66, y: 147, value: 47, radius: 26}, {x: 427, y: 154, value: 69, radius: 29},{x: 229, y: 81, value: 82, radius: 10}, {x: 424, y: 159, value: 92, radius: 14}, {x: 87, y: 163, value: 43, radius: 33}, {x: 95, y: 52, value: 22, radius: 28}, {x: 214, y: 123, value: 75, radius: 12}, {x: 239, y: 50, value: 30, radius: 15}, {x: 63, y: 127, value: 88, radius: 6}, {x: 427, y: 77, value: 62, radius: 15}, {x: 470, y: 129, value: 99, radius: 39}, {x: 437, y: 114, value: 13, radius: 45}, {x: 464, y: 115, value: 31, radius: 46}, {x: 301, y: 64, value: 7, radius: 18}, {x: 406, y: 154, value: 21, radius: 24}, {x: 201, y: 131, value: 43, radius: 32}, {x: 318, y: 88, value: 43, radius: 38}, {x: 341, y: 130, value: 77, radius: 20}, {x: 103, y: 118, value: 43, radius: 37}, {x: 467, y: 193, value: 72, radius: 23}, {x: 392, y: 83, value: 63, radius: 1}, {x: 470, y: 100, value: 75, radius: 31}, {x: 84, y: 178, value: 60, radius: 24}, {x: 388, y: 67, value: 89, radius: 46},
{x: 451, y: 156, value: 60, radius: 46}, {x: 127, y: 139, value: 81, radius: 16}, {x: 433, y: 122, value: 36, radius: 28}, {x: 190, y: 160, value: 68, radius: 4}, {x: 83, y: 91, value: 1, radius: 13}, {x: 148, y: 119, value: 38, radius: 28}, {x: 275, y: 169, value: 58, radius: 9}, {x: 351, y: 168, value: 2, radius: 18}, {x: 336, y: 162, value: 13, radius: 24}, {x: 98, y: 154, value: 45, radius: 28}, {x: 161, y: 55, value: 57, radius: 41}, {x: 159, y: 113, value: 100, radius: 4}, {x: 474, y: 89, value: 76, radius: 7}, {x: 269, y: 172, value: 59, radius: 29}, {x: 100, y: 65, value: 33, radius: 23}, {x: 374, y: 100, value: 37, radius: 38}, {x: 285, y: 92, value: 75, radius: 37}, {x: 229, y: 91, value: 49, radius: 29},{x: 477, y: 98, value: 60, radius: 32},{x: 336, y: 94, value: 50, radius: 3}, {x: 403, y: 111, value: 49, radius: 30}];
     /* while (len--) {
        data.push({
          x: ((Math.random() * maxX) >> 0)+50,
          y: ((Math.random() * maxY) >> 0)+50,
          value: ((Math.random() * max + min) >> 0),
          radius: ((Math.random() * 50 + min) >> 0)
        });
      }*/

      console.log (data);
      return {
        max: max,
        min: min,
        data: data
      }
    };

    this.heatData = generateRandomData(100);

  }


  getSensordatas() {
    this.sensordataService.getSensordatas().subscribe(
      data => this.sensordatas = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  getSectors() {
    this.sectorService.getSectors().subscribe(
      data => this.sectors = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  getSensordevices() {
    this.sensordeviceService.getSensordevices().subscribe(
      data => this.sensordevices = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  getSensordevicesbytype(sector: String, mode: String) {
    this.sensordeviceService.getSensordevicesbytype(sector, 'all').subscribe(
      res => {
        this.sensordevices = res;
        if (this.sensordevices.length > 0) {
          this.butDisabled = false;
         } else {
          this.butDisabled = true;
        }
        this.sensordevice = new Sensordevice();
        // this.drawAllCharts();

      },
      error => console.log(error)
    );
  }

  getSensordataswithfilter(init: Number, end: Number, type: String, source: String, interval: Number) {

    this.sensordataService.getSensordataswithfilter(init, end, type, source, interval).subscribe(
      data => this.sensordatas = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  lastWeek() {
    this.endDate = new Date();
    this.startDate = new Date(); // .setDate(this.endDate.getDate()-7);
    this.startDate.setDate(this.startDate.getDate() - 6);
    this.drawAllCharts();

  }

  setHistory() {
    this.sensordataService.getSensordatasFirstDate().subscribe(
      res => {
        let  sensordataAux = new Sensordata();
        sensordataAux = res;
        this.startDate = new Date(sensordataAux.date);
        this.endDate = new Date();
        this.drawAllCharts();
      },
      error => console.log(error)
    );
  }

  changeMode(mode: string) {
    this.mode = mode;
    if (this.sectorName !== '') {
      this.getSensordevicesbytype(this.sectorName, this.mode);
    }
  }

  calculatePrice() {

    let totalEnergy = 0;
    for (let i = 0; i < this.energyDatas.length; i++) {
      for (let j = 0; j < this.energyDatas[i]['data'].length; j++) {
        totalEnergy = totalEnergy + this.energyDatas[i]['data'][j];
      }
    }

    console.log ("prueba de modelo predictivo")

    //Días para el modelo predictivo
    const  now = new Date();
    const today = String(now.getDate())+"-"+String(now.getMonth()+1)+"-"+String(now.getFullYear())
    const firstMonthDay = "01"+"-"+String(now.getMonth()+1)+"-"+String(now.getFullYear())
    const firstLastMonthDay = "01"+"-"+String(now.getMonth())+"-"+String(now.getFullYear())
    const lastDay = new Date(now.getFullYear(), now.getMonth(), 0);
    const lastLastMonthDay = String(lastDay.getDate())+"-"+String(now.getMonth())+"-"+String(now.getFullYear())
    this.sensordataService.getSensordatasPredictiveValue("ITSI", firstMonthDay, today).subscribe(
      res => {
        this.totalExpected = res;
        console.log (res);
      }

    )

    this.sensordataService.getSensordatasPredictiveValue("ITSI", firstLastMonthDay, lastLastMonthDay).subscribe(
      res => {
        this.totalLastExpected = res;
        console.log (res);
      }

    )

    //const  now = new Date();
    this.sensordataService.getEnergyMonth(now.getMonth() + 1).subscribe(
      res => {
        if (res[0]){
          this.priceCurrent1 = parseFloat((this.energyPrice1 * res[0].count).toFixed(2));
          this.priceCurrent2 = parseFloat((this.energyPrice2 * res[0].count).toFixed(2));
          this.priceCurrent3 = parseFloat((this.energyPrice3 * res[0].count).toFixed(2));
          this.priceCurrent4 = parseFloat((this.energyPrice4 * res[0].count).toFixed(2));
        }
      }
    );

    this.sensordataService.getEnergyMonth(now.getMonth()).subscribe(
      res => {
        if (res[0]){
          this.priceLast1 = parseFloat((this.energyPrice1 * res[0].count).toFixed(2));
          this.priceLast2 = parseFloat((this.energyPrice2 * res[0].count).toFixed(2));
          this.priceLast3 = parseFloat((this.energyPrice3 * res[0].count).toFixed(2));
          this.priceLast4 = parseFloat((this.energyPrice4 * res[0].count).toFixed(2));
        }
        this.priceLastExpected = parseFloat((this.energyPriceExpected * this.totalLastExpected).toFixed(2));
      }
    );

    this.price = parseFloat((this.energyPrice * totalEnergy).toFixed(2));
    this.priceExpected = parseFloat((this.energyPriceExpected * this.totalExpected).toFixed(2));
    console.log ("Precio esperado: "+this.priceExpected);
  }

  calculateAVGVoltage(devices: Array<Sensordevice>) {
    let literalAVG = '';
    for (let i = 0; i < devices.length; i++) {
      if (i === 0) {
        literalAVG = devices[0].citisimId;
      } else {
        literalAVG = literalAVG + ',' + devices[i].citisimId;
      }
    }

    this.sensordataService.getAVGVoltage(literalAVG).subscribe(
      data => this.avgVoltage = parseFloat(data[0].voltageAVG.toFixed(2)),
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  calculateAVGPF(devices: Array<Sensordevice>) {
    let literalAVG =  '';
    for (let i = 0; i < devices.length; i++) {
      if (i === 0) {
        literalAVG = devices[0].citisimId;
      } else {
        literalAVG = literalAVG + ',' + devices[i].citisimId;
      }
    }

    this.sensordataService.getAVGPF(literalAVG).subscribe(
      data => {
        this.needleValue = (parseFloat(data[0].powerFactorAVG.toFixed(2)) * 100) / 2;
        this.bottomLabel = String(parseFloat(data[0].powerFactorAVG.toFixed(2)));
      },
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  drawAllCharts () {
      this.createChart('Voltage');
      this.createChart('ActivePower');
      this.createChart('Energy');
      this.createChart('PowerFactor');
  }

  createChart(typeGraph: String) {
    // Calculate mins between dates
    const diffMs = (this.endDate.getTime() - this.startDate.getTime());
    const diffMins = Math.round((diffMs  /  1000) / 60); // minutes
    const interval = Math.round(diffMins / 24); // Intervalo para que se representen como máximo 24 valores

    this.chart = new Chart();
    let chartDevices = [];
    let chartCategories = [];
    const chartValues = [];
    let chartValuesExpected = [];
    let indexDevice = 0;

    if (this.sectorName === '') {
      this.sensordeviceService.getSensordevicesbytype('all', 'all').subscribe(
        res => {
          chartDevices = res;
         if (typeGraph === 'Voltage') {
            this.calculateAVGVoltage(chartDevices);
          }
          if (typeGraph === 'PowerFactor"') {
            this.calculateAVGPF(chartDevices);
          }
          for (let i = 0; i < chartDevices.length; i++) {
            this.sensordataService.
            getSensordataswithfilter(this.startDate.getTime(), this.endDate.getTime(), typeGraph, chartDevices[i].citisimId, interval).
            subscribe(
              res2 => {
                // set Categories
                if (res2.length > chartCategories.length || chartCategories.length === 0) {
                  chartCategories = [];
                  for (let j = 0; j < res2.length; j++) {
                    chartCategories.push(this.datepipe.transform(res2[j].date, 'MM/dd/yy HH:mm'));
                  }
                }
                if (res2.length > 0) {
                  const oneSerie = [];
                  const oneSerieExpected = [];
                  for (let j = 0; j < res2.length; j++) {
                    oneSerie.push(res2[j].value);
                    oneSerieExpected.push(res2[j].value * this.randNum);
                  }
                  const resultMatch = chartDevices.filter(function (entry) { return entry.citisimId === res2[0].source; });
                  chartValues.push ({'name': resultMatch[0].name, 'data': oneSerie});
                  chartValuesExpected.push ({'name': resultMatch[0].name, 'data': oneSerieExpected});
                }
                indexDevice++;
                if (indexDevice === chartDevices.length) {
                  switch (typeGraph) {
                    case 'Voltage':
                      this.voltageDrawChart(chartCategories, chartValues);
                      break;
                    case 'ActivePower':
                      this.activePowerDrawChart(chartCategories, chartValues);
                      break;
                    case 'Energy':
                      this.energyDatas = chartValues;
                      this.calculatePrice();
                      this.energyDrawChart(chartCategories, chartValues);

                      //Para dibujar el gráfico de expected
                      const  now = new Date();
                      console.log ("Chart Normales");
                      console.log (chartCategories);
                      const today = String(this.endDate.getDate())+"-"+String(this.endDate.getMonth()+1)+"-"+String(this.endDate.getFullYear())
                      const firstMonthDay = String(this.startDate.getDate())+"-"+String(this.startDate.getMonth()+1)+"-"+String(this.startDate.getFullYear())
                      this.sensordataService.getSensordatasPredictiveArray("ITSI", firstMonthDay, today).subscribe(
                        res3 => {
                          console.log ("Entra en 1");
                          console.log (this.startDate);
                          console.log (this.endDate);
                          chartValuesExpected = [];
                          chartValuesExpected.push ({'name': 'ITSI', 'data': res3[1]});
                          console.log ("Datos expected");
                          console.log (res3[0]);
                          console.log (chartValuesExpected);
                          this.energyExpectedDrawChart(res3[0], chartValuesExpected);
                          console.log (res3);
                        }
                      )
                      break;
                    case 'PowerFactor':
                      this.powerFactorDrawChart(chartCategories, chartValues);
                      break;
                    default:
                      console.log('Otro tipo');
                  }
                }
              },
              error => console.log(error)
            );

          }
        },
        error => console.log(error)
      );

    } else {
      if (this.sensordevice._id !== undefined) {
        if (typeGraph === 'Voltage') {
          const arrayAux = [];
          arrayAux.push(this.sensordevice);
          this.calculateAVGVoltage(arrayAux);
        }
        if (typeGraph === 'PowerFactor') {
            const arrayAux2 = [];
            arrayAux2.push(this.sensordevice);
            this.calculateAVGPF(chartDevices);
        }
        this.sensordataService.
        getSensordataswithfilter(this.startDate.getTime(), this.endDate.getTime(), typeGraph, this.sensordevice.citisimId, interval).
        subscribe(
          res => {
            // set Categories
            if (res.length > chartCategories.length || chartCategories.length === 0) {
              chartCategories = [];
              for (let j = 0; j < res.length; j++) {
                chartCategories.push(this.datepipe.transform(res[j].date, 'MM/dd/yy HH:mm'));
              }
            }
            if (res.length > 0) {
              const oneSerie = [];
              const oneSerieExpected = [];
              for (let j = 0; j < res.length; j++) {
                oneSerie.push(res[j].value);
                oneSerieExpected.push(res[j].value * this.randNum);
              }
              chartValues.push ({'name': this.sensordevice.name, 'data': oneSerie});
              chartValuesExpected.push ({'name': this.sensordevice.name, 'data': oneSerieExpected});
            }

            switch (typeGraph) {
              case 'Voltage':
                this.voltageDrawChart(chartCategories, chartValues);
                break;
              case 'ActivePower':
                this.activePowerDrawChart(chartCategories, chartValues);
                break;
              case 'Energy':
                this.energyDatas = chartValues;
                this.calculatePrice();
                this.energyDrawChart(chartCategories, chartValues);
                //Para dibujar el gráfico de expected
                console.log (chartCategories);
                const today = String(this.endDate.getDate())+"-"+String(this.endDate.getMonth()+1)+"-"+String(this.endDate.getFullYear())
                const firstMonthDay = String(this.startDate.getDate())+"-"+String(this.startDate.getMonth()+1)+"-"+String(this.startDate.getFullYear())
                console.log ("Chart Normales");
                console.log (chartCategories);
                this.sensordataService.getSensordatasPredictiveArray("ITSI", firstMonthDay, today).subscribe(
                  res3 => {
                    console.log ("Entra en 2");
                    console.log (this.startDate);
                    console.log (this.endDate);
                    chartValuesExpected = [];
                    chartValuesExpected.push ({'name': 'ITSI', 'data': res3[1]});
                    console.log ("Datos expected");
                    console.log (res3[0]);
                    console.log (chartValuesExpected);
                    this.energyExpectedDrawChart(res3[0], chartValuesExpected);
                    console.log (res);
                  }
                )
                break;
              case 'PowerFactor':
                this.powerFactorDrawChart(chartCategories, chartValues);
                break;
              default:
                console.log('Otro tipo');
            }
            indexDevice++;
          },
          error => console.log(error)
        );
      } else {
        this.sensordeviceService.getSensordevicesbytype(this.sectorName, 'all').subscribe(
          res => {
            chartDevices = res;
            if (typeGraph === 'Voltage') {
              this.calculateAVGVoltage(chartDevices);
            }
            if (typeGraph === 'PowerFactor') {
                this.calculateAVGPF(chartDevices);
            }
            for (let i = 0; i < chartDevices.length; i++) {
              this.sensordataService.
              getSensordataswithfilter(this.startDate.getTime(), this.endDate.getTime(), typeGraph, chartDevices[i].citisimId, interval).
              subscribe(
                res2 => {
                  // set Categories
                  if (res2.length > chartCategories.length || chartCategories.length === 0) {
                    chartCategories = [];
                    for (let j = 0; j < res2.length; j++) {
                      chartCategories.push(this.datepipe.transform(res2[j].date, 'MM/dd/yy HH:mm'));
                    }
                  }
                  if (res2.length > 0) {
                    const oneSerie = [];
                    const oneSerieExpected = [];
                    for (let j = 0; j < res2.length; j++) {
                      oneSerie.push(res2[j].value);
                      oneSerieExpected.push(res2[j].value * this.randNum);
                    }
                    const resultMatch = chartDevices.filter(function (entry) { return entry.citisimId === res2[0].source; });
                    chartValues.push ({'name': resultMatch[0].name, 'data': oneSerie});
                    chartValuesExpected.push ({'name': resultMatch[0].name, 'data': oneSerieExpected});
                  }
                  switch (typeGraph) {
                    case 'Voltage':
                      this.voltageDrawChart(chartCategories, chartValues);
                      break;
                    case 'ActivePower':
                      this.activePowerDrawChart(chartCategories, chartValues);
                      break;
                    case 'Energy':
                      this.energyDatas = chartValues;
                      this.calculatePrice();
                      console.log ("Chart Normales");
                      console.log (chartCategories);
                      this.energyDrawChart(chartCategories, chartValues);
                      this.energyDrawChart(chartCategories, chartValues);
                      //Para dibujar el gráfico de expected
                      console.log (chartCategories);
                      const today = String(this.endDate.getDate())+"-"+String(this.endDate.getMonth()+1)+"-"+String(this.endDate.getFullYear())
                      const firstMonthDay = String(this.startDate.getDate())+"-"+String(this.startDate.getMonth()+1)+"-"+String(this.startDate.getFullYear())
                      this.sensordataService.getSensordatasPredictiveArray("ITSI", firstMonthDay, today).subscribe(
                        res3 => {
                          console.log ("Entra en 3");
                          console.log (this.startDate);
                          console.log (this.endDate);
                          chartValuesExpected = [];
                          chartValuesExpected.push ({'name': 'ITSI', 'data': res3[1]});
                          console.log ("Datos expected");
                          console.log (res3[0]);
                          console.log (chartValuesExpected);
                          this.energyExpectedDrawChart(res3[0], chartValuesExpected);
                          console.log (res);
                        }
                      )
                      break;
                    case 'PowerFactor':
                      this.powerFactorDrawChart(chartCategories, chartValues);
                      break;
                    default:
                      console.log('Otro tipo');
                  }
                  // this.voltageDrawChart(chartCategories, chartValues);
                  indexDevice++;
                },
                error => console.log(error)
              );
            }
          },
          error => console.log(error)
        );
      }
    }
  }

  voltageDrawChart(chartCategories: Array<String>, chartValues: Array<Object>) {
    this.chart = new Chart({
        chart: {
            type: 'spline',
            marginRight: 10,
        },

        time: {
            useUTC: false
        },

        title: {
            text: 'VOLTAGE'
        },
        exporting: {
          enabled: true
      },
        xAxis: {
            categories: chartCategories,
            type: 'datetime',
            dateTimeLabelFormats: {
                hour: '%l:%M %p'
            }
        },
        yAxis: {
            title: {
                text: 'Voltage'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.y:.2f}'
        },
        legend: {
            enabled: true
        },
        series: chartValues
    });
  }

  activePowerDrawChart(chartCategories: Array<String>, chartValues: Array<Object>) {
    this.activePowerChart = new Chart({
        chart: {
            type: 'spline',
            marginRight: 10
        },

        time: {
            useUTC: false
        },

        title: {
            text: 'ACTIVE POWER'
        },
        xAxis: {
            categories: chartCategories
        },
        yAxis: {
            title: {
                text: 'kW'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.y:.2f}'
        },
        legend: {
            enabled: true
        },
        exporting: {
            enabled: false
        },
        series: chartValues
    });
  }

  energyDrawChart(chartCategories: Array<String>, chartValues: Array<Object>) {
    this.energyChart = new Chart({
        chart: {
            type: 'spline',
            marginRight: 10
        },

        time: {
            useUTC: false
        },

        title: {
            text: 'ENERGY'
        },
        xAxis: {
            categories: chartCategories
        },
        yAxis: {
            title: {
                text: 'kWH'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.y:.2f}'
        },
        legend: {
            enabled: true
        },
        exporting: {
            enabled: false
        },
        series: chartValues
    });
  }

  energyExpectedDrawChart(chartCategories: Array<String>, chartValues: Array<Object>) {
    this.energyExpectedChart = new Chart({
        chart: {
            type: 'spline',
            marginRight: 10
        },

        time: {
            useUTC: false
        },

        title: {
            text: 'ENERGY EXPECTED'
        },
        xAxis: {
            categories: chartCategories
        },
        yAxis: {
            title: {
                text: 'kWH'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br/>',
            pointFormat: '{point.y:.2f}'
        },
        legend: {
            enabled: true
        },
        exporting: {
            enabled: false
        },
        series: chartValues
    });
  }

  powerFactorDrawChart(chartCategories: Array<String>, chartValues: Array<Object>) {
    this.powerFactorChart = new Chart({

      chart: {
         type: 'spline'
      },

      title: {
          text: 'POWER FACTOR'
      },
      xAxis: {
          categories: chartCategories
      },
      yAxis: {
          title: {
                text: '%'
            }
      },
      tooltip: {
          headerFormat: '<b>{series.name}</b><br/>',
          pointFormat: '{point.y:.2f}'
      },
      legend: {
          enabled: true
      },
      exporting: {
          enabled: true
      },
      plotOptions: {
          series: {
              zones: [{
                  value: 0.85,
                  color: '#cc0000'
              }, {
                  value: 12,
                  color: '#7cb5ec'
              }, {
                  color: '#7cb5ec'
              }]
          }
      },

      series: chartValues
    });
  }
}
