import Sector from '../models/sector';
import BaseCtrl from './base';

export default class SectorCtrl extends BaseCtrl {
  model = Sector;

}
