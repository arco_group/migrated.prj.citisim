import Sensordata from '../models/sensordata';
import Sensordevice from '../models/sensordevice';
import BaseCtrl from './base';
import * as cron from 'node-cron';
import { spawn } from 'child_process';


// Chronetab to calculate de Energy each hour
cron.schedule('0 * * * *', () => {

    Sensordevice.find({'topic': 'Power'}).exec(function (err, sensordevices) {
      for (let i = 0; i < sensordevices.length; i++) {
        const d = new Date();

        d.setHours(d.getHours() - 1);
        const query = [
            {'$match': {'date': {$gte: d, '$lt': new Date()}, 'source': sensordevices[i].citisimId}},
            { '$group': {
                  '_id': null,
                  'value': { $avg: '$power'},
                  'source': {$last: '$source'},
                  'place': {$last: '$place'}
            }}
        ];
        Sensordata.aggregate(query).exec(function (err2, energyData) {
          if (energyData.length > 0) {
            const energyValues = new Sensordata({
              'type': 'Energy',
              'source': energyData[0].source,
              'energy': energyData[0].value,
              'place': energyData[0].place,
              'date': new Date()
            });
            energyValues.save(function (err3, results) {
              console.log ('Energía insertada');
              console.log(results._id);
            });
          }
        });
      }
    });
});


export default class SensordataCtrl extends BaseCtrl {
  model = Sensordata;

  // Get by id
  getwithfilter = async (req, res) => {
    try {
      const startDate = new Date(Number (req.params.init));
      const endDate = new Date(Number (req.params.end));
      const match = {'date': {$gte: startDate, '$lt': endDate}};
      match['type'] = req.params.type;
      if (req.params.source !== 'all') {
        match['source'] = req.params.source;
      }

      let valueQuery = {};
      switch (req.params.type) {
        case 'Voltage':
          valueQuery = { '$avg': '$voltage' };
          break;
        case 'ActivePower':
          valueQuery = { '$avg': '$power' };
          break;
        case 'PowerFactor':
          valueQuery = { '$avg': '$powerFactor' };
          break;
        case 'Energy':
          valueQuery = { '$sum': '$energy' };
          break;
        default:
          valueQuery = {};
      }

      const query = [
        {'$match': match},
        { '$group': {
            '_id': { '$add': [new Date(0), {
                '$subtract': [
                    { '$subtract': [ '$date', new Date ('1970-01-01') ] },
                    {'$mod': [
                        { '$subtract': [ '$date', new Date('1970-01-01') ] },
                        1000 * 60 * req.params.interval
                    ]}
                ]}
            ]},
           'value': valueQuery,
           'source': { '$last': '$source'}
        }},
        {'$project': {'_id': 0, 'date': '$_id', 'value': 1, 'source': 1}},
        { '$sort' : {'date' : 1} }
      ];
      const obj = await this.model.aggregate(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }

  // get AVG voltage
  getavgvoltage = async (req, res) => {
    try {
      const ar = req.params.sources.split(',');
      const query = [{ '$match': {'type': 'Voltage', 'source': {'$in': ar}}},
        { '$group': {
          '_id': null,
          'voltageAVG': { $avg: '$voltage'}
        }}
      ];
      const obj = await this.model.aggregate(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
  // get AVG Power Factor
  getavgPowerFactor = async (req, res) => {
    try {
      const ar = req.params.sources.split(',');
      const query = [{ '$match': {'type': 'PowerFactor', 'source': {'$in': ar}}},
        { '$group': {
          '_id': null,
          'powerFactorAVG': { $avg: '$powerFactor'}
        }}
      ];
      const obj = await this.model.aggregate(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
  // Get energy of a Month
  getEnergyCurrentMonth = async (req, res) => {
    console.log ("Llega a entrar en el WS, entonces es este el que muere");
    try {
      const query =
      [
        {$project: {energy: 1, month: {$month: '$date'}}},
        {$match: {month: Number(req.params.month)}},
        {
         $group:
           {
             _id: null,
             count: { $sum: '$energy' }
           }
        }
      ];
      const obj = await this.model.aggregate(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }

  // Get Fails of Voltage
  getfailvoltage = async (req, res) => {
    try {
      const ar = req.params.sources.split(',');
      const query = [{ '$match': {'type': 'Voltage', 'source': {'$in': ar}, 'voltage' : {$lt: 200}}},
        {'$group': {
           '_id': null,
           'fallsum"': { $sum: 1}
        }}
      ];
      const obj = await this.model.aggregate(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
  // Get First date with Data
  getFirstDate = async (req, res) => {
    try {
      const obj = await this.model.findOne().sort({date: 1});
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message});
    }
  }

  getPredictiveModel = async (req, res) => {
    try {
      const pythonProcess = spawn('python3', ["/home/vagrant/citisim/server/scripts/predictive-model/predictive.py", req.params.source, req.params.init, req.params.end]);
      //res.status(200).json(pythonProcess);
      var finalArray = [];
      pythonProcess.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        console.log (data.toString());
        var dataAux = data.toString().replace("{","");
        dataAux = dataAux.replace("}","");
        var arrayData = dataAux.split("\n");
        if (arrayData[3]){
          arrayData = arrayData[3].split(",");
          arrayData = arrayData.sort();
        }

        
        finalArray[0] = [];
        finalArray[1] = [];
        arrayData.forEach(function(element) {
          var dataSplit = element.split(": ");
          dataSplit[0] = dataSplit[0].replace("'","");
          //var dataSplit = element.split(": ");
          dataSplit[0] = dataSplit[0].replace(" ","");
          var dateModel = dataSplit[0].split(":");
          dateModel[0] = dateModel[0].replace("'","");
          if (dateModel[1])
            dateModel[1] = dateModel[1].replace("'","");
          finalArray[0].push(dateModel[0]+" "+dateModel[1]+":00");
          finalArray[1].push(parseFloat(dataSplit[1]));
        });

      });

      pythonProcess.stderr.on('data', (data) => {
        console.log(`stderr: ${data}`);
      });

      pythonProcess.on('close', (code) => {
        res.status(200).json(finalArray);
        //console.log(`child process exited with code ${code}`);
      });
      
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
  
  getPredictiveValue = async (req, res) => {
    try {
      const pythonProcess = spawn('python3', ["/home/vagrant/citisim/server/scripts/predictive-model/predictive.py", req.params.source, req.params.init, req.params.end]);
      //res.status(200).json(pythonProcess);

      var totalEnergy = 0; 
      pythonProcess.stdout.on('data', (data) => {
        //console.log(`stdout: ${data}`);
        //console.log (data.toString());
        var dataAux = data.toString().replace("{","");
        dataAux = dataAux.replace("}","");
        var arrayData = dataAux.split("\n");
        if (arrayData[3])
          arrayData = arrayData[3].split(",");

        arrayData.forEach(function(element) {
          var dataSplit = element.split(": ");
          totalEnergy = totalEnergy + parseFloat(dataSplit[1]);
        });
      });

      pythonProcess.stderr.on('data', (data) => {
        //console.log(`stderr: ${data}`);
      });

      pythonProcess.on('close', (code) => {
        res.status(200).json(totalEnergy);
        //console.log(`child process exited with code ${code}`);
      });
      
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }

  // Purge data
  purgeData = async (req, res) => {
    try {
      const obj = await this.model.find({'source': req.params.source}).sort({date: 1}).limit(200000);

      let ref = 0;
      for (let i = 1; i < obj.length; i++) {
          const seconds = (obj[i].date - obj[ref].date) / 1000;
          if (seconds < 180) {
            await this.model.findOneAndRemove({ _id: obj[i]._id });
          } else {
            ref = i;
          }
      }
    } catch (err) {
      return res.status(500).json({ error: err.message});
    }
  }
  // Refill static datas
  refillData = async (req, res) => {
    try {
      const start = Number(req.params.dateStart);
      const end = Number(req.params.dateEnd);
      const value1 = req.params.rangeStart * 100;
      const value2 = req.params.rangeEnd * 100;
      const interval = req.params.interval * 60000;
      for (let i = start; i < end; i = i + interval) {
        const value = Math.floor(Math.random() * (value2 - value1 + 1) + value1) / 100;
        switch (req.params.topic) {
          case 'Voltage':
            const objV = await new this.model(
              {'type' : 'Voltage',
               'source' : req.params.source,
               'date' : new Date(i),
               'voltage' : value
              }
            ).save();
            break;
          case 'ActivePower':
            const objAP = await new this.model(
              {'type' : 'ActivePower',
               'source' : req.params.source,
               'date' : new Date(i),
               'power' : value
              }
            ).save();
            break;
          case 'PowerFactor':
            const objPF = await new this.model(
              {'type' : 'PowerFactor',
               'source' : req.params.source,
               'date' : new Date(i),
               'powerFactor' : value
              }
            ).save();
            break;
          case 'Energy':
            const objE = await new this.model(
              {'type' : 'Energy',
               'source' : req.params.source,
               'date' : new Date(i),
               'energy' : value
              }
            ).save();
            break;
          default:
        }
      }
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }
}