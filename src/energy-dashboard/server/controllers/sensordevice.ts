import SensorDevice from '../models/sensordevice';
import BaseCtrl from './base';

const {ObjectId} = require('mongodb');

export default class SensorDeviceCtrl extends BaseCtrl {
  model = SensorDevice;

  // Get by id
  getSector = async (req, res) => {
    try {
      const obj = await this.model.find({ sector: ObjectId(req.params.sector) });
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }

  // Get by Type && sector
  getType = async (req, res) => {
    try {

      let query = {};
      if (req.params.sector !== 'all') {
        if (req.params.type !== 'all') {
          query = { sector: ObjectId(req.params.sector), type: req.params.type};
        } else {
          query = { sector: ObjectId(req.params.sector)};
          console.log (query);
        }
      } else {
        if (req.params.type !== 'all') {
          query = { type: req.params.type};
        } else {
          query = {};
        }
      }
      const obj = await this.model.find(query);
      res.status(200).json(obj);
    } catch (err) {
      return res.status(500).json({ error: err.message });
    }
  }

}
