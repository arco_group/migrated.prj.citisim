import * as mongoose from 'mongoose';

const sensordataSchema = new mongoose.Schema({
  id: String,
  type: String,
  temperature: Number,
  voltage: Number,
  energy: Number,
  power: Number,
  powerFactor: Number,
  place: String,
  source: String,
  date: Date
});

const Sensordata = mongoose.model('Sensordata', sensordataSchema);

export default Sensordata;
