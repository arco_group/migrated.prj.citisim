import * as mongoose from 'mongoose';

const sectorSchema = new mongoose.Schema({
  name: String,
  latitude: Number,
  longitude: Number,
  altitude: Number,
  description: String
});

const Sector = mongoose.model('Sector', sectorSchema);

export default Sector;
