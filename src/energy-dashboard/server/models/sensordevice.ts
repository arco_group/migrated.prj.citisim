import * as mongoose from 'mongoose';

const sensordeviceSchema = new mongoose.Schema({
  name: String,
  sector: mongoose.Schema.Types.ObjectId,
  citisimId: String,
  latitude: Number,
  longitude: Number,
  altitude: Number,
  description: String
});

const Sensordevice = mongoose.model('Sensordevice', sensordeviceSchema);

export default Sensordevice;
