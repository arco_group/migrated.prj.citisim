#!/usr/bin/python3

from datetime import datetime, timedelta
from libcitisim import Broker
import sys

if __name__ == "__main__":
    broker = Broker("/home/vagrant/citisim/server/scripts/predictive-model/libcitisim.config")

    start_date = sys.argv[2];
    end_date = sys.argv[3]

    forecast = broker.energy_model.getEnergyForecast(
        sys.argv[1], start_date, end_date)
    print(sys.argv[1])
    print(start_date)
    print(end_date)
    print(format(forecast))
