#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import datetime
import sys
from libcitisim import Broker

from pymongo import MongoClient
client = MongoClient('localhost', 27017)

db = client.citisim

NOTIFY_MSG = '''
New notification:
  * data: {:.2f}
  * source: {}'''


class ActivePowerSubscriber:
    def run(self, args):
        config = 'subscriber.config'
        if len(args) > 1:
            config = args[1]

        topic_name = "ActivePower"
        broker = Broker(config)

        print("Subscribing to '" + topic_name + "' topic")
        broker.subscribe(topic_name, self.event_printer)

        print("Awaiting data...")
        broker.wait_for_events()

    def event_printer(self, value, source, metadata):
        try:
            print(NOTIFY_MSG.format(value, source))
            sensordatas = db.sensordatas
            sensorValues = {
                'type': 'ActivePower',
                'source': source,
                'power': value,
                'place': metadata['place'],
                'date': datetime.datetime.now()
            }
            result = sensordatas.insert_one(sensorValues)
            for key in metadata:
                print("  * {}: {}".format(key, metadata[key]))
        except:
            print("An exception occurred")
if __name__ == "__main__":
    ActivePowerSubscriber().run(sys.argv)

