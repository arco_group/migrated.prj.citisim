#!/usr/bin/python3
# -*- coding: utf-8; mode: python -*-

import datetime
import sys
from libcitisim import Broker

from pymongo import MongoClient
client = MongoClient('localhost',27017)

db = client.citisim

NOTIFY_MSG = '''
New notification:
  * data: {:.2f}
  * source: {}'''


class GenericConnector:
    def run(self, args):

        print ("Argumento 2" + args[2])
        config = 'subscriber.config'
        if len(args) > 1:
            config = args[1]

        topic_name = args[2]
        broker = Broker(config)

        print("Subscribing to '" + topic_name + "' topic")
        broker.subscribe(topic_name, self.event_printer)

        print("Awaiting data...")
        broker.wait_for_events()

    def event_printer(self, value, source, metadata):
        print(NOTIFY_MSG.format(value, source))
        sensordatas = db.sensordatas
        lastData = sensordatas.find_one({'source':source}, sort=[('date', -1)])
        if abs((datetime.datetime.now() - lastData['date']).seconds) > 180:
              sensorValues = {
                'type': sys.argv[2],
                'source': source,
                sys.argv[3]: value,
		'place': metadata['place'],
                'date': datetime.datetime.now()
              }
              result = sensordatas.insert_one(sensorValues)
        for key in metadata:
            print("  * {}: {}".format(key, metadata[key]))


if __name__ == "__main__":
    GenericConnector().run(sys.argv)
