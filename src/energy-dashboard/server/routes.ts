import * as express from 'express';

import UserCtrl from './controllers/user';
import SectorCtrl from './controllers/sector';
import SensordeviceCtrl from './controllers/sensordevice';
import SensordataCtrl from './controllers/sensordata';

export default function setRoutes(app) {

  const router = express.Router();

  const userCtrl = new UserCtrl();
  const sectorCtrl = new SectorCtrl();
  const sensordeviceCtrl = new SensordeviceCtrl();
  const sensordataCtrl = new SensordataCtrl();

  // Users
  router.route('/login').post(userCtrl.login);
  router.route('/users').get(userCtrl.getAll);
  router.route('/users/count').get(userCtrl.count);
  router.route('/user').post(userCtrl.insert);
  router.route('/user/:id').get(userCtrl.get);
  router.route('/user/:id').put(userCtrl.update);
  router.route('/user/:id').delete(userCtrl.delete);

  //Sector
  router.route('/sectors').get(sectorCtrl.getAll);
  router.route('/sectors/count').get(sectorCtrl.count);
  router.route('/sector').post(sectorCtrl.insert);
  router.route('/sector/:id').get(sectorCtrl.get);
  router.route('/sector/:id').put(sectorCtrl.update);
  router.route('/sector/:id').delete(sectorCtrl.delete);

  //Sensordevices
  router.route('/sensordevices').get(sensordeviceCtrl.getAll);
  router.route('/sensordevices/count').get(sensordeviceCtrl.count);
  router.route('/sensordevice').post(sensordeviceCtrl.insert);
  router.route('/sensordevice/:id').get(sensordeviceCtrl.get);
  router.route('/sensordevicesbysector/:sector').get(sensordeviceCtrl.getSector);
  router.route('/sensordevicesbytype/:sector/:type').get(sensordeviceCtrl.getType);
  router.route('/sensordevice/:id').put(sensordeviceCtrl.update);
  router.route('/sensordevice/:id').delete(sensordeviceCtrl.delete);

  //SensorData
  router.route('/sensordatas').get(sensordataCtrl.getAll);
  router.route('/sensordatas/count').get(sensordataCtrl.count);
  router.route('/sensordatasfirstdate').get(sensordataCtrl.getFirstDate);
  router.route('/sensordata').post(sensordataCtrl.insert);
  router.route('/sensordata/:id').get(sensordataCtrl.get);
  router.route('/sensordata/:id').put(sensordataCtrl.update);
  router.route('/sensordata/:id').delete(sensordataCtrl.delete);
  router.route('/sensordatas/:init/:end/:type/:source/:interval').get(sensordataCtrl.getwithfilter);
  router.route('/avgvoltage/:sources').get(sensordataCtrl.getavgvoltage);
  router.route('/avgpowerfactor/:sources').get(sensordataCtrl.getavgPowerFactor);
  router.route('/failvoltage/:sources').get(sensordataCtrl.getfailvoltage);
  router.route('/refilldata/:source/:topic/:rangeStart/:rangeEnd/:dateStart/:dateEnd/:interval').get(sensordataCtrl.refillData);
  router.route('/energyMonth/:month').get(sensordataCtrl.getEnergyCurrentMonth);
  router.route('/predictivedata/:source/:init/:end/').get(sensordataCtrl.getPredictiveModel);
  router.route('/predictivevalue/:source/:init/:end/').get(sensordataCtrl.getPredictiveValue);
  router.route('/purge/:source').get(sensordataCtrl.purgeData);

  // Apply the routes to our application with the prefix /api
  app.use('/api', router);

}
