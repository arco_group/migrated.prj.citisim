// -*- mode: cpp; coding: utf-8 -*-

#include <Arduino.h>
#include <ESP8266WiFi.h>

#include <IceC/IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>
#include <IceC-IoT-node.h>

// NOTE: remember to run 'make' to generate this file
#include "flux-sensor.h"

// IceC broker and adapter
Ice_Communicator ic;
Ice_ObjectAdapter adapter;
Ice_ObjectPrx publisher;

// servants
Citisim_FluxSensor node;

volatile unsigned int flux_frequency;
bool enabled;

ICACHE_RAM_ATTR
void flux_detected() {
  flux_frequency++;
}

void raise_alarm(String event) {
  Serial.println("App: Gas leak detected");
  Serial.printf("App: - value: %d\n", flux_frequency);
  if (!is_online)
    return;

  SmartObject_Metadata meta;
  SmartObject_Metadata_init(meta, NULL, 0);
  new_Ice_String(alarm, event.c_str());
  new_Ice_String(source, STR(TRANSDUCER_ADDR));
  SmartObject_EventSink_notify(&publisher, alarm, source, meta);
}

void check_flux() {
  if (!enabled)
    return;

  flux_frequency = 0;
  interrupts();
  delay(500);
  noInterrupts();

  if (flux_frequency > 0) {
    led_blink(LEAK_TIME, 75);
    digitalWrite(LED_BUILTIN, HIGH);
    raise_alarm("GasLeak");
  }
}

void setup() {
  Serial.begin(115200);
  Serial.flush();

  pinMode(FLUX_SENSOR, INPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  //initialize variables
  flux_frequency = 0;
  enabled = false;

  // setup an interrupt in flux sensor and enable interrupts
  attachInterrupt(digitalPinToInterrupt(FLUX_SENSOR), flux_detected, RISING);

  delay(1000);
  Serial.println("\n------Booting---------\n");

  // setup WiFi, Ice, Endpoints...
  IceC_Storage_begin();
  setup_wireless();
  setup_ota();

  Ice_initialize(&ic);
  TCPEndpoint_init(&ic);

  // set Citisim locator
  Ice_Communicator_setDefaultLocator
    (&ic, "IceGrid/Locator -t:tcp -h pike.esi.uclm.es -p 5061");

  // setup adapter and register servants
  Ice_Communicator_createObjectAdapterWithEndpoints
    (&ic, "Adapter", "tcp -p 4455", &adapter);
  Ice_ObjectAdapter_activate(&adapter);
  Citisim_FluxSensor_init(&node);

  // register twice to be able to connect it even if ADDR is unknown
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, "Node");
  Ice_ObjectAdapter_add(&adapter, (Ice_ObjectPtr)&node, STR(TRANSDUCER_ADDR));

  // register on Welcome Service
  if (is_online) {
    Ice_ObjectPrx welcome;
    Ice_Communicator_stringToProxy(&ic, "WelcomeServer -o @ WelcomeServiceAdapter", &welcome);
    String proxy = STR(TRANSDUCER_ADDR) " -o -e 1.0:tcp -h " + get_local_ip() + " -p 4455";
    new_Ice_String(address, proxy.c_str());
    new_Ice_String(type, STR(TRANSDUCER_TYPE));
    SmartObject_WelcomeService_hello(&welcome, address, type);
  }

  // everything is fine, say it to the world!
  Serial.println("\n------Boot done!------\n");
  led_blink(5, 50);
  digitalWrite(LED_BUILTIN, HIGH);
}

void loop() {
  Ice_Communicator_loopIteration(&ic);
  handle_ota();

  // enable only when instructed
  if (digitalRead(PIN_BUTTON) == 0) {
    led_blink(20, 20);
    digitalWrite(LED_BUILTIN, HIGH);

    enabled = !enabled;

    if (enabled) {
      digitalWrite(LED_BUILTIN, LOW);
      delay(1000);
      digitalWrite(LED_BUILTIN, HIGH);
    }

    Serial.printf("App: enabled: %d\n", enabled);
  }

  check_buttons();
  check_flux();
}

// redirect callbacks (needed here to overwrite __weak__ defs)
void IoT_NodeAdminI_restart(IoT_NodeAdminPtr self) { async_restart_node(); }
void IoT_NodeAdminI_factoryReset(IoT_NodeAdminPtr self) { async_factory_reset(); }
void IoT_WiFiAdminI_setupWiFi(IoT_WiFiAdminPtr self, Ice_String ssid, Ice_String key) {
    store_wifi_settings(ssid, key);
}

void SmartObject_ObservableI_setObserver(SmartObject_ObservablePtr self, Ice_String Observer){
  char observer_prx[Observer.size];
  strncpy(observer_prx, Observer.value, Observer.size);
  Ice_Communicator_stringToProxy(&ic, observer_prx, &publisher);
}