#!/usr/bin/python3

import os
import sys
import Ice
import logging
from argparse import ArgumentParser
from libcitisim import Broker, SmartServices
from pygame import mixer
from glob import glob

logging.basicConfig(level=logging.INFO)


class MessageRenderI(SmartServices.MessageRender):
    def __init__(self, path):
        self.sounds = self.read_sounds(path)
        self.playing = None

    def render(self, messageid, current):
        if self.playing is not None:
            logging.info("- already playing!")
            return

        try:
            filename = self.sounds[messageid]
            self.playing = mixer.Sound(filename)
            self.playing.play(5)
            logging.info("- render message: {}".format(messageid))
        except KeyError:
            logging.error("- unknown message: {}!".format(messageid))

    def clear(self, current):
        if self.playing is None:
            logging.info("- stop (nothing done)")
            return

        self.playing.stop()
        self.playing = None
        logging.info("- stopped")

    def read_sounds(self, path):
        retval = {}
        for f in glob(path + "/*.wav"):
            name = os.path.splitext(os.path.basename(f))[0]
            retval[name] = f
        return retval


class SpeakerServer(Ice.Application):
    def run(self, args):
        if not self.parse_args(args):
            return -1

        self.ic = self.communicator()
        self.broker = Broker(ic=self.ic)
        mixer.init()

        sourceid = self.broker.props.getProperty("Speaker.SourceID")
        if not sourceid:
            logging.error("Property 'Speaker.SourceID' not provided!")
            return 1

        prx = self.broker.adapter_add(MessageRenderI(self.args.path), sourceid)
        self.broker.hello(str(prx.ice_oneway()), "Speaker")

        logging.info('Service ready, waiting events...')
        self.broker.wait_for_events()

    def parse_args(self, args):
        parser = ArgumentParser()
        parser.add_argument("--path", default="/usr/share/citisim-speaker-server/sounds",
            help="directory where sound will be taken")

        try:
            self.args = parser.parse_args(args[1:])
            return True
        except SystemExit:
            return False


if __name__ == "__main__":
    SpeakerServer().main(sys.argv)