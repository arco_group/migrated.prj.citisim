Overview
========

This asset provides a runable environment with the deployed services
that are being developed for Citisim.


Starting the system
===================

Just run in a terminal the following command:

```sh
$ vagrant up
```

When it finishes, you can access it using ssh:

```sh
$ vagrant ssh
```


Exposed services
================

* **IceGrid Registry (Audit-IceGrid)**:
    * Proxy: `Audit-IceGrid/Registry -t:tcp -h 127.0.0.1 -p 4061`
    * `icegridgui` settings:
        * Host: 127.0.0.1
        * Port: *empty* (or 4061)
        * Username: *anything* (i.e: user)
        * Password: *anything* (i.e: password)
	<br><br>

* **IceStorm-GUI**:
    * Web app: [http://127.0.0.1:8080](http://127.0.0.1:8080)
    * Settings:
        * Ice Locator: `Audit-IceGrid/Locator -t:ws -h 127.0.0.1 -p 4071`
        * IceStorm Proxy: `IceStorm/TopicManager -t:ws -h 127.0.0.1 -p 8194`
        * IceStorm Admin: `IceStorm/admin -t:ws -h 127.0.0.1 -p 8195`
	<br><br>

* **Persistence Service**:
    * Web app: [http://127.0.0.1:8081](http://127.0.0.1:8081)
    * Username: admin
    * Password: citisim.
    <br><br>

* **Node Controller**:
    * web app: [http://127.0.0.1:8082](http://127.0.0.1:8082)
    <br><br>

* **BiDir IceStorm Service**:
    * Proxy (use locator): `BidirIceStorm/TopicManager`
    * Example application (run it **outside** the VM):
    <br><br>

* **Service Dashboard**:
    * Web app: [http://127.0.0.1:8084](http://127.0.0.1:8084)
    <br><br>

```sh
cd tools
./analogsink-subscriber-bidir.py
```

Internal services
==================

* **Property Service**:
    * Proxy: `PropertyServer -t:tcp -h 127.0.0.1 -p 4334`
    <br><br>

* **IceStorm Service**:
    * Proxy: `IceStorm/TopicManager -t:tcp -h 127.0.0.1 -p 8192`
    <br><br>

* **Wiring Service**:
    * Proxy: `WiringService -t:tcp -h 127.0.0.1 -p 7160`
    <br><br>

* **Occupancy Service**:
    * Topics: `PersonTrackerTopic`, `OccupancyChanges`, `PersonMovements`
    <br><br>

* **Dashboard**:
    * Webapp Data Sink
    * IceGrid Data Collector
    * IceStorm Data Collection
    <br><br>

<br>
