﻿// -*- mode: c++; coding: utf-8 -*-

// This file contents should be coordinated with CITISIM partners
// acording to project requirements

module SmartObject {

    interface Observable {
	void setObserver(string observer);
    };

    enum MetadataField {
	Timestamp,    // Unix time 
	Quality,      // 0:unknown, 1:poor, 255:best 
	Expiration,   // seconds
	Latitude,     // GPS coordinates (ie. 42.361020)
	Longitude,    // GPS coordinates (ie. 0.160056)
	Altitude,     // GPS coordinates (ie. 1852.25)
	Place	      // Human readable coarse position description
    };

	//["clr:generic:SortedDictionary"]
    dictionary <MetadataField, string> Metadata;

    // G-Force sensed on each axis (m/s² or Gs) 
    struct Acceleration {
	float x;
	float y;
	float z;
    };

    interface DigitalSink {
	void notify(bool value, string source, Metadata data);
    };

    interface AnalogSink {
	void notify(float value, string source, Metadata data); 
    };

    interface AccelerometerSink {
	void notify(Acceleration value, string source, Metadata data);
    };

    interface EventSink {
	void notify(string source, Metadata data);
    };
};
