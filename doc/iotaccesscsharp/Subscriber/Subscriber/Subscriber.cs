﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ice;
using IceStorm;
using SmartObject;

namespace Subscriber
{
    public class Subscriber
    {
        class App : Ice.Application
        {

            public class AnalogSinkI : AnalogSinkDisp_
            {
                public override void notify(float value, string source, System.Collections.Generic.Dictionary<MetadataField, string> meta, Ice.Current current)
                {
                    var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    var timeSpan = TimeSpan.FromSeconds(Convert.ToDouble(meta[MetadataField.Timestamp]));
                    var human_ts = epoch.Add(timeSpan).ToLocalTime();

                    String msg = String.Format("New notification:\n " +
                        "* data: {0}\n " +
                        "* source: {1}\n " +
                        "* timestamp: {2}\n " +
                        "* location: {3}\n " +
                        "* quality: {4}\n " +
                        "* expiration: {5}s\n", value, source, human_ts, meta[MetadataField.Place], meta[MetadataField.Quality], meta[MetadataField.Expiration]);
                    Console.WriteLine(msg);
                }
            }

            public override int run(string[] args)
            {
                ObjectPrx servant_prx = Register_servant();
                Console.WriteLine("subcscriptor's proxy: " + communicator().proxyToString(servant_prx));

                string topicListProperty = "LibeliumNotifier.Topic.List";
                Properties props = communicator().getProperties();
                Dictionary<String, String> topicNames = props.getProperty(topicListProperty).TrimEnd(',')
                    .Split(',').ToDictionary(item => item.Split(':')[0], 
                    item => item.Split(':')[1].Replace("\'", "").Replace(" ", "").Replace("}",""));


                foreach (var item in topicNames)
                {
                    Console.WriteLine("Subscribing to:" + item.Value);
                    Subscribe_to_topic(servant_prx, item.Value);
                }

                Console.WriteLine("Subscribed to all topics. Awaiting data:");
                shutdownOnInterrupt();
                communicator().waitForShutdown();

                return 0;
            }

            private ObjectPrx Register_servant()
            {
                Communicator ic = communicator();
                AnalogSink servant = new AnalogSinkI();

                ObjectAdapter adapter = ic.createObjectAdapter("Adapter");
                adapter.activate();

                return adapter.addWithUUID(servant);
            }

            private void Subscribe_to_topic(ObjectPrx proxy, String topicName)
            {
                Dictionary<String, String> qos = new Dictionary<String, String>();
                TopicPrx topic = Get_topic(topicName);
                topic.subscribeAndGetPublisher(qos, proxy);
            }

            private TopicPrx Get_topic(String topicName)
            {
                Communicator ic = communicator();
                ObjectPrx manager_prx = ic.propertyToProxy("IceStorm.TopicManager.Proxy");
                IceStorm.TopicManagerPrx manager = IceStorm.TopicManagerPrxHelper.checkedCast(manager_prx);

                try
                {
                    return manager.retrieve(topicName);
                }
                catch
                {
                    Console.WriteLine("Temporary Error. Try again later.");
                    return null;
                }

            }
        }

        public static int Main(string[] args)
        {
            App app = new App();
            return app.main(args, "../../subscriber.config");
        }
    }
}