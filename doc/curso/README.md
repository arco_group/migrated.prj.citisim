
Descripción
===========

Curso de ZerocC Ice.


Parte 1: (2 horas) Hello Ice
----------------------------

* Hello World (Java)
* IceStorm (Java)
* Asynchronous Method Invocation (Java)
* Asynchronous Method Dispatch (Java)

Parte 2: (2 horas) Actuadores ESP
---------------------------------

* Actuador IceC (ESP, DigitalSink)
* CLI de control (Java, DigitalSink)
* Web client (IceJS, DigitalSink)

Parte 3: (2 horas) Sensores ESP
-------------------------------

* Sensor IceC (ESP, AnalogSink)
* Observer CLI de eventos de temperatura (Java, AnalogSink)
* WebApp para eventos de temperatura (JS, SSE, AnalogSink)
* WebApp para eventos de temperatura (JS, IceJS, AnalogSink)

