#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("iot.ice")
import SmartObject  # noqa


class AnalogSinkI(SmartObject.AnalogSink):
    def notify(self, value, source, meta, current):
        print("notify called with value: {}".format(value))


class Observer(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapterWithEndpoints(
            "Adapter", "tcp -p 7894")
        adapter.activate()

        oid = ic.stringToIdentity("TempListener")
        proxy = adapter.add(AnalogSinkI(), oid)

        print("Proxy: '{}'".format(proxy))
        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    Observer().main(sys.argv)
