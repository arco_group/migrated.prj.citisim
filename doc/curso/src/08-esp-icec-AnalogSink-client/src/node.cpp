// -*- mode: c++; coding: utf-8 -*-

// SonOff TH10 usable pins:
// - GPIO  0: Button
// - GPIO 12: Relay + LED1
// - GPIO 13: LED2
// - GPIO 14: TRRS connector (Jack tip)

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <Ticker.h>
#include <SPI.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <DHT.h>

#include <IceC.h>
#include <IceC/platforms/esp8266/TCPEndpoint.h>
#include <IceC/platforms/esp8266/debug.hpp>

#include "iot.h"

#define WIFI_SSID "volatile"
#define WIFI_KEY  "123698741"


Ice_Communicator ic;
Ice_ObjectAdapter adapter;
SmartObject_Observable servant;
SmartObject_AnalogSinkPrx observer;

DHT sensor(14, AM2301);
volatile bool send_readings = false;
Ticker reader;

bool is_null_proxy(Ice_ObjectPrxPtr prx) {
    return ((ObjectPtr)prx)->tag != TAG_OK;
}

void check_wifi_connected() {
    for (byte c=0; WiFi.status() != WL_CONNECTED; c++) {
    	Serial.print(".");

    	if (c > 60) {
    	    Serial.println("\nWiFi: connection failed! Rebooting...");
    	    delay(1000);
    	    ESP.restart();
    	}
    	delay(500);
    }
}

void setup_wireless() {
    Serial.print("WiFi: connecting to " WIFI_SSID);
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.enableAP(false);

    WiFi.begin(WIFI_SSID, WIFI_KEY);
    check_wifi_connected();

    Serial.print("\nWiFi: ready, IP address: ");
    Serial.println(WiFi.localIP());
}

void setup_ota() {
    ArduinoOTA.onStart([]() {
        Serial.println("OTA: start");
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("OTA: progress: %u%%\r", (progress / (total / 100)));
    });

    ArduinoOTA.onEnd([]() {
        Serial.println("\nOTA: end");
    });

    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("OTA error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });

    ArduinoOTA.begin();
}

float read_temperature() {
    float temp = sensor.readTemperature();
    if (isnan(temp)) {
        Serial.println("Sensor: ERROR reading value!");
    }

    return temp;
}

void send_temperature_to_observer(float temp) {
    if (is_null_proxy(&observer))
        return;

    // identifier of this event source
    Ice_String source;
    Ice_String_init(source, "node");

    // metadata information
    SmartObject_Metadata meta;
    SmartObject_Metadata_init(meta, NULL, 0);

    Serial.println("Send temperature to observer");
    SmartObject_AnalogSink_notify(&observer, temp, source, meta);
}

void SmartObject_ObservableI_setObserver(
        SmartObject_ObservablePtr self,
	Ice_String observer_prx) {

    // must add \0 to the end
    char strprx[observer_prx.size + 1];
    memcpy(strprx, observer_prx.value, observer_prx.size);
    strprx[observer_prx.size] = 0;

    Ice_Communicator_stringToProxy(&ic, strprx, &observer);
    Serial.printf("Set observer to '%s'\n", strprx);
}

void setup() {
    Serial.begin(115200);
    Serial.flush();
    delay(100);

    Serial.println("\n\n------\nSystem: booting...");
    setup_wireless();
    setup_ota();

    /* init and setup communicator */
    Ice_initialize(&ic);
    TCPEndpoint_init(&ic);

    /* create object adapter */
    Ice_Communicator_createObjectAdapterWithEndpoints
	(&ic, "Adapter", "tcp -p 7894", &adapter);
    Ice_ObjectAdapter_activate(&adapter);

    /* register servant on adapter */
    SmartObject_Observable_init(&servant);
    Ice_ObjectAdapter_add
        (&adapter, (Ice_ObjectPtr)&servant, "Temp");

    /* read sensor and send to observer periodically */
    sensor.begin();
    reader.attach(10, []() {
	    send_readings = true;
	});

    Serial.println("System: boot done!");
}

void loop() {
    if (send_readings) {
        send_readings = false;
        float temp = read_temperature();
        send_temperature_to_observer(temp);
    }

    Ice_Communicator_loopIteration(&ic);
    ArduinoOTA.handle();
}
