#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("slice/iot.ice")
import SmartObject


class SetObserver(Ice.Application):
    def run(self, args):
        if len(args) < 3:
            print("Usage: {} <subject_proxy> <observer_proxy>".format(args[0]))
            return 1

        subject = self.get_subject_proxy(args[1])
        observer = self.filter_proxy(args[2])
        subject.setObserver(observer)
        print("+'{}'\n+------> '{}'".format(subject, observer))

    def get_subject_proxy(self, strprx):
        ic = self.communicator()
        proxy = ic.stringToProxy(strprx)
        proxy = proxy.ice_encodingVersion(Ice.Encoding_1_0)
        return SmartObject.ObservablePrx.uncheckedCast(proxy)

    def filter_proxy(self, strprx):
        ic = self.communicator()
        prx = ic.stringToProxy(strprx)

        # remove unaccesible hosts, set v1.0, set oneway
        print(prx)
        for e in prx.ice_getEndpoints():
            if e.getInfo().host.startswith("192.168.8."):
                prx = prx.ice_encodingVersion(Ice.Encoding_1_0)
                prx = prx.ice_oneway()
                return ic.proxyToString(prx.ice_endpoints([e]))
        raise RuntimeError("Not connected to proper network")


if __name__ == "__main__":
    SetObserver().main(sys.argv)
