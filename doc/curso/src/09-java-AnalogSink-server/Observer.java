import java.util.Map;
import SmartObject.MetadataField;

// -*- mode: java; coding: utf-8 -*-


final class AnalogSinkI extends SmartObject._AnalogSinkDisp {
    public void _notify(float value, String source, Map<MetadataField, String> data, Ice.Current current) {
    	System.out.println("notify called with value: " + value);
    }	
}

public class Observer extends Ice.Application {
    public int run(String[] args) {
	Ice.Communicator ic = communicator();
        Ice.Object servant = new AnalogSinkI();

	Ice.ObjectAdapter adapter =
	    ic.createObjectAdapterWithEndpoints("Adapter", "tcp -p 7894");
	adapter.activate();

	Ice.Identity oid = Ice.Util.stringToIdentity("TempListener");
	Ice.ObjectPrx proxy = adapter.add(servant, oid);

	System.out.println("Proxy: '" + ic.proxyToString(proxy) + "'");
	shutdownOnInterrupt();
	ic.waitForShutdown();

        return 0;
    }

    static public void main(String[] args) {
        Observer app = new Observer();
	app.main("Observer", args);
    }
}
