// -*- mode: web; coding: utf-8 -*-

_ = console.info.bind(console);

var AnalogSinkI = Ice.Class(SmartObject.AnalogSink, {
    notify: function(value, source, meta) {
	$('#gauge').jqxLinearGauge('value', value);
	$('#caption').text(value.toFixed(2) + " ºC");
    }
});

function setup_ice() {
    var ic = Ice.initialize();
    var host = document.location.hostname || "127.0.0.1";
    var server = ic.stringToProxy("TempListener :ws -p 7895 -h " + host);
    var sink = null;
    var adapter = null;

    return ic.createObjectAdapter("").then(on_adapter_ready);

    function on_adapter_ready(adapter_) {
	adapter = adapter_;
	sink = adapter.addWithUUID(new AnalogSinkI());
	return SmartObject.CallbackPrx.checkedCast(server).then(on_server_ready);
    };

    function on_server_ready(server_) {
	var con = server_.ice_getCachedConnection();
	con.setAdapter(adapter);
	return server_.attach(sink.ice_getIdentity());
    };
}

$(document).ready(function () {
    $('#gauge').jqxLinearGauge({
        orientation: 'vertical',
        labels: { interval: 5 },
        ticksMajor: { size: '5%', interval: 10 },
        ticksMinor: {
	    size: '5%', interval: 2.5,
	    style: { 'stroke-width': 1, stroke: '#aaaaaa'}},
        max: 50,
	min: -20,
        pointer: { size: '3%', fill: "#003F66", stroke: "#003F66" },
        colorScheme: 'scheme05',
        ranges: [
            { startValue: -20, endValue: 1, style: { fill: '#B8D8EC', stroke: '#B8D8EC'}},
            { startValue: 1, endValue: 15, style: { fill: '#94EF97', stroke: '#94EF97'}},
            { startValue: 15, endValue: 30, style: { fill: '#EFBD94', stroke: '#EFBD94'}},
	    { startValue: 30, endValue: 50, style: { fill: '#FF9396', stroke: '#FF9396'}}],
	height: '100px',
	width: '450px',
	orientation: 'horizontal',
	background: {visible: false}
    });

    Ice.Promise.try(setup_ice).
	exception(function(ex) {
	    console.error("ERROR: " + ex);
	});
});
