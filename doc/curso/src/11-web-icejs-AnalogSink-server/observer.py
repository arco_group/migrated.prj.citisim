#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("bidir.ice --all -I{}".format(Ice.getSliceDir()))
import SmartObject  # noqa


class AnalogSinkI(SmartObject.AnalogSinkCb):
    def __init__(self):
        self.observers = []

    def notify(self, value, source, meta, current):
        print("notify called with value: {}".format(value))
        for o in self.observers[:]:
            try:
                o.notify(value, source, meta)
            except Exception:
                print("ERROR on notify, disconnect client '{}'".format(
                    o.ice_getIdentity().name))
                self.observers.remove(o)

    def attach(self, observer, current):
        print("attach of '{}'".format(observer.name))
        observer = current.con.createProxy(observer)
        observer = SmartObject.AnalogSinkPrx.uncheckedCast(observer)
        self.observers.append(observer)


class WebServer(Ice.Application):
    def run(self, args):
        ic = self.communicator()
        ic.getProperties().setProperty("Ice.ThreadPool.Server.SizeMax", "10")

        adapter = ic.createObjectAdapterWithEndpoints(
            "Adapter", "tcp -p 7894 : ws -p 7895")
        adapter.activate()

        self.sensor = AnalogSinkI()
        oid = ic.stringToIdentity("TempListener")
        proxy = adapter.add(self.sensor, oid)

        print("Proxy: '{}'".format(proxy))
        self.shutdownOnInterrupt()
        ic.waitForShutdown()
        print("\rBye!")


if __name__ == "__main__":
    WebServer().main(sys.argv)
