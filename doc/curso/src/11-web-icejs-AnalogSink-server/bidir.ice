// -*- mode: c++; coding: utf-8 -*-

#include <Ice/Identity.ice>
#include "iot.ice"

module SmartObject {

    interface Callback {
	void attach(Ice::Identity oid);
    };

    interface AnalogSinkCb extends AnalogSink, Callback {};
};
