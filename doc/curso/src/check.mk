# -*- mode: makefile-gmake; coding: utf-8 -*-

SHELL = /bin/bash
R     = \e[1;31m
C     = \e[0m
G     = \e[1;32m

check:
	@if [ -z "$(NODE_IP)" ]; then echo "Please, define NODE_IP"; else \
	   if ! ping -c 1 -W 1 $(NODE_IP) > /dev/null; then \
	     echo -e "[$(R)ERROR$(C)] Node $(NODE_IP) NOT ready!"; else \
	     echo -e "[$(G)OK$(C)] Everything is fine."; \
           fi \
	fi
