// -*- mode: js; coding: utf-8 -*-

_ = console.info.bind(console);

$(document).ready(function () {
    $('#gauge').jqxLinearGauge({
        orientation: 'vertical',
        labels: { interval: 5 },
        ticksMajor: { size: '5%', interval: 10 },
        ticksMinor: {
	    size: '5%', interval: 2.5,
	    style: { 'stroke-width': 1, stroke: '#aaaaaa'}},
        max: 50,
	min: -20,
        pointer: { size: '3%', fill: "#003F66", stroke: "#003F66" },
        colorScheme: 'scheme05',
        ranges: [
            { startValue: -20, endValue: 1, style: { fill: '#B8D8EC', stroke: '#B8D8EC'}},
            { startValue: 1, endValue: 15, style: { fill: '#94EF97', stroke: '#94EF97'}},
            { startValue: 15, endValue: 30, style: { fill: '#EFBD94', stroke: '#EFBD94'}},
	    { startValue: 30, endValue: 50, style: { fill: '#FF9396', stroke: '#FF9396'}}],
	height: '100px',
	width: '450px',
	orientation: 'horizontal',
	background: {visible: false}
    });

    // listen for incoming updates
    new EventSource("/updates").onmessage = function(event) {
	var data = JSON.parse(event.data);
	$('#gauge').jqxLinearGauge('value', data.temperature);
	$('#caption').text(data.temperature.toFixed(2) + " ºC");
    }
});
