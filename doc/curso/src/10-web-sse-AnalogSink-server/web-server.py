#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice
from threading import Event
from flask import Flask, Response, render_template

Ice.loadSlice("iot.ice")
import SmartObject  # noqa


class AnalogSinkI(SmartObject.AnalogSink):
    def __init__(self):
        self.value = 0
        self.changed = Event()

    def notify(self, value, source, meta, current):
        print("notify called with value: {}".format(value))
        self.value = value
        self.changed.set()
        self.changed.clear()


class WebServer:
    def __init__(self):
        self.setup_ice_observer()
        self.setup_web_server()

    def setup_web_server(self):
        self.app = Flask(__name__, static_folder='static', template_folder='static/html')
        self.should_stop = False

        @self.app.route("/")
        def index():
            return render_template("index.html")

        def wait_for_changes():
            while True:
                if self.should_stop:
                    return
                self.sensor.changed.wait()
                yield "data: {{\"temperature\": {}}}\n\n".format(self.sensor.value)

        @self.app.route("/updates")
        def send_updates():
            return Response(wait_for_changes(), mimetype="text/event-stream")

    def setup_ice_observer(self):
        self.ic = Ice.initialize(sys.argv)

        adapter = self.ic.createObjectAdapterWithEndpoints(
            "Adapter", "tcp -p 7894")
        adapter.activate()

        self.sensor = AnalogSinkI()
        oid = self.ic.stringToIdentity("TempListener")
        proxy = adapter.add(self.sensor, oid)

        print("Proxy: '{}'".format(proxy))

    def run(self):
        self.app.run(threaded=True, port=8888)
        self.on_close()

    def on_close(self):
        self.should_stop = True
        self.sensor.changed.set()
        self.ic.destroy()
        print("\rBye!")


if __name__ == "__main__":
    WebServer().run()
