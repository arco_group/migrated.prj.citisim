#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("iot.ice")
import SmartObject  # noqa


class Client(Ice.Application):
    def run(self, args):
        if len(args) < 3:
            print("Usage: {} <proxy> <0|1>".format(args[0]))
            return 1

        relay = self.get_proxy(args[1])
        state = args[2] == "1"
        relay.notify(state, "", {})

    def get_proxy(self, strprx):
        ic = self.communicator()
        relay = ic.stringToProxy(strprx)
        relay = relay.ice_encodingVersion(Ice.Encoding_1_0)
        return SmartObject.DigitalSinkPrx.uncheckedCast(relay)


if __name__ == "__main__":
    Client().main(sys.argv)
