#!/usr/bin/python3 -u
# -*- mode: python; coding: utf-8 -*-

import sys
import Ice

Ice.loadSlice("slice/iot.ice")
import SmartObject


class DigitalSinkI(SmartObject.DigitalSink):
    def notify(self, value, source, meta, position, current):
        print("notify called with value: {}".format(value))


class Server(Ice.Application):
    def run(self, args):
        ic = self.communicator()

        adapter = ic.createObjectAdapterWithEndpoints(
            "Adapter", "tcp -h 127.0.0.1 -p 7894")
        adapter.activate()

        oid = ic.stringToIdentity("Relay")
        proxy = adapter.add(DigitalSinkI(), oid)

        print("Proxy: '{}'".format(proxy))
        self.shutdownOnInterrupt()
        ic.waitForShutdown()


if __name__ == "__main__":
    Server().main(sys.argv)
