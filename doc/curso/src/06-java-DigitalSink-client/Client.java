// -*- mode: java; coding: utf-8 -*-

import SmartObject.DigitalSinkPrx;
import SmartObject.DigitalSinkPrxHelper;

public class Client extends Ice.Application {
    public int run(String[] args) {
        DigitalSinkPrx relay = get_proxy(args[0]);
        boolean state = "1".equals(args[1]);
        relay._notify(state, "", null);

        return 0;
    }

    private DigitalSinkPrx get_proxy(String strprx) {
        Ice.Communicator ic = communicator();
        Ice.ObjectPrx proxy = ic.stringToProxy(strprx);
        proxy = proxy.ice_encodingVersion(Ice.Util.Encoding_1_0);
        return DigitalSinkPrxHelper.uncheckedCast(proxy);
    }

    static public void main(String[] args) {
        if (args.length < 2 ||
            args[0].isEmpty() ||
            args[1].isEmpty()) {

            System.out.println("Usage: client <proxy> <state>");
            return;
        }

        Client app = new Client();
        app.main("Client", args);
    }
}
