// -*- mode: js; coding: utf-8 -*-

$(document).ready(function() {

    var status = false;

    function on_connect() {
	var strprx = $('#proxy').val();
	relay = ic.stringToProxy(strprx);
	relay = SmartObject.DigitalSinkPrx.uncheckedCast(relay);
    };

    function on_error(err) {
	console.error("ERROR: " + err);
    };

    function on_toggle_relay() {
	if (!relay)
	    return on_error("relay is not started!");

	relay.notify(status, "", null).exception(on_error);
	status = !status;
    };

    ic = Ice.initialize();
    $('.button').on("click", on_toggle_relay);
    $('#proxy').on("change", on_connect);
    on_connect();
});
