%% -*- mode: latex; coding: utf-8 -*-

\documentclass{beamer}

\graphicspath{{images/}}
\usepackage{import}
\import{}{common}

%% ----------------------------------------------------------------------

\title[AnalogSink Web Observer (SSE)]
      {\emph{AnalogSink} observer as a \textbf{WebApp}}
\subtitle{Flask + SSE + JavaScript}
\author[Arco Group (UCLM)]{}
\date[Ice 2017]{ZeroC Ice Course, 2017}

\begin{document}
\logo{\includegraphics[width=0.25\paperwidth]{arco_watermark}}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame} \frametitle{Our Goal}
  \center \Large{A \textbf{Web Application} that will \emph{receive}
    \textbf{\alert{temperature}} events from a \textbf{remote}
    sensor. \\

    \vspace{0.6cm}
    Browser notifications through \textbf{\alert{SSE}}
    (\emph{Server Sent Events}).} \endcenter
\end{frame}

\section{Slice}
\begin{frame}[fragile] \frametitle{Slice Interface}
  Our web application \textbf{must implement} the same interface that
  our \textbf{sensor} node \textbf{uses}, the \alert{SmartObject.AnalogSink}:

  \begin{cppCode}
interface AnalogSink {
  void notify(float value, string source, Metadata meta);
};
  \end{cppCode}
  \center \tiny{(this interface is defined on iot.ice)} \endcenter
\end{frame}

\section{Observer}
\begin{frame} \frametitle{AnalogSink Web Observer}

  \begin{minipage}[c]{0.65\textwidth}
    \begin{itemize}
    \item Python Server:
      \begin{itemize}
      \item ZeroC Ice \textbf{Observer}
      \item Web Server (Python \textbf{Flask})
      \end{itemize}

    \vspace{0.3cm}
    \item HTML:
      \begin{itemize}
      \item \textbf{Temperature} view element
      \item Load of needed \textbf{JavaScript} files
      \end{itemize}

    \vspace{0.3cm}
    \item JavaScript:
      \begin{itemize}
      \item Setup of \textbf{Event Source}
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \begin{minipage}[c]{0.30\textwidth}
    \center
    \includegraphics[width=0.7\textwidth]{html-js-css} \\
    \vspace{0.3cm}
    \includegraphics[width=\textwidth]{python-logo}
    \endcenter
  \end{minipage}

  \vspace{0.3cm}
  \pause
  \begin{itemize}
  \item Code ready on repo:
    \code{10-web-sse-AnalogSink-server/}
    \code{\{\alert{web-server.py, index.html, app.js}\}}
  \end{itemize}
\end{frame}

\subsection{Python Server}
\begin{frame} \frametitle{web-server.py}
  \center \huge{Let's view the \alert{Web Server} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{ZeroC Ice Observer}
  \begin{itemize}
  \item Our servant \textbf{must} implement the interface \alert{AnalogSink}:
    \begin{pyCode*}{fontsize=\scriptsize}
class AnalogSinkI(SmartObject.AnalogSink):
    def __init__(self):
        self.value = 0
        self.changed = Event()
    \end{pyCode*}

    \pause
    \vspace{0.5cm}
    \textbf{Note}: this server will use \emph{threads}. \code{Event()}
    is used to notify the \textbf{web} server thread that a \emph{new}
    value \textbf{\alert{is ready}}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{ZeroC Ice Observer}
  \begin{itemize}
  \item When a \textbf{new value} is set, store it and \textbf{notify}
    all possible \emph{waiting threads} \alert{asynchronously}:
    \begin{pyCode*}{fontsize=\scriptsize}
def notify(self, value, source, meta, current):
    self.value = value
    self.changed.set()
    self.changed.clear()
    \end{pyCode*}

  \pause
  \item Add servant to \textbf{Object Adapter} (as usual):
    \begin{pyCode*}{fontsize=\scriptsize}
self.sensor = AnalogSinkI()
oid = self.ic.stringToIdentity("TempListener")
proxy = adapter.add(self.sensor, oid)
    \end{pyCode*}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Web Server (Python Flask)}

  \begin{minipage}[c]{0.65\textwidth}
    \begin{itemize}
    \item Python \colorhref{http://flask.pocoo.org/}{Flask} is an
      \emph{extensible} micro \textbf{web framework}, used to create web applications in
      an easy manner
    \vspace{0.2cm}
    \item Supports \textbf{Jinja2} templating system
    \vspace{0.2cm}
    \item Is \textbf{100\% WSGI} 1.0 compliant
    \vspace{0.2cm}
    \item Big users like \textbf{Pinterest} or \textbf{LinkedIn}
    \end{itemize}
  \end{minipage}
  \begin{minipage}[c]{0.30\textwidth}
    \includegraphics[width=\textwidth]{flask-logo}
  \end{minipage}

  \pause
  \vspace{0.7cm}
  \begin{itemize}
  \item Has \textbf{native} support for \textbf{\alert{SSE}} (Server Sent Events)
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Web Server (Python Flask)}
  \begin{itemize}
  \item Create \textbf{Flask app}:
    \begin{pyCode}
def setup_web_server(self):
    self.app = Flask(__name__, static_folder='static',
                     template_folder='static/html')
    \end{pyCode}

  \pause
  \item Setup \textbf{handler} for base URL \alert{'/'} (just return \code{index.html}):
    \begin{pyCode}
@self.app.route("/")
def index():
    return render_template("index.html")
    \end{pyCode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Web Server (Python Flask)}
  Handler for \textbf{\alert{SSE}} resource:
  \begin{pyCode*}{fontsize=\scriptsize}
def wait_for_changes():
    while True:
        if self.should_stop:
            return
        self.sensor.changed.wait()
        yield "data: {{\"temperature\": {}}}\n\n".format(
               self.sensor.value)
  \end{pyCode*}

  \pause
  \textbf{Notes:}
  \begin{itemize}
  \item This function will \textbf{process} each client requests for
    \textbf{\alert{new data}}
  \pause
  \item It will \textbf{run} until client \emph{disconnects} (or server's shutdown)
  \pause
  \item It waits for \code{Event} to \textbf{be set}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Web Server (Python Flask)}
  Setup handler:
  \begin{pyCode}
@self.app.route("/updates")
def send_updates():
    return Response(wait_for_changes(),
                    mimetype="text/event-stream")
  \end{pyCode}

  \pause
  \textbf{Notes}:
  \begin{itemize}
  \item \code{\alert{/updates}} resource will return a \code{Response} object
  \pause
  \item Created with a Python \textbf{generator}, result of \code{wait\_for\_changes()}
  \pause
  \item Response \textbf{type} must be \code{text/event-stream}
  \end{itemize}
\end{frame}

\subsection{HTML}
\begin{frame} \frametitle{index.html}
  \center \huge{Let's view the \alert{HTML} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Temperature View Element}
  Just a \textbf{simple} \code{<div>} element:
  \begin{htmlCode}
<div class="center">
  <div id="gauge" style="margin-left: 20px; float: left;">
</div></div>
<div id="caption" class="center">- ºC</div>
  \end{htmlCode}

  \pause
  \vspace{0.5cm} \center There is \textbf{no need}, but we will use
  JQuery UI widgets to \\ make it look \textbf{better} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Load of needed JavaScript files}
  This time, \textbf{no} Ice is \textbf{needed}, just our JS app (and JQuery UI):
  \begin{htmlCode}
<script src="static/js/jquery.js"></script>
<script src="static/js/jqxcore.js"></script>
<script src="static/js/jqxdraw.js"></script>
<script src="static/js/jqxgauge.js"></script>

<script src="static/js/app.js"></script>
  \end{htmlCode}
\end{frame}

\subsection{JavaScript}
\begin{frame} \frametitle{app.js}
  \center \huge{Let's view the \alert{JavaScript} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Setup of Event Source}
  Standard JS \code{EventSource} creation:
  \begin{jsCode}
new EventSource("/updates").onmessage = function(event) {
   var data = JSON.parse(event.data);
   $('#gauge').jqxLinearGauge('value', data.temperature);
   $('#caption').text(data.temperature.toFixed(2) + " ºC");
}
  \end{jsCode}

  \pause
  When a \textbf{new value} arrives:
  \begin{itemize}
  \item Parse the \textbf{\alert{JSON}} object
  \item \textbf{Update} the HTML view (\code{gauge} and \code{caption})
  \end{itemize}
\end{frame}

\section{Running}
\begin{frame} \frametitle{Testing Time}
  \center \huge{Let's try it!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Testing Time}
  Run Web \textbf{Server}:
  \begin{terminal*}{fontsize=\scriptsize}
$ make run-webserver
  \end{terminal*}

  \pause
  \textbf{\alert{Connect}} observer to \textbf{remote sensor} node:
  \begin{terminal*}{fontsize=\scriptsize}
$ make connect
  \end{terminal*}

  \pause
  Open WebApp on \textbf{browser}:
  \begin{terminal*}{fontsize=\scriptsize}
$ make run-browser
  \end{terminal*}

  \textbf{Note:} \code{make run-browser} will use
  \alert{chromium-browser}. But, you can just \textbf{open}
  \colorhref{http://127.0.0.1:8888/}{http://127.0.0.1:8888} using another one.

\end{frame}


\end{document}
