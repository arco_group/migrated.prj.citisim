%% -*- mode: latex; coding: utf-8 -*-

\documentclass{beamer}

\graphicspath{{images/}}
\usepackage{import}
\import{}{common}

%% ----------------------------------------------------------------------

\title[AnalogSink Client Node]
      {\textbf{Sensor} using \emph{AnalogSink} in a hardware node}
\subtitle{ESP8266 + IceC}
\author[Arco Group (UCLM)]{}
\date[Ice 2017]{ZeroC Ice Course, 2017}

\begin{document}
\logo{\includegraphics[width=0.25\paperwidth]{arco_watermark}}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame} \frametitle{Our Goal}
  \center \Large{A \textbf{hardware} node which reads the room
    \alert{temperature} (with its own sensor), and sends it to some
    \emph{remote}, and configurable, \textbf{observer}.} \endcenter
\end{frame}

\section{Slice}
\begin{frame}[fragile] \frametitle{Slice Interface}
  The TH10 will \textbf{use} the \alert{SmartObject.AnalogSink} interface:

  \begin{cppCode}
interface AnalogSink {
  void notify(float value, string source, Metadata meta);
};
  \end{cppCode}
  \center \tiny{(this interface is defined on iot.ice)} \endcenter
\end{frame}

\begin{frame} \frametitle{Slice Interface}
  \center \Huge{and...} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Slice Interface}
  ...also, the TH10 will \textbf{implement} the \alert{SmartObject.Observable} interface:

  \begin{cppCode}
interface Observable {
  void setObserver(string observer);
};
  \end{cppCode}
  \center \tiny{(this interface is defined on iot.ice)} \endcenter
\end{frame}

\section{Node}
\begin{frame} \frametitle{Temperature Publisher node}
  \begin{itemize}
  \item Initialization (on node \textbf{boot})
  \item Periodic Events (on \textbf{runtime})
  \item Event Loop (on \textbf{runtime})
  \item Observable Servant Implementation (on \textbf{demand})
  \end{itemize}

  \pause
  \begin{itemize}
  \item Code ready on repo:
    \code{08-esp-icec-AnalogSink-client/src/\alert{node.cpp}}
  \end{itemize}
\end{frame}

\begin{frame} \frametitle{node.cpp}
  \center \huge{Show me the code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{1) Initialization}
  \begin{itemize}
  \item Global declaration of \textbf{broker}, \textbf{adapter},
    \alert{Observable}'s \textbf{servant} and \textbf{observer}'s
    proxy:
    \begin{cppCode}
Ice_Communicator ic;
Ice_ObjectAdapter adapter;
SmartObject_Observable servant;
SmartObject_AnalogSinkPrx observer;
    \end{cppCode}

  \pause
  \item Initialization of IceC \textbf{Communicator} and \textbf{TCP} Endpoint:
    \begin{cppCode}
Ice_initialize(&ic);
TCPEndpoint_init(&ic);
    \end{cppCode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{1) Initialization}
  \begin{itemize}
  \item Setup of \textbf{Object Adapter}:
    \begin{cppCode}
Ice_Communicator_createObjectAdapterWithEndpoints
    (&ic, "Adapter", "tcp -p 7894", &adapter);
Ice_ObjectAdapter_activate(&adapter);
    \end{cppCode}

  \pause
  \item \alert{Observable}'s \textbf{servant} registration on adapter:
    \begin{cppCode}
SmartObject_Observable_init(&servant);
Ice_ObjectAdapter_add
    (&adapter, (Ice_ObjectPtr)&servant, "Temp");
    \end{cppCode}
  \end{itemize}

  \pause
  \textbf{Remember}, port: \textbf{\alert{7894}}, identity: \code{\textbf{\alert{Temp}}}
\end{frame}

\begin{frame}[fragile] \frametitle{2) Periodic Events}
  The node will send it's \textbf{temperature} periodically. We need
  to setup a \textbf{timer}:

  \pause
  \begin{itemize}
  \item Declare it:
    \begin{cppCode}
volatile bool send_readings = false;
Ticker reader;
    \end{cppCode}

  \pause
  \item Setup to \textbf{send} readings each \textbf{\alert{10}} seconds:
    \begin{cppCode}
reader.attach(10, []() {
    send_readings = true;
});
    \end{cppCode}
  \end{itemize}

\end{frame}

\begin{frame}[fragile] \frametitle{3) Event Loop}
  It will be called \textbf{continuously}. Has \textbf{\alert{two}} tasks:

  \pause
  \begin{itemize}
  \item Actually \textbf{send} the temperature to observer:
    \begin{cppCode}
if (send_readings) {
    send_readings = false;
    float temp = read_temperature();
    send_temperature_to_observer(temp);
}
    \end{cppCode}

  \pause
  \item Process \textbf{incoming events} from broker:
    \begin{cppCode}
Ice_Communicator_loopIteration(&ic);
    \end{cppCode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{3) Event Loop}
  \center \huge{and \textbf{how} the temperature is sent?} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{3) Event Loop}
  \begin{itemize}
  \item First, check if \textbf{observer} is set:
  \begin{cppCode*}{fontsize=\scriptsize}
if (is_null_proxy(&observer)) return;
  \end{cppCode*}

  \pause
  \item Then, construct the invocation params:
  \begin{cppCode*}{fontsize=\scriptsize}
  Ice_String source; Ice_String_init(source, "node");
  SmartObject_Metadata meta;
  SmartObject_Metadata_init(meta, NULL, 0);
  \end{cppCode*}

  \pause
  \item Finally, \textbf{send} it to observer:
    \begin{cppCode*}{fontsize=\scriptsize}
SmartObject_AnalogSink_notify(&observer, temp, source, meta);
    \end{cppCode*}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{4) Servant Implementation}
  \code{setObserver()} is used to specify the \textbf{proxy} of a
  remote object, which \textbf{\alert{MUST}} implement the interface
  \alert{SmartObject.AnalogSink}.

  \pause
  \begin{cppCode}
void SmartObject_ObservableI_setObserver(
        SmartObject_ObservablePtr self,
	Ice_String observer_prx) {

    // must add \0 to the end
    char strprx[observer_prx.size + 1];
    memcpy(strprx, observer_prx.value, observer_prx.size);
    strprx[observer_prx.size] = 0;

    Ice_Communicator_stringToProxy(&ic, strprx, &observer);
}
  \end{cppCode}
\end{frame}

\section{Running}
\begin{frame} \frametitle{Testing Time}
  \center \huge{Let's try it!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Testing Time}
  Run our \textbf{observer}, which will receive the temperature (as \alert{push} events):
  \begin{terminal}
$ make run-observer
  \end{terminal}

  \pause
  Connect \textbf{observer} to \textbf{node}:
  \begin{terminal}
$ make connect
  \end{terminal}

  \pause
  \center \Large{Check \textbf{observer's} output!} \endcenter
\end{frame}

\end{document}
