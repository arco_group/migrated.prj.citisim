%% -*- mode: latex; coding: utf-8 -*-

\documentclass{beamer}

\graphicspath{{images/}}
\usepackage{import}
\import{}{common}

%% ----------------------------------------------------------------------

\title[AnalogSink Web Observer (IceJS)]
      {\emph{AnalogSink} observer as a \textbf{WebApp}}
\subtitle{ZeroC Ice + JavaScript}
\author[Arco Group (UCLM)]{}
\date[Ice 2017]{ZeroC Ice Course, 2017}

\begin{document}
\logo{\includegraphics[width=0.25\paperwidth]{arco_watermark}}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame} \frametitle{Our Goal}
  \center \Large{A \textbf{Web Application} that will \emph{receive}
    \textbf{\alert{temperature}} events from a \textbf{remote}
    sensor. \\
    \vspace{0.6cm}
    Browser notifications using ZeroC \textbf{\alert{Ice}} for JavaScript.} \endcenter
\end{frame}

\section{Slice}
\begin{frame}[fragile] \frametitle{Slice Interface}
  This example is \textbf{a bit} different:

  \pause
  \vspace{0.3cm}
  \begin{itemize}
  \item It will use a \textbf{\alert{bi-dir}} proxy
  \pause
  \item There will be \textbf{two objects} implementing \alert{AnalogSink}:
    \begin{itemize}
    \item one in a \textbf{Python observer}, listening for \emph{TCP} conections
    \item one in our \textbf{Browser}, receiving invocations through \emph{WS}
    \end{itemize}
  \pause
  \item Browser's object will \textbf{register} itself on Python observer
  \end{itemize}

  \vspace{0.3cm}
  \pause \center \large{To achieve this, we need to define a
    \textbf{new Slice} interface: \code{\alert{bidir.ice}}} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{bidir.ice}
  Actually, there are \textbf{\alert{two}} interfaces. First one is used to
  \textbf{register} Browser's object on \textbf{observer}:
  \begin{cppCode}
interface Callback {
    void attach(Ice::Identity oid);
};
  \end{cppCode}

  \pause The \textbf{second} one is a \emph{mixin} of the above and
  \alert{AnalogSink}:
  \begin{cppCode}
interface AnalogSinkCb extends AnalogSink, Callback {};
  \end{cppCode}
\end{frame}

\section{Observer}
\begin{frame} \frametitle{AnalogSink Web Observer}

  \begin{minipage}[c]{0.65\textwidth}
    \begin{itemize}
    \item Python Observer

    \vspace{0.3cm}
    \item HTML:
      \begin{itemize}
      \item \textbf{Temperature} view element
      \item Load of needed \textbf{JavaScript} files
      \end{itemize}

    \vspace{0.3cm}
    \item JavaScript:
      \begin{itemize}
      \item Ice \textbf{initialization}
      \item Object Adapter and \textbf{servant} registration
      \item \textbf{Observer} attachment
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \begin{minipage}[c]{0.30\textwidth}
    \center
    \includegraphics[width=0.7\textwidth]{html-js-css} \\
    \vspace{0.3cm}
    \includegraphics[width=\textwidth]{python-logo}
    \endcenter
  \end{minipage}

  \vspace{0.3cm}
  \pause
  \begin{itemize}
  \item Code ready on repo:
    \code{11-web-icejs-AnalogSink-server/}
    \code{\{\alert{observer.py, index.html, app.js}\}}
  \end{itemize}
\end{frame}

\subsection{Python Server}
\begin{frame} \frametitle{observer.py}
  \center \huge{Let's view the \alert{Observer} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{ZeroC Ice Observer}
  Implements \code{attach()} method, to register new web clients:
  \begin{pyCode*}{fontsize=\scriptsize}
def attach(self, observer, current):
    observer = current.con.createProxy(observer)
    observer = SmartObject.AnalogSinkPrx.uncheckedCast(observer)
    self.observers.append(observer)
  \end{pyCode*}

  \pause
  \textbf{Notes:}
  \begin{itemize}
  \item Create \textbf{\alert{bidir}} proxy, using web \textbf{observer id}
  \item Append it to observer \textbf{list}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{ZeroC Ice Observer}
  On a new \code{notify{}} invocation, call the \textbf{same method} on web observers:
  \begin{pyCode*}{fontsize=\scriptsize}
def notify(self, value, source, meta, position, current):
    for o in self.observers[:]:
        try:
            o.notify(value, source, meta, position)
        except Exception:
            print("ERROR on notify, disconnect client")
            self.observers.remove(o)
  \end{pyCode*}

  \pause
  Also, if could \textbf{not} reach an observer, \textbf{remove} it from list.
\end{frame}

\begin{frame}[fragile] \frametitle{ZeroC Ice Observer}
  Add servant to \textbf{Object Adapter} (as usual):

  \pause
  \begin{pyCode*}{fontsize=\scriptsize}
adapter = ic.createObjectAdapterWithEndpoints(
    "Adapter", "tcp -p 7894 : ws -p 7895")
adapter.activate()

self.sensor = AnalogSinkI()
oid = ic.stringToIdentity("TempListener")
proxy = adapter.add(self.sensor, oid)
  \end{pyCode*}

  \pause
  \textbf{Note} that the \emph{Object Adapter} has two
  endpoints (\textbf{\alert{TCP}} and \textbf{\alert{WS}}), because it
  \textbf{should} be reachable by Sensor Node and by Web Clients.
\end{frame}

\begin{frame} \frametitle{Web Server}
  \center \Large{Again, there is \textbf{no} web server, this example
    could \\ run as \textbf{static} files only} \endcenter
\end{frame}

\subsection{HTML}
\begin{frame} \frametitle{index.html}
  \center \huge{Let's view the \alert{HTML} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Temperature View Element}
  The \textbf{same} code as previous example, just a \textbf{simple} \code{<div>} element:
  \begin{htmlCode}
<div class="center">
  <div id="gauge" style="margin-left: 20px; float: left;">
</div></div>
<div id="caption" class="center">- ºC</div>
  \end{htmlCode}
\end{frame}

\begin{frame}[fragile] \frametitle{Load of needed JavaScript files}
  We need to load \textbf{Ice JS} and the \emph{translated} units:

  \begin{htmlCode*}{fontsize=\scriptsize}
<script src="static/js/Ice.js"></script>
<script src="static/js/iot.js"></script>
<script src="static/js/bidir.js"></script>
<script src="static/js/app.js"></script>
  \end{htmlCode*}

  \pause
  \begin{itemize}
  \item \code{\alert{Ice.js}}: it provides the Ice \textbf{engine}
    \pause
  \item \code{\alert{iot.js}}: \textbf{translated} from the interface (\code{iot.ice})
  \item \code{\alert{bidir.js}}: \textbf{translated} from the interface (\code{bidir.ice})
    \pause
  \item \code{\alert{app.js}}: our web \textbf{application}
  \end{itemize}
\end{frame}

\subsection{JavaScript}
\begin{frame} \frametitle{app.js}
  \center \huge{Let's view the \alert{JavaScript} code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Ice Initialization}
  Create \textbf{Ice broker} and proxy to remote \textbf{\alert{observer}}:
  \begin{jsCode*}{fontsize=\scriptsize}
var ic = Ice.initialize();
var host = document.location.hostname || "127.0.0.1";
var server = ic.stringToProxy("TempListener :ws -p 7895 -h " + host);
var sink = null;
var adapter = null;
  \end{jsCode*}
\end{frame}

\begin{frame}[fragile] \frametitle{Object Adapter}
  Create \textbf{object adapter} in JS (using \emph{automatic} endpoints):
  \begin{jsCode}
return ic.createObjectAdapter("").then(on_adapter_ready);
  \end{jsCode}

  \vspace{1cm}
  \pause
  \center \Large{When adapter is ready...} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Servant Registration}
  Add a \textbf{servant} of \alert{AnalogSink}:
  \begin{jsCode*}{fontsize=\scriptsize}
function on_adapter_ready(adapter_) {
    adapter = adapter_;
    sink = adapter.addWithUUID(new AnalogSinkI());
    return SmartObject.CallbackPrx.checkedCast(server).
        then(on_server_ready);
};
  \end{jsCode*}

  \pause Also, make a \textbf{\alert{checkedCast}} to remote proxy (which will stablish
  a \textbf{new connection} with the server).

    \pause
  \center \Large{When the connection is ready...} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Observer Attachment}
  Establish the \textbf{\alert{bidir}} mechanism: set as Adapter's \textbf{connection}:
  \begin{jsCode}
function on_server_ready(server_) {
    var con = server_.ice_getCachedConnection();
    con.setAdapter(adapter);
    return server_.attach(sink.ice_getIdentity());
};
  \end{jsCode}

  \pause
  And \textbf{attach} local sink to remote \textbf{observer}.
\end{frame}

\section{Running}
\begin{frame} \frametitle{Testing Time}
  \center \huge{Ready! Let's try it...} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Testing Time}
  Run \textbf{Observer}:
  \begin{terminal*}{fontsize=\scriptsize}
$ make run-observer
  \end{terminal*}

  \pause
  \textbf{\alert{Connect}} observer to \textbf{remote sensor} node:
  \begin{terminal*}{fontsize=\scriptsize}
$ make connect
  \end{terminal*}

  \pause
  Open WebApp on \textbf{browser}:
  \begin{terminal*}{fontsize=\scriptsize}
$ make run-browser
  \end{terminal*}

  \textbf{Note:} \code{make run-browser} will use
  \alert{chromium-browser}. But, you can just \textbf{open}
  \code{index.html} using whatever you prefer.

\end{frame}

\end{document}
