%% -*- mode: latex; coding: utf-8 -*-

\documentclass{beamer}

\graphicspath{{images/}}
\usepackage{import}
\import{}{common}

%% ----------------------------------------------------------------------

\title[DigitalSink Web Client]
      {\emph{DigitalSink} client as a \textbf{WebApp}}
\subtitle{ZeroC Ice + JavaScript}
\author[Arco Group (UCLM)]{}
\date[Ice 2017]{ZeroC Ice Course, 2017}

\begin{document}
\logo{\includegraphics[width=0.25\paperwidth]{arco_watermark}}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame} \frametitle{Our Goal}
  \center \Large{A \textbf{Web Application} that could change the
    \emph{state} (\textbf{\alert{true}} or \textbf{\alert{false}})
    of a remote controlled \textbf{boolean service}.} \endcenter
\end{frame}

\section{IceJS}
\begin{frame} \frametitle{About IceJS}
  Ice for Javascript does \textbf{not} support all standard Ice features:
  \begin{itemize}
  \item Only \textbf{asynchronous} invocations (sync and async dispatch)
  \item Browser: \textbf{no} support for TCP/UDP/SSL (only \textbf{WS/WSS})
  \item No threads, no compression, no plugins...
  \item Full compatibility list:
    \colorhref{https://doc.zeroc.com/display/Ice36/JavaScript+Mapping}{JavaScript Mapping}
  \end{itemize}

  \pause
  \vspace{0.3cm}
  \center \huge{\textbf{but}, it is still very useful!}
\end{frame}

\section{Slice}
\begin{frame}[fragile] \frametitle{Slice Interface}

  We \textbf{must use} the same interface that our server node implements,
  the \alert{SmartObject.DigitalSink}:

  \begin{cppCode}
interface DigitalSink {
  void notify(bool value, string source, Metadata meta);
};
  \end{cppCode}
  \center \tiny{(this interface is defined on iot.ice)} \endcenter
\end{frame}

\section{Client}
\begin{frame} \frametitle{DigitalSink Web Client}

  \begin{minipage}[c]{0.65\textwidth}
    \begin{itemize}
    \item HTML:
      \begin{itemize}
      \item \textbf{Proxy} input element
      \item \textbf{Action} button element
      \item Load of needed \textbf{JavaScript} files
      \end{itemize}

      \vspace{0.5cm}
    \item JavaScript:
      \begin{itemize}
      \item \textbf{Initialization} and \emph{CallBacks} setup
      \item Proxy creation
      \item Remote servant \textbf{invocation}
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \begin{minipage}[c]{0.30\textwidth}
    \includegraphics[width=0.9\textwidth]{html-js-css}
  \end{minipage}

  \vspace{0.5cm}
  \pause
  \begin{itemize}
  \item Code ready on repo:
    \code{07-web-DigitalSink-client/\{\alert{index.html,client.js}\}}
  \end{itemize}
\end{frame}

\subsection{HTML}
\begin{frame} \frametitle{index.html}
  \center \huge{First, let's view the code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{HTML Elements}
  \begin{itemize}
  \item \textbf{Proxy} input:
    \begin{htmlCode}
<input type="text" id="proxy" placeholder="Remote Proxy"
       value="Relay -o:ws -h 127.0.0.1 -p 7894"/>
    \end{htmlCode}

  \item \textbf{Action} button:
    \begin{htmlCode}
<span class="button"></span>
    \end{htmlCode}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Needed JS files}
  Ice JS does not depend on \textbf{third party} libraries:

  \begin{htmlCode}
<script src="static/js/Ice.js"></script>
<script src="iot.js"></script>
<script src="client.js"></script>
  \end{htmlCode}

  \pause
  \begin{itemize}
  \item \code{\alert{Ice.js}}: it provides the Ice \textbf{engine}
    \pause
  \item \code{\alert{iot.js}}: \textbf{translated} from the interface (\code{iot.ice})
    \pause
  \item \code{\alert{client.js}}: our client \textbf{implementation}
  \end{itemize}
\end{frame}

\subsection{JavaScript}
\begin{frame} \frametitle{client.js}
  \center \huge{Again, let's see the code...} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Initialization and CallBacks setup}
  \begin{itemize}
  \item \textbf{Broker} initialization:
    \begin{jsCode}
ic = Ice.initialize();
    \end{jsCode}

  \pause
  \item Handle \textbf{events} of HTML elements:
    \begin{jsCode}
$('.button').on("click", on_toggle_relay);
$('#proxy').on("change", on_connect);
    \end{jsCode}

  \pause
  \item Don't forget \textbf{initial} proxy creation: \code{on\_connect();}
  \end{itemize}
\end{frame}

\begin{frame}[fragile] \frametitle{Proxy Creation}
  \begin{itemize}
  \item \code{on\_connect()} implementation:
  \end{itemize}
  \begin{jsCode}
function on_connect() {
  var strprx = $('#proxy').val();
  relay = ic.stringToProxy(strprx);
  relay = SmartObject.DigitalSinkPrx.uncheckedCast(relay);
};
  \end{jsCode}

  \pause
  \vspace{0.5cm} \textbf{Note:} casting is \emph{\alert{unchecked}}
  for \textbf{simplicity}. It could be \emph{\alert{checked}}, in
  which case it will be an async call (using a \emph{promise}).
\end{frame}

\begin{frame}[fragile] \frametitle{Remote Servant Invocation}
  \begin{itemize}
  \item \code{on\_toggle\_relay()} implementation:
  \end{itemize}
  \begin{jsCode}
function on_toggle_relay() {
  if (!relay)
    return on_error("relay is not started!");

  relay.notify(status, "", null).exception(on_error);
  status = !status;
};
  \end{jsCode}

  \pause
  \textbf{Important} things to note:

  \pause
  \begin{itemize}
  \item \code{source} and \code{meta} are \textbf{empty}
    \pause
  \item \code{notify()} is \alert{async}, so it returns an
    \colorhref{https://doc.zeroc.com/display/Ice36/JavaScript+Mapping+for+Operations\#JavaScriptMappingforOperations-promise}{Ice.Promise}
  \end{itemize}
\end{frame}

\subsection{Web Server}
\begin{frame} \frametitle{Web Server}
  \center \huge{And... \\Where is the \textbf{server}?} \endcenter
\end{frame}

\begin{frame} \frametitle{\st{Web Server}}
  \center \Large{For this example, there is \textbf{no} need of a web server!}
  \Large{Everything works as \textbf{static} files.}
  \endcenter
\end{frame}

\section{Running}
\begin{frame} \frametitle{Testing Time}
  \center \huge{Let's try it!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{Testing Time}
  Run \alert{Relay}'s \textbf{server}:
  \begin{terminal}
$ make run-server
  \end{terminal}

  \pause
  Open WebApp on \textbf{browser}:
  \begin{terminal}
$ make run-browser
  \end{terminal}

  \vspace{0.5cm} \textbf{Note:} \code{make run-browser} will use
  \alert{chromium-browser}. But, you can just \textbf{open}
  \code{index.html} using whichever one.
\end{frame}


\end{document}
