%% -*- mode: latex; coding: utf-8 -*-

\documentclass{beamer}

\graphicspath{{images/}}
\usepackage{import}
\import{}{common}

%% ----------------------------------------------------------------------

\title[DigitalSink Server Node]
      {\textbf{Implementation} of \emph{DigitalSink} in a hardware node}
\subtitle{ESP8266 + IceC}
\author[Arco Group (UCLM)]{}
\date[Ice 2017]{ZeroC Ice Course, 2017}

\begin{document}
\logo{\includegraphics[width=0.25\paperwidth]{arco_watermark}}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame} \frametitle{Our Goal}
  \center \Large{A \textbf{hardware} node that controls some \alert{AC load}
    (through a relay), which state could be set from other \emph{remote}
    \textbf{clients}.} \endcenter
\end{frame}

\section{Hardware}
\begin{frame} \frametitle{Brief description of HW platform}

  \begin{minipage}[c]{0.55\textwidth}
    \begin{itemize}
    \item ESP8266
      \begin{itemize}
      \item 32 bits SoC, \textbf{80 MHz}
      \item \textbf{1 MiB} of Flash, 96 KiB of RAM
      \item 16 GPIO pins, I2C, UART, ADC...
      \item 802.11 b/g/n \textbf{Wi-Fi} + WPA2
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}[c]{0.35\textwidth}
    \includegraphics[width=0.5\textwidth]{esp8266-12}
  \end{minipage}

  \pause
  \vspace{0.3cm}
  \begin{minipage}[c]{0.55\textwidth}
    \begin{itemize}
    \item Itead SonOff TH10
      \begin{itemize}
      \item ESP8266 + Power + Relay
      \item \textbf{Temperature} + Humidity sensors
      \end{itemize}
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}[c]{0.35\textwidth}
    \includegraphics[width=0.5\textwidth]{sonoff-sensor}
  \end{minipage}
\end{frame}


\section{IceC}
\begin{frame} \frametitle{ZeroC Ice on 1 MiB of Flash?}
  \pause

  \begin{itemize}
  \item \textbf{Hardly!}
  \item ESP8266: \textbf{no} underlaying OS
  \item Even ZeroC Ice-E (Ice for Embedded systems) needs GNU/Linux
  \end{itemize}
  \pause

  \vspace{0.3cm}
  \center \huge{so, what?} \endcenter
\end{frame}

\begin{frame} \frametitle{IceC: Ice for Constrained devices}
  \begin{itemize}
  \item A (not so) small \textbf{subset} of features of ZeroC Ice
  \item Compatible with IceP v1.0 encoding
  \item Features: basic types, structs, sequences, dictionaries,
    default servants, \textbf{dynamic invocation}, oneway/twoway/datagram...
  \item Endpoints: serial, FIFO, RS-485, \textbf{ZigBee}, nRF24, TCP/UDP (Ethernet, WiFI)

  \vspace{0.5cm}
  \pause
  \item Supports: 8 bit AVR (Arduino), \textbf{ESP8266}, Zynq A9, x86
  \end{itemize}
\end{frame}

\section{Slice}
\begin{frame}[fragile] \frametitle{Slice Interface}

  The TH10 will \textbf{implement} the \alert{SmartObject.DigitalSink} interface:

  \begin{cppCode}
interface DigitalSink {
  void notify(bool value, string source, Metadata meta);
};
  \end{cppCode}
  \center \tiny{(this interface is defined on iot.ice)} \endcenter
\end{frame}

\section{Server}
\begin{frame} \frametitle{Example DigitalSink server}
  \begin{itemize}
  \item Initialization (on node \textbf{boot})
  \item Event Loop (on \textbf{runtime})
  \item Servant Implementation (on \textbf{demand})
  \end{itemize}
  \pause

  \begin{itemize}
  \item Code ready on repo:
    \code{05-esp-icec-DigitalSink-server/src/\alert{server.cpp}}
  \end{itemize}

\end{frame}

\begin{frame} \frametitle{server.cpp}
  \center \huge{Show me the code!} \endcenter
\end{frame}

\begin{frame}[fragile] \frametitle{1) Initialization}
  \begin{itemize}
  \item Global declaration of \textbf{broker}, \textbf{adapter} and \textbf{servant}:
    \begin{cppCode}
Ice_Communicator ic;
Ice_ObjectAdapter adapter;
SmartObject_DigitalSink relay;
    \end{cppCode}

  \pause
  \item Initialization of IceC \textbf{Communicator} and \textbf{TCP} Endpoint:
    \begin{cppCode}
Ice_initialize(&ic);
TCPEndpoint_init(&ic);
    \end{cppCode}
  \end{itemize}

\end{frame}

\begin{frame}[fragile] \frametitle{1) Initialization}
  \begin{itemize}
  \item Setup of \textbf{Object Adapter}:
    \begin{cppCode}
Ice_Communicator_createObjectAdapterWithEndpoints
    (&ic, "Adapter", "tcp -p 7894", &adapter);
Ice_ObjectAdapter_activate(&adapter);
    \end{cppCode}

  \pause
  \item \textbf{Servant} registration on adapter:
    \begin{cppCode}
SmartObject_DigitalSink_init(&relay);
Ice_ObjectAdapter_add
    (&adapter, (Ice_ObjectPtr)&relay, "Relay");
    \end{cppCode}
  \end{itemize}

  \pause
  \textbf{Remember}, port: \textbf{\alert{7894}}, identity: \code{\textbf{\alert{Relay}}}

\end{frame}

\begin{frame}[fragile] \frametitle{2) Event Loop}
  It will be called \textbf{continuously}. Just process incoming events:
  \begin{cppCode}
void loop() {
    Ice_Communicator_loopIteration(&ic);
}
  \end{cppCode}
\end{frame}

\begin{frame}[fragile] \frametitle{3) Servant Implementation}
  Out servant \textbf{implements} each method of \alert{DigitalSink}'s interface:
  \begin{cppCode}
void SmartObject_DigitalSinkI_notify(
   SmartObject_DigitalSinkPtr self, Ice_Bool value,
   Ice_String source, SmartObject_Metadata meta) {

     digitalWrite(RELAY_PIN, value ? HIGH : LOW);
}
  \end{cppCode}
\end{frame}

\section{Running}
\begin{frame} \frametitle{Testing Time}
  \center \huge{Let's try it!} \endcenter
\end{frame}

\end{document}
