#!/bin/bash
# -*- mode: sh; coding: utf-8 -*-

if [ $(id -u) -ne "0" ]; then
    echo "Please, run this script as root (with sudo)"
    exit 1
fi

## Add package repositories
echo 'Acquire::Languages "none";' > /etc/apt/apt.conf.d/99translations
echo "deb http://ftp.es.debian.org/debian stretch main contrib non-free" \
    > /etc/apt/sources.list.d/debian-stretch.list
echo "deb http://pike.esi.uclm.es/arco sid main contrib non-free" \
    > /etc/apt/sources.list.d/pike.list

wget -O- http://pike.esi.uclm.es/arco/key.asc | apt-key add -
apt-get update
apt-get install -y --allow-unauthenticated debian-archive-keyring
apt-get update


## Install software dependencies
apt-get install -y \
    python3-zeroc-ice \
    zeroc-ice-compilers \
    libzeroc-ice3.6-java \
    zeroc-ice36 \
    chromium-browser \
    python3-flask \
    jed 


## Some minor user things
U="su $(who | awk '{print $1}') -c"
$U 'gsettings set org.pantheon.terminal.settings follow-last-tab true'
$U 'gsettings set org.pantheon.scratch.settings auto-indent true'
$U 'gsettings set org.pantheon.scratch.settings highlight-current-line true'
$U 'gsettings set org.pantheon.scratch.settings style-scheme "solarized-dark"'

$U "gsettings set org.pantheon.scratch.settings plugins-enabled \
    \"['strip-trailing-save', 'detect-indent', 'brackets-completion', \
       'contractor', 'preserve-indent', 'terminal', 'folder-manager']\""
       
echo "file:///home/user/repos/prj.citisim prj.citisim" >> ~/.config/gtk-3.0/bookmarks
echo "file:///home/user/repos/prj.citisim/doc/curso curso" >> ~/.config/gtk-3.0/bookmarks

 
## A bit of cleaning
rm -f /etc/apt/sources.list.d/debian-stretch.list
apt-get update

