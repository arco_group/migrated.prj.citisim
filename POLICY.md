This is aplicable to any Citisim related repository


Naming rules
------------

- namespaces and libraries identifiers are lowercase. Corresponding filenames are
  also lowercase (using underscore when require serveral words).
- Ice methods are lowerCamelCase.
- interface and class names are UpperCamelCase.
- method and variable names must follow implementation language's guide style.
- Interface specifications are "Services", implementations are "Servers" (including IceGrid ones).
- Textual literal identifiers (like properties and metadata) are lowercase-with-hyphens.
- Topic names are UpperCamelCase.


Transducer setup
----------------

- Transducer node has no knowledge about Citisim infrastructure (expect conectivity related info).
- Transducers implement SmartObject::Observable interface.
- WiringService provides observers to the transducers (topic or not) using SmartObject::Observable.
- Physical transducer setup requires property registration.


Packaging
---------

Each platform service implementation should provide 2 (debian) packages

- Server distribution with suffix "-server"
- Slice files. Same name with suffix "-slice"


Includes
--------
- Include path asume always relative paths to default path.
  Example:

     #include "citisim/iot.ice"


    - So file is installed in: Ice.getSliceDir() + "/citisim/"
    - and compilation flag is: "-I" + Ice.getSliceDir()


Slice interfaces
----------------

- First version of a Slice interface (without related implementation) should be tagged with
  "STATE: proposal" comment
- If there is a proof of concept or example, its state change to "testing".
- When the interface is considered stable, the STATE comment is removed.


Slice Modules
-------------

- Modules and interfaces are UpperCamelCase
- "SmartObject" for transducer (sensor-actuator) low level interfaces
- "SmartServices" for common generic services: Wiring, Cache, Welcome, etc.
- "Citisim" for Citisim specific (non-portable) services


Ice Servers
-----------

- Server adapter names must fit the format "<Service>.Adapter".


Properties
----------

- Permanent (set by hand) properties are stored in prj.citisim/config/properties.json. If
  your service require some properties to exists, set on that file to avoid be lost on
  Property Server reseet. Load and restore of this dump is not automatic.


;; Local Variables:
;; fill-column: 90
;; End:
