#!/usr/bin/python3

import sys
import Ice
import json
from libcitisim import Broker


class GetProps(Ice.Application):
    def run(self, args):
        broker = Broker(ic=self.communicator())
        props = broker.get_property("")

        store = {}
        for oid, ps in props.items():
            prx = ps.get('proxy')
            if prx is None:
                continue

            store[oid] = {"proxy": prx}

        with open("current-proxies.json", "w") as dst:
            dst.write(json.dumps(store))


if __name__ == '__main__':
    GetProps().main(sys.argv)

