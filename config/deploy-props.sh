#!/bin/bash

./get-current-proxies.py --Ice.Config=locator.config
prop-tool --Ice.Config=locator.config -p "PropertyServer" clear ""
prop-tool --Ice.Config=locator.config -p "PropertyServer" set "" properties.json
./set-current-proxies.py --Ice.Config=locator.config