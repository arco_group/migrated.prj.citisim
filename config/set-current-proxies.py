#!/usr/bin/python3

import sys
import json
import Ice
from libcitisim import Broker


class DeployProps(Ice.Application):
    def run(self, args):
        broker = Broker(ic=self.communicator())

        props = {}
        with open("current-proxies.json") as src:
            props = json.loads(src.read())

        for oid, ps in props.items():
            prx = ps.get('proxy')
            broker.set_property(oid + "|proxy", prx)


if __name__ == '__main__':
    DeployProps().main(sys.argv)

