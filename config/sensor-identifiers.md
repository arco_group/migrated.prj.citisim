Sensor Identifiers
==================

This is a list of current source identifiers of sensors. The field
Source ID is the value of the 'source' parameter on every event the
sensor will send. Please note that this list may not be exhaustive.

Source ID is an hexadecimal number, comprised by 8 bytes, that
uniquely identifies the sensor on the whole network. The first 4 bytes
are used to identify a specific group of sensors (like the network
part in an IP address), and the remaining 4 bytes identifies the
specific sensor (like the host part in an IP address).


    | Source ID  |  Sensor Name                         |
    |------------|--------------------------------------|
    | 17510010   |  Wunderground Temp - ITSI            |
    | 17510011   |  Twilight sensor - ITSI              |
    |            |                                      |
    | 17510020   |  waspSA_02 - PluviometerLastDay      |
    | 17510021   |  waspSA_02 - PluviometerCurrent      |
    | 17510022   |  waspSA_02 - PluviometerLastHour     |
    | 17510023   |  waspSA_02 - Battery                 |
    | 17510024   |  waspSA_02 - Anemometer              |
    | 17510025   |  waspSA_02 - WindVane                |
    | 17510026   |  waspSA_02 - Temperature             |
    | 17510027   |  waspSA_02 - Humidity                |
    |            |                                      |
    | 17510028   |  waspSE_08 - CarbonDioxide           |
    | 17510029   |  waspSE_08 - Ozone                   |
    | 17510030   |  waspSE_08 - AirPollutants2          |
    | 17510031   |  waspSE_08 - AirPollutants1          |
    | 17510032   |  waspSE_08 - Battery                 |
