#!/bin/bash

echo "Installing Pike and MongoDB repositories..."
export DEBIAN_FRONTEND=noninteractive
apt update
apt install -y apt-transport-https dirmngr

echo "deb http://pike.esi.uclm.es/arco sid main" > /etc/apt/sources.list.d/pike.list
echo "deb https://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.0.list
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
wget -q -O- http://pike.esi.uclm.es/arco/key.asc | apt-key add -
apt update

echo "Installing Ice 3.7 and libcitisim..."
apt install -q -y \
    python3-zeroc-ice \
    libcitisim

echo "Installing other Debian packages..."
apt install -q -y \
    mongodb \
    curl \
    dirmngr \
    git \
    make \
    build-essential \
    g++ \
    python-pip \
    python3-pip

echo "Installing PIP packages..."
pip3 install pymongo

echo "Setting up Node..."
curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs npm

echo "Configuring NPM..."
npm config set user 0
npm config set unsafe-perm true

echo "Enabling MongoDB service..."
systemctl enable mongodb.service
systemctl start mongodb.service

echo "Provisioning MongoDB..."
mongo citisim --eval 'db.users.insert({"_id" : ObjectId("5c121123a24d21c53ad3e776"), "username" : "jgisbert", "email" : "jorge.gisbert@abalia.com", "password" : "$2a$10$DCqPRaXCKXXHVwwTjHUbSu9DzNxAIOoElgmqXL1r0WyjYcHdv.fiy", "role" : "admin", "__v" : 0})'
mongo citisim --eval 'db.sectors.insert({ "_id" : ObjectId("5c3f054d1e8eff3f717b7aa2"), "name" : "sector1", "altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Sector número 1" })'
mongo citisim --eval 'db.sectors.insert({ "_id" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM", "altitude" : 0, "longitude" : 0, "latitude" : 0, "description" : "UCLM" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c41a73ef425c7a20da8d552"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM1", "Altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Device 1 UCLM", "citisimId" : "0A06175100000034", "type" : "Voltage" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c41a773f425c7a20da8d553"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM2", "Altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Device 2 UCLM", "citisimId" : "0A06175100000039", "type" : "Voltage" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c41a799f425c7a20da8d554"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM3", "Altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Device 3 UCLM", "citisimId" : "0A06175100000044", "type" : "Voltage" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c46ce45f96ea98e22f8aca4"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM1 Active Power", "Altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Device 1 UCLM Active Power", "citisimId" : "0A06175100000033", "type" : "Energy", "topic" : "Power" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c46cea7f96ea98e22f8aca8"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM2 Active Power", "Altitude" : 2, "longitude" : 2, "latitude" : 2, "description" : "Device 2 UCLM Active Power", "citisimId" : "0A06175100000043", "type" : "Energy", "topic" : "Power" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c46ceacf96ea98e22f8aca9"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM1 Power Factor", "Altitude" : 1, "longitude" : 1, "latitude" : 1, "description" : "Device 1 UCLM Power Factor", "citisimId" : "0A06175100000052", "type" : "Energy" })'
mongo citisim --eval 'db.sensordevices.insert({ "_id" : ObjectId("5c46ceb1f96ea98e22f8acaa"), "sector" : ObjectId("5c41a67af425c7a20da8d551"), "name" : "UCLM2 Power Factor", "Altitude" : 2, "longitude" : 2, "latitude" : 2, "description" : "Device 2 UCLM Power Factor", "citisimId" : "0A06175100000047", "type" : "Energy" })'

echo "Cloning 'prj.citisim' repository..."
cd /home/vagrant
mkdir -p .ssh

cat <<- EOF > .ssh/audit-key
-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEA2Z/eXiEjPd3owzSdxPxBdlrwNTkAl/G+bi0Vh9Q2h9+uuTM0
dBf87lyVbQ1PofC88OrwqyoDEB3Ib3Cjr6gyej5/B8uTYDnzRb9L8f3JqDKhEVYB
673iIm6aknzAu2K6pjplpokt92+yMdLHLYpIc8O/4qZJYJAtqCQXF0mgOmOvGCRM
P0qcA1XXyF/Hw1rYIj8Eg9Zntm/0QrWx8f9KL1Ghl88qEGVZL1UbBgFvcG8Yuv/t
RaMfLtlzTAdvnyxUwGEmBoZp0DaBBDC4R4RggfQELKlLduv2QQAl3lAqSzTW9AYq
yZfbEFWtJvqwRA58Tu2YxTk4L7JPCXdXgZuNQwIDAQABAoIBACwU2PsiJTxT5eEc
mFSdcDkkJIHzYO5mNlek5DaWQtF0+rRi7d4SOdaihFYJmzbHR85ZGJTjNqmeDsqb
+1wKauceH6cZkmmsc12OaTjEWoOqdbvIOy1gUhz9YGwJvxR15C7lp097fAch5et3
sYbE+vYQEZ88ZaKcsdLcNJ0v+be5t5YjDPlBoZkP0gBY9krUgPoYmEMjfr4ZG5/y
ZcKzvjum6X91DAcq/ifvaimsNzAwDBZCGKcEKHunFxzrBOV9PwVLNv1i5IsGRhQ6
E5i0ncfP+ynrqVqXzAJJ2d2Dcs1jhmc5Zv0gFi4Z9jptwpp3S3RIVDaYg09rSBmK
8Drd/6ECgYEA/MWT5ay8+uxNKF7IKkLytB8dhT0I8ruAoG3+pazEB25LViOxBBr2
vldOsSa7ymcgoAGCsejOa+Bgve+P684lihaoOLK9TZxmbRmFOip59agRdR0XGhMT
OiszxzSF/Tc3RqzLKb5YVMm3e6PTUjwILivYtkSC4vHAxMNfytko2HMCgYEA3Gdg
/2A6iSULrE3wbTygTOwOQ4T65Knxgw6qBHBmRCKGCBEe3yliPMG9mAmJPTOXtVRW
0bOOayXnyno4V8fOTENHfa2xC2005Pz3vxYwzZ4M2ZPYjmm0xuvbhjpM/sKaqHc2
uAXsmIjWdXaA4n3G5lZh1tFiUBM3nAnAHlX70/ECgYA8/1TUXN3x02o/Lrgw8pzs
+IFAMN1i7fp5PHHx+wIHJQeLDuvdqMAYaKs1trokesR1uEeYtSxy/d96lj/t0H5g
l10a6Y8jFnXlJ/5tbeimbltgHpf/k9DU/a3uCW+4DkYJiRMxzJw2QcNEU+Ff+9qh
gbU3LstDBsHU+ay5H7RqnwKBgQCTTgCVDaJxsLG5iimrSH62inoKBGGO3EoO1okp
CFpBJXXBBHhGnCkNBEuLxqvTnJzDU0fnRYNk92OQftkTSfYGIF0ykRin02grtyp5
qwIZGo3qCRcOL3XkERSaBHMbPcMR3wNSQJtdYu86eGYxefCqV4dyhGSZsg5Tmmyt
mbgCcQKBgQCVxAVJ+FXEJiDcrad5qUQFFp+m5rbtx/2FqIxzGjXpInmOGCrzPV2f
1PcAbeFmfkULZVIg6+PxgRN4Vxo8TCbJuOEyRFZsZMA6rukss9MtcxGZDqgRDvxa
xfeWTRUSqLszMEfnTt83Zi2Ch+6v7L+AZJ0JzR6zzbjtqNM5R+yIeg==
-----END RSA PRIVATE KEY-----
EOF

cat <<-EOF > .ssh/config
Host bitbucket.org
    Hostname		            bitbucket.org
    IdentityFile  		      ~/.ssh/audit-key
    IdentitiesOnly		      yes
    StrictHostKeyChecking 	no
EOF

chown vagrant:vagrant .ssh -R
chmod 600 /home/vagrant/.ssh/audit-key

su vagrant -c "git clone git@bitbucket.org:arco_group/prj.citisim.git"
mv prj.citisim/src/energy-dashboard citisim
rm -rf prj.citisim

echo "Installing service configuration files..."
cp citisim/server/scripts/services/* /etc/systemd/system/

echo "Enable and start services..."
systemctl daemon-reload
systemctl enable powerFactorScript.service
systemctl enable activePowerScript.service
systemctl enable voltageScript.service
systemctl enable citisimDashboard.service
systemctl start powerFactorScript.service
systemctl start activePowerScript.service
systemctl start voltageScript.service

echo "Building project..."
cd /home/vagrant/citisim
npm i -g @angular/cli
npm i
npm audit fix
npm rebuild node-sass
systemctl start citisimDashboard.service

echo "Enabling ssh access..."
adduser --gecos "" --quiet --disabled-password jorgegisbert1
adduser jorgegisbert1 sudo
mkdir /home/jorgegisbert1/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDG2UPnN6H7H2bneZUTW7MTCaZqthRwx/hVuZXwebVPXQwrbdRlpwaPYJq/JJe/kYq2y02Wf3HfSWmt1CrUvpJOnJYNyms3FS/43UulXnsOoT3htcNy992RmYI7KClTTYF4erNGs2kERTBCJUF04Vd8Ez9hA3fv7qwOqLUR8l+QJ6XMlql3NrzQM2NOqMxr8JfS8N7xvAcWzUjD7WoADYWA4/316BnR70f9+ygJQte/XHh2XU1ftfFbqgFYWNWInE1D/IxdMcbM72O1bcBprzr/tO/z/QerkpVIkfsQkVTQoaRTCKXy213PfMZLYsoa7yTXfQ77wgvcyWR07YRIfNaB jorgegisbert1@MacBook-Pro-de-Jorge.local" > authorized_keys
mv authorized_keys /home/jorgegisbert1/.ssh/
chown jorgegisbert1:jorgegisbert1 /home/jorgegisbert1/.ssh -R
chmod 600 /home/jorgegisbert1/.ssh/authorized_keys
echo "jorgegisbert1 ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/jorgegisbert1
chmod 600 /etc/sudoers.d/jorgegisbert1


