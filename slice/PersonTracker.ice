
module KinectData {

  struct KinectPoint {
  	string trakingId;
  	string trakingState;
    float px;
    float py;
    float pz;
  	float ox;
  	float oy;
  	float oz;
  	float ow;
  	double timestamp;
  };

  sequence <KinectPoint> PointSeq;

  interface PersonTracker {
    void notify(PointSeq points, string source);
  };

};
