// -*- mode: c++; coding: utf-8 -*-

// This file contents should be coordinated with CITISIM partners
// acording to project requirements

module citisim {

    module infosystem {

        interface LoudSpeakerManager {
            void sayInPlace(string placeID, string message);
            void sayInSpeaker(string loudSpeakerID, string message);
        };

    };

};