// -*- mode: c++; coding: utf-8 -*-

// This file contents should be coordinated with CITISIM partners
// acording to project requirements

module SmartObject {

    interface Observable {
        void setObserver(string observer);
    };

    enum MetadataField {
        Timestamp,    // Unix time (CET timezone)
        Quality,      // 0:unknown, 1:poor, 255:best
        Expiration,   // Seconds
        Latitude,     // GPS coordinates (ie. 42.361020)
        Longitude,    // GPS coordinates (ie. 0.160056)
        Altitude,     // GPS coordinates (ie. 1852.25)
        Place,	      // Unique human readable place identifier
        Category,     // String, single identifier
        CitizenEmail, // Citizen Report user identifier
        Type,         // Citizen Report type

        // Intended for batching
        // State: Proposal
        Delta,        // Time between measures
        Size          // Bytes of each measure
    };

    dictionary <MetadataField, string> Metadata;
    sequence<byte> ByteSeq;

    // G-Force sensed on each axis (m/s² or Gs)
    struct Acceleration {
        float x;
        float y;
        float z;
    };

    interface DigitalSink {
        void notify(bool value, string source, Metadata meta);
    };

    interface AnalogSink {
        void notify(float value, string source, Metadata meta);
    };

    interface DataSink {
        void notify(ByteSeq data, string source, Metadata meta);
    };

    interface EventSink {
        void notify(string eventName, string source, Metadata meta);
    };

    interface PulseSink {
        void notify(string source, Metadata meta);
    };

    interface AccelerometerSink {
        void notify(Acceleration value, string source, Metadata meta);
    };

    // STATE: Proposal
    interface WelcomeService {
        void hello(string address, string transducerType);
        void helloBidir(string address, string transducerType);
        void bye(string address);
    };

    // STATE: alfa
    // interface AnnounceListener {
    // };
};
