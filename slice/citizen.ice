// -*- mode: c++; coding: utf-8 -*-

#include "iot.ice"

module Citisim {
  module Citizen {
    sequence<string> tagsSeq;

    struct CitizenReport {
      string reportid;
      tagsSeq content;
      string photourl;

      // - proposed 'userid' is "source" parameter
      // - proposed 'date' in meta['Timestamp']
      // - proposed 'point' in meta (Latitude, Longitude, Altitude)
    };

    interface ReportSink{
      void notify(CitizenReport report, string source, SmartObject::Metadata data);
    };
  };
  
  // module CityReport{
  //   interface EventSink{
  //     void notify(EventReport report, string source, SmartObject::Metadata data);
  //   };
  // };
  
};
