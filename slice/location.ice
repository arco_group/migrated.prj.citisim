// -*- mode: c++; coding: utf-8 -*-

module citisim {

  struct GeoCoord {
    float latitude;
    float longitude;
    float elevation;
  };

  struct Point3D {
    float x;
    float y;
    float z;
  };

  struct Orientation {
    float yaw;
    float pitch;
    float roll;
  };

  struct Location {
    GeoCoord origin;
    string placeID;
    Point3D offset;
    Orientation orientation;
  };
};
