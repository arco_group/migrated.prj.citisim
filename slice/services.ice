// -*- mode: c++; coding: utf-8 -*-
// This file contents should be coordinated with CITISIM partners
// acording project requirements


#include "iot.ice"
#include "location.ice"


module SmartObject {

  // All these interfaces are too specific and they will be replaced soon.
  interface SnapshotService extends EventSink, Observable {
    void trigger(byte number, float delta);
  };

  interface ClipService extends EventSink, Observable {
    // Esto no es aplicable en la práctica (Óscar)
    void trigger(byte seconds);
  };

  // Necesitamos aquí estas ifaces si la KB es quien conoce la semántica?
  interface PersonRecognizer extends DataSink, Observable {};
  interface SpeechToText extends DataSink, Observable {};

  interface AuthenticatedCommandService extends Observable {
    void notifyPerson(string personID, Metadata data);
    void notifyCommand(string command, Metadata data);
  };

};


// State: PROPOSAL
module SmartServices {

  sequence<string> StringSeq;
  dictionary<string, string> Status;

  exception UnknownTransducer {};

  struct TransducerStatus {
    string value;
    string timestamp;
  };

  interface TransducerStatusCache {
    TransducerStatus getStatus(string transducerID) throws UnknownTransducer;
  };

  interface LocationService {
    // obtain a list of transducers, given some place ID
    // - returns: list of transducer IDs
    StringSeq listTransducers(string placeID);

    // obtain a list of transducers, given some place ID, filtered by transducer type
    // - returns: list of transducer IDs
    StringSeq listTransducersByType(string placeID, string transducerType);

    void setMeetingPointForPlace(string placeID, string mpName);
    string getMeetingPointForPlace(string placeID);

    // Location related methods
    citisim::Location getLocation(string transducerID);
    void addMeetingPoint(citisim::Location location, string name);
    citisim::Location getMeetingPointLocation(string name);
  };

  enum EmergencyType {HearthQuake, Fire};

  interface EmergencySink {
    void notify(string sourceID, EmergencyType type, string placeID, string timestamp);
  };

  sequence<string> Path;

  interface RoutingService {
      Path getPath(string startPoint, string endPoint);
  };

  interface RoutingSink {
      void notify(Path thePath);
  };

  interface MessageRender {
    void render(string messageID);
    void clear();
  };
};


// State: PROPOSAL
module SmartEnvironment {

  struct PersonMovement {
    string personID;
    string areaOfInterest;
    string type; // INBOUND, OUTBOUND
    string timestamp;
  };

  sequence<PersonMovement> PersonMovementSeq;

  interface PersonMovementMonitor {
    void notify(PersonMovementSeq movements);
  };

  struct OccupancyChange {
    string areaOfInterest;
    int amount;
    string timestamp;
  };

  sequence<OccupancyChange> OccupancySeq;

  sequence<string> PlaceSeq;

  interface OccupancyMonitor {
    void notify(OccupancySeq occupancies);
    PlaceSeq getOccupiedPlaces();
  };

  struct OccupancyRange {
    int min;
    int max;
  };

  interface OccupancyQuery {
    OccupancyRange getOccupancy(string area);
  };
};
