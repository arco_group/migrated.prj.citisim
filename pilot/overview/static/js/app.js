_ = console.info.bind(console);

class Windows {
    constructor() {
        this.wm = new Ventus.WindowManager();
    }

    open(url, title, x=60, y=60, w=330, h=475) {
        var frame = this.createFrame(url).get(0);
        var win = this.wm.createWindow.fromElement(frame, {
            title: title,
            width: w, height: h,
            animations: false,
            stayinspace: true,
            x: x, y: y,
            events: {
                open: function () {
                    frame.style.visibility = "visible";
                },
            },
        })
        win.open();
        return win;
    }

    createFrame(url) {
        var frame = $('<iframe src="' + url + '" class="win"></iframe>');
        $('body').append(frame);
        return frame;
    }
}

winlist = []
$(window).on("load", function () {
    wm = new Windows();
    winlist[0] = wm.open("http://127.0.0.1:4990", 'Alarm Injector', 21, 162, 288, 440);
    winlist[1] = wm.open("http://127.0.0.1:4991", 'Emergency Service', 330, 16, 845, 475);
    winlist[2] = wm.open("http://127.0.0.1:4993", 'Occupancy Service', 330, 507, 845, 475);
    winlist[3] = wm.open("http://127.0.0.1:4994", 'Smart Screen 1', 1200, 16, 385, 307);
    winlist[4] = wm.open("http://127.0.0.1:4995", 'Smart Screen 2', 1495, 135, 385, 307);
    winlist[5] = wm.open("http://127.0.0.1:4996", 'Property GUI', 1200, 460, 580, 330);
    winlist[6] = wm.open("http://127.0.0.1:4997", 'Speaker 1', 1200, 805, 330, 205);
    winlist[7] = wm.open("http://127.0.0.1:4998", 'Speaker 2', 1550, 805, 330, 205);
    winlist[8] = wm.open("http://127.0.0.1:4999", 'Gas Valve', 1385, 85, 300, 321);
    winlist[9] = wm.open("http://127.0.0.1:5000", 'Main Switch', 1385, 540, 300, 321);
    show_desktop(1);
});

isfull = false;
function toggle_fullscreen() {
    if (isfull) {
        window.resizeTo(orig_w, orig_h);
        isfull = false;
    }
    else {
        orig_w = $(window).width();
        orig_h = $(window).height();
        window.resizeTo(window.screen.availWidth * 2, window.screen.availHeight);
        isfull = true;
    }
}

function show_desktop(number) {
    if (number == 1) {
        var other = 2;
        var visible = [true, true, true, true, true, true, true, true, false, false];
    }
    else {
        var other = 1;
        var visible = [true, true, true, false, false, false, false, false, true, true];
    }

    $("#desktop-" + number + " img").attr(
        {"src": "/static/images/desktop-" + number + "-sel.png"});
    $("#desktop-" + other + " img").attr(
            {"src": "/static/images/desktop-" + other + ".png"});

    for (var i in visible) {
        if (visible[i])
            winlist[i].view.show();
        else
            winlist[i].view.hide();
    }
}