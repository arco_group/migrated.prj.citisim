Dudas a resolver:
=================

1.- Opciones:
 - El sensor envía información constante a su canal, usando `AnalogSink`
 - Se configura el sensor con un *threshold*, cuando las lecturas superan el
   límite establecido, se envía una notificación a su canal (usando `EventSink`).
    - Es **necesaria una interfaz** para configurar el *threshold*

    -[REUNION] Este sensor lanza la demo (2ª opción.)

2.- Este servicio entiendo que recibe información de un conjunto de sensores:
 - Cómo se indican los sensores que debe monitorizar?
    - Es necesario indicar el canal donde publica el sensor (quizá a partir del
      transducerID + PS), el tipo (que se puede obtener del PS), una forma de
      determinar la condición de alarma (todo los sensores, varios eventos,
      thresholds,...), y el mensaje específico de alarma que debe emitir

 - Qué interfaz se emplea para indicar el estado de alarma?
 - [REUNION] Este servicio está hueco, genera el evento Earth Quake.

3.- *Alarm* es un canal de eventos, o un servicio?

- [REUNIÓN] Canal de eventos
- El event actual pasa a ser pulse, y se crea un nuevo event que tiene un string, metadata y source

4.- Varias cuestiones:
 - La solicitud de una ruta entre dos puntos no debe pedirse al servicio de
   rutado (`RoutingService`)? Dijimos que sería un mismo servidor, prestando
   dos servicios diferenciados.
    
  [REUNIÓN] es verdad, la ruta se pide al routing service. hay que añadirlo al diagrama de secuencia.
  

 - El servicio de `IndoorGML` proporciona una lista de celdas como ruta entre
   dos puntos, no son coordenadas. Las celdas pueden ser espacios de cualquier
   tamaño, con uno o **varios** puntos de unión entre otras celdas. Esto
   implica:
    - Cualquier visualizador debe interpretar esa información y adaptarla a su modelo
    [REUNIÓN] OK
    - De ser necesario, habría que incluir una interfaz para obtener otros
      puntos (OLC, GPS, etc) a partir de las celdas.

    [REUNIÓN] Es lo que hablamos con el de Prodevelop. Esto lo piensa Óscar.

5.- Entiendo que este servicio se encarga de, a partir de una ruta de evacuación:
 - Visual:
    - Determinar los indicadores visuales configurables en la ruta especificada
      - Quizá una solicitud a `listTransducersByType()`, pero lo que tenemos son
        celdas...
    - Determinar qué mensaje mostrar en cada panel
      - Según donde esté el panel, puede ser una flecha a la izquierda, a la
        derecha, de frente...
    - Anular o mostrar información de ruta no válida en otros paneles de la zona afectada
      - Determinar la zona afectada, y qué paneles no entran en la ruta
      - Evitar mostrar información en paneles no afectados por la evacuación
        (otros edificios, ...)
    - Quizá fuese interesante delegar estas tareas a un servicio específico

 - Auditiva:
    - Determinar los speakers de la zona afectada
    - Determinar el mensaje a reproducir
      - Soporte para mensajes no transcribibles (sonido de alarma...)?
    - Misma problemática que en Visual

6.- Cómo determinar la válvula o el interruptor que se han de cerrar?
 - Esa información también la necesita el visualizador para representar corte de
   suministro
   [REUNIÓN] Sólo hay una.

